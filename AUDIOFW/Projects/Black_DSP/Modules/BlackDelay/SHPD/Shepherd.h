/*
 * BlackDelay.h
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#ifndef SHEPHERD_H_
#define SHEPHERD_H_

#include "debug.h"
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include <stdlib.h>

#include <SHPD_Conf.h>
#include <BDSP/BDSP_Conf.h>
#include <BDSP/OD_Outs.h>
#include <BDSP/adc.h>
#include <BDSP/i2c.h>
#include "codec.h"
#include "gpio.h"
#include <BDSP/memory.h>
#include <BDSP/timers.h>

#include "mixer.h"
#include "compressor.h"
#include "fader.h"
#include "delay.h"
#include "feedback.h"


typedef enum
{
	BDEL_MODE_NORMAL=0,
	BDEL_MODE_SIMPLE,
	BDEL_MODE_CALIB,
	BDEL_MODE_NUM
}BlackDelay_Glob_ModeEnum;

#define FADER_TIME_MS 15.0f
#define FADER_LENGTH ((int)FADER_TIME_MS*I2S_SAMPLING_FREQ/1000U)

#define wavetable_num 20000U
#define min_wavetable	512
//void TIM_Callback(TIM_HandleTypeDef *htim);
void Shepherd_init(void);
void Shepherd_run(void);
void create_waverings(void);

static int32_t get_level(float CV, float POT, float scale);
static float get_level_f(float CV, float POT, float scale);

static void sync(void);
static void rev_pb(void);
static void add_pb(void);
static void hold_pb(void);
static void tapeordigi(void);
static void pingpong(void);
static void tap_control(void);
void IO_IT_Callback(IO_ENUM_TypeDef num, IO_States Current_state);

static inline void speed_test(void);
#endif

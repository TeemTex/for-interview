/*
 * BlackDelay.c
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#include "stm32f4xx_it.h"
#include "Shepherd.h"

static uint16_t latestADC=0;

IO_States Controls[SWPB_NUM];
float adc_vals[4];

static Delay_HandleTypeDef hDelay;

I2C_HandleTypeDef hi2c;
static Feedback_HandleTypeDef Feedback;

static uint32_t last_press_time[SWPB_NUM];

static volatile int CV_Feedback_num, POT_Feedback_num, CV_Time_num, POT_Time_num;

Ring_Handle_TypeDef SDRAM_Ring;

Ring_Handle_TypeDef wavetable[10];


static uint32_t counter;
static uint32_t ADC_TIME;

void Shepherd_init(void)
{

	debug_print("%s\n","Initializing SHPD");

	I2C_Init(&hi2c);
	Codec_configure_CodecA();



	OD_Init_all();
	CV_Feedback_num = ADC_PrepareChannel(CV_FEEDBACK, CV_LPF_COEF,CV_TRACK_FOR, CV_MIN_CHANGE);
	CV_Time_num = ADC_PrepareChannel(CV_TIME, CV_LPF_COEF,CV_TRACK_FOR, CV_MIN_CHANGE);
	POT_Feedback_num = ADC_PrepareChannel(POT_FEEDBACK, KNOB_LPF_COEF, KNOB_TRACK_FOR, KNOB_MIN_CHANGE);
	POT_Time_num = ADC_PrepareChannel(POT_TIME,KNOB_LPF_COEF, KNOB_TRACK_FOR, KNOB_MIN_CHANGE);

	ADC_Init();
	TIM2_init();

	Delay_init(&hDelay);
	Memory_RAM_Init(&hDelay.hMem);

	IO_configure(SYNC_IN_ENUM,IO_IS_IT,35);
	IO_configure(SYNC_PB_ENUM,IO_IS_INPUT,35);
	IO_configure(ADD_PB_ENUM,IO_IS_INPUT,35);
	IO_configure(ADD_IN_ENUM,IO_IS_IT,3);
	IO_configure(HOLD_PB_ENUM,IO_IS_IT,35);
	IO_configure(HOLD_IN_ENUM,IO_IS_IT,3);
	IO_configure(REV_PB_ENUM,IO_IS_IT,35);
	IO_configure(REV_IN_ENUM,IO_IS_IT,3);
	IO_configure(PINGPONG_PB_ENUM,IO_IS_INPUT,35);
	IO_configure(DELAY_TYPE_ENUM,IO_IS_INPUT,3);

	IO_InitAll();

	ADC_start();
	TIM2_start();
	Codec_start_CodecA();

	create_waverings();

}
float val;
void create_waverings(void)
{
	float pi = 3.142f;
	float phi=0;
	ring_buffer_init(&SDRAM_Ring,(float*)SDRAM_BANK_ADDR,SDRAM_SIZE_S32);

	wavetable[1].hParent = &SDRAM_Ring;
	uint32_t size = I2S_SAMPLING_FREQ;
	ring_buffer_init(&wavetable[1],SDRAM_Ring.Head,size);
	wavetable[1].hParent = &SDRAM_Ring;

	for(volatile int y = 0; y<size; y++)
	{
		float * ptr =  wavetable[1].Head;
		val=8388008.0f*sin(2.0f*pi*(double)y/(double)I2S_SAMPLING_FREQ);
		ptr[0]=val;
		Ring_Head_Move(&wavetable[1],1);
	}

	Ring_Head_Move(&SDRAM_Ring,size);
	ring_buffer_init(&wavetable[2],SDRAM_Ring.Head,size);
	wavetable[2].hParent = &SDRAM_Ring;

	for(volatile int y = 0; y<size; y++)
	{
		float * ptr =  wavetable[2].Head;
		val=sin(pi*(double)y/(double)I2S_SAMPLING_FREQ);
		ptr[0]=val;
		Ring_Head_Move(&wavetable[2],1);
	}
//	int i = 440;
//		for(volatile int i=0; i<wavetable_num; i++)
//		{
//
//			uint32_t size = I2S_SAMPLING_FREQ/(i+200);
//			while(size<min_wavetable)
//			{
//				size=size*2;
//			}
//			ring_buffer_init(&wavetable[i],SDRAM_Ring.Head,size);
//			wavetable[i].hParent = &SDRAM_Ring;
//			for(volatile int y = 0; y<size; y++)
//			{
//				float * ptr =  wavetable[i].Head;
//				val=8388008.0f*sin(2.0f*pi*(double)y*(double)(i+200)/(double)I2S_SAMPLING_FREQ);
//				ptr[0]=val;
//				Ring_Head_Move(&wavetable[i],1);
//			}
//			Ring_Head_Move(&SDRAM_Ring,size);
//		}
}

/*
 * Main module loop
 * ReadVal - Values from memory
 * i2sread - Values from ADC
 * ToMemory_f - Values to memory
 * */

//ToDo: array size should be defined in config file. BDSP platform supports up to 4 channels in and out..
static  float ReadVal[2];
static float i2sread[2];
static float ToMemory_f[2];
static float Debug_var[2];


#define OCTAVE_NUM 8
#define pi 3.142f
float phi=0.0f;
float sine_wave[OCTAVE_NUM];
float f=440.0f;
float A;
float * pRead;
static int cnt=0;
int F=440;
float cnt_lim;
float coefs[15]={1/10000, 1/5000, 1/1000, 1/500, 1/100, 1/50, 1/10, 1, 1/10, 1/50, 1/100, 1/500, 1/1000, 1/5000, 1/10000};
void Shepherd_run(void)
{


	/* #2 - READ ADC	*/
	i2sread[0]=CodecA_Read_Val_24b();
	i2sread[1]=CodecA_Read_Val_24b();



	cnt_lim = adc_vals[3]/48.0f;
	F = cnt>(int)cnt_lim ? F+1 : F;
	cnt = cnt>(int)cnt_lim ? 0:cnt+1;
	F = F>adc_vals[2]*2/8?55:F;
//

//	F=440;
	i2sread[0]=0.0f;
	i2sread[1]=0.0f;
	float coef =0;
	float * pcoef;
	for(int xq=1; xq<16; xq++)
	{
		Ring_Head_Move(&wavetable[1],xq*F);
		pRead = wavetable[1].Head;

		wavetable[2].Head = wavetable[2].pBuf;
		Ring_Head_Move(&wavetable[2],xq*F*54);
		pcoef = wavetable[2].Head;
		i2sread[0]+=pcoef[1]*pRead[0];
		i2sread[1]=i2sread[0];
		Ring_Head_Move(&wavetable[1],-xq*F);
	}
	Ring_Head_Move(&wavetable[1],F);

	CodecA_Write_Val_24b(i2sread[0]);
	CodecA_Write_Val_24b(i2sread[1]);

//	speed_test();
}

static inline void speed_test(void)
{
	static signed cntr=0;
	static uint32_t timestamp=0;
	if(HAL_GetTick()-timestamp>1000)
	{

		timestamp=HAL_GetTick();
		debug_print("%s","Cycle time = ");
		float cycle_time = 1000.0f/(float)cntr;
		debug_print("%.6f\n",cycle_time);
		cntr=0;
	}
	else
	{
		//		timestamp=HAL_GetTick();
		cntr++;
	}
}


static uint32_t update_hold_fader_start(Ring_Handle_TypeDef* pRing, Delay_DirectionkModes_EnumTypeDef Direction)
{
	if(Direction==DIRECTION_NORMAL)
	{
		if(pRing->size>FADER_LENGTH)
		{
			return pRing->size - FADER_LENGTH/2;
		}
		else
		{
			return pRing->size/2;
		}
	}
	else
	{
		if(pRing->size>FADER_LENGTH)
		{
			return FADER_LENGTH/2;
		}
		else
		{
			return pRing->size/2;
		}
	}


}


void TIM_Callback(TIM_HandleTypeDef *htim)
{
	if (htim->Instance == TIM2)
	{

		counter+=1;

		IO_States dt_state = IO_Read_Pin(DELAY_TYPE_ENUM);
		if(dt_state!=Controls[SW_DEL_TYPE])
		{
			Controls[SW_DEL_TYPE]=dt_state;
			if(Controls[SW_DEL_TYPE]==OFF)
			{
				hDelay.mode = DELAY_Digital;
			}
			else
			{
				hDelay.mode = DELAY_TAPE;
			}
		}

		IO_States pp_state = IO_Read_Pin(PINGPONG_PB_ENUM);
		if(pp_state!=Controls[SW_PP])
		{
			Controls[SW_PP]=pp_state;
			if(Controls[SW_PP]==OFF)
			{
				Feedback.type = FEEDBACK_DIRECT;
			}
			else
			{
				Feedback.type = FEEDBACK_PING_PONG;
			}
		}

		IO_States adpb_state = IO_Read_Pin(ADD_PB_ENUM);
		if(adpb_state!=Controls[PB_ADD])
		{
			Controls[PB_ADD]=adpb_state;
			//			if(Controls[PB_ADD]!=OFF)
			{
				add_pb();
			}
		}

		ADC_Val_Update(adc_vals);
		tap_control();

		Feedback.level=get_level_f(adc_vals[CV_Feedback_num],adc_vals[POT_Feedback_num],1.0f)/FB_COEF;
		ADC_TIME = get_level(adc_vals[CV_Time_num],adc_vals[POT_Time_num],1.0f);

	}
}


static __IO uint32_t Sync_timestamp;
static uint8_t SyncIsBlink;
static uint32_t LongOnTimestamp;
static  uint8_t reset;
static  uint8_t toggle;
static  uint32_t l_timestamp;

static void tap_control(void)
{

}



static int32_t get_level(float CV, float POT, float scale)
{

	CV = CV - 2048.0f-132.0f;
	if(CV<-2048.0f)
	{
		CV=-2047.0f;
	}
	if(CV>=2048.0f)
	{
		CV=2047.0f;
	}
	if(fabs(CV)<15.0f) CV=0;

	if(POT<=0.0f)
	{
		POT=0.0f;
	}
	if(POT>=4096.0f)
	{
		POT=4095.0f;
	}

	float result=0;
	//	if(POT>CV)
	{
		result = POT + CV;
		//		result = POT;
		if(result>=4095.0f)
		{
			return (int32_t)(4095.0f*scale);
		}
		else if (result<0)
		{
			return 0;
		}
		else
		{
			return (int32_t)(result*scale);
		}

	}

}
//
static float get_level_f(float CV, float POT, float scale)
{

	CV = CV - 2048.0f-150.0f;
	if(CV<=-2048.0f)
	{
		CV=-2047.0f;
	}
	if(CV>=2048.0f)
	{
		CV=2047.0f;
	}

	if(POT<=0.0f)
	{
		POT=0.0f;
	}
	if(POT>=4096.0f)
	{
		POT=4095.0f;
	}

	float result=0;
	//	if(POT>CV)
	{
		result = POT + CV;
		if(result>=4095.0f)
		{
			return (4095.0f*scale);
		}
		else if (result<0)
		{
			return 0;
		}
		else
		{
			return (result*scale);
		}
	}
}


static void sync(void)
{
}


static void rev_pb(void)
{
}
static void add_pb(void)
{
}
static void hold_pb(void)
{
}


static void tapeordigi(void)
{
}

static void pingpong(void)
{
}


void IO_IT_Callback(IO_ENUM_TypeDef num, IO_States Current_state)
{
	switch(num)
	{
	case SYNC_PB_ENUM:
	case SYNC_IN_ENUM:
		sync();
		break;
	case ADD_PB_ENUM:
	case ADD_IN_ENUM:
		add_pb();
		break;
	case HOLD_PB_ENUM:
	case HOLD_IN_ENUM:
		hold_pb();
		break;
	case REV_PB_ENUM:
	case REV_IN_ENUM:
		rev_pb();
		break;
	case PINGPONG_PB_ENUM:
		pingpong();
		break;
	case DELAY_TYPE_ENUM:
		tapeordigi();
		break;
	}
}


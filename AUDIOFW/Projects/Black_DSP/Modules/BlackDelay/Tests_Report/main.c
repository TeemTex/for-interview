
/* Includes ------------------------------------------------------------------*/


#include "BDSP.h"
#include "arm_math.h"
#include "arm_const_structs.h"
#include "arm_common_tables.h"
#include "wavetable_init.h"
#include "debug.h"

#define BLOCK_SIZE            32
#define NUM_TAPS              29
#define TEST_LENGTH_SAMPLES  1024
#define SNR_THRESHOLD_F32    140.0f


static unsigned int get_cyclecount (void);

float sine64[64];
float sine256[256];
float sine1024[1024];
float sine1024_out[1024];
float sine4096[4096];

/* ------------------------------------------------------------------
 * Global variables for FFT Bin Example
 * ------------------------------------------------------------------- */
uint32_t fftSize = 1024;
uint32_t ifftFlag = 0;
uint32_t doBitReverse = 1;

uint32_t refIndex = 213, testIndex = 0;

const float32_t firCoeffs32[NUM_TAPS] = {
  -0.0018225230f, -0.0015879294f, +0.0000000000f, +0.0036977508f, +0.0080754303f, +0.0085302217f, -0.0000000000f, -0.0173976984f,
  -0.0341458607f, -0.0333591565f, +0.0000000000f, +0.0676308395f, +0.1522061835f, +0.2229246956f, +0.2504960933f, +0.2229246956f,
  +0.1522061835f, +0.0676308395f, +0.0000000000f, -0.0333591565f, -0.0341458607f, -0.0173976984f, -0.0000000000f, +0.0085302217f,
  +0.0080754303f, +0.0036977508f, +0.0000000000f, -0.0015879294f, -0.0018225230f
};

static float32_t firStateF32[BLOCK_SIZE + NUM_TAPS - 1];
uint32_t blockSize = BLOCK_SIZE;
uint32_t numBlocks = TEST_LENGTH_SAMPLES/BLOCK_SIZE;


/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void)
{
	  uint32_t i;
	  arm_fir_instance_f32 S;
	  arm_status status;
	  float32_t  *inputF32, *outputF32;


	  /* Initialize input and output buffer pointers */
	  inputF32 = &sine1024[0];
	  outputF32 = &sine1024_out[0];

	HAL_Init();
	BDSP_Init_SysClk();

	float maxValue;



	sine_init(sine64,64,10,1);
	sine_init(sine256,256,10,4);
	sine_init(sine1024,1024,10,16);
	sine_init(sine4096,4096,10,32);

	/* enable user-mode access to the performance counter*/
	asm ("MCR p15, 0, %0, C9, C14, 0\n\t" :: "r"(1));


	uint32_t entry_cycle = get_cyclecount();

	arm_cfft_f32(&arm_cfft_sR_f32_len1024, sine1024, ifftFlag, doBitReverse);

	/* Process the data through the Complex Magnitude Module for
  calculating the magnitude at each bin */
	arm_cmplx_mag_f32(sine1024, sine1024_out, fftSize);

	/* Calculates maxValue and returns corresponding BIN value */
	arm_max_f32(sine1024_out, fftSize, &maxValue, &testIndex);

	uint32_t exit_cycle = get_cyclecount();

	uint32_t elapsed=exit_cycle - entry_cycle;


	debug_print("%s\t%d\n","FFT cycles taken:",elapsed);

	entry_cycle = get_cyclecount();

	  /* Call FIR init function to initialize the instance structure. */
	  arm_fir_init_f32(&S, NUM_TAPS, (float32_t *)&firCoeffs32[0], &firStateF32[0], blockSize);

	  for(i=0; i < numBlocks; i++)
	  {
	    arm_fir_f32(&S, inputF32 + (i * blockSize), outputF32 + (i * blockSize), blockSize);
	  }

		exit_cycle = get_cyclecount();

		elapsed=exit_cycle - entry_cycle;

		debug_print("%s\t%d\n","FIR cycles taken:",elapsed);

	while (1)	{	}
}

static inline unsigned int get_cyclecount(void)
{
	unsigned int value;
	// Read CCNT Register
	asm volatile ("MRC p15, 0, %0, c9, c13, 0\t\n": "=r"(value));
	return value;
}






/*
 * BlackDelay.h
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#ifndef BLACKDELAY_H_
#define BLACKDELAY_H_

#include "debug.h"
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include <stdlib.h>

#include <BDEL1_Conf.h>
#include <BDSP/BDSP_Conf.h>
#include <BDSP/OD_Outs.h>
#include <BDSP/adc.h>
#include <BDSP/i2c.h>
#include "codec.h"
#include "gpio.h"
#include <BDSP/memory.h>
#include <BDSP/timers.h>

#include "mixer.h"
#include "compressor.h"
#include "fader.h"
#include "delay.h"
#include "feedback.h"


typedef enum
{
	BDEL_MODE_NORMAL=0,
	BDEL_MODE_SIMPLE,
	BDEL_MODE_CALIB,
	BDEL_MODE_NUM
}BlackDelay_Glob_ModeEnum;

#define FADER_TIME_MS 15.0f
#define FADER_LENGTH ((int)FADER_TIME_MS*I2S_SAMPLING_FREQ/1000U)

//void TIM_Callback(TIM_HandleTypeDef *htim);
void BDEL_init(void);
void BDEL_Run(void);

static int32_t get_level(float CV, float POT, float scale);
static float get_level_f(float CV, float POT, float scale);

static void sync(void);
static void rev_pb(void);
static void add_pb(void);
static void hold_pb(void);
static void tapeordigi(void);
static void pingpong(void);
static void tap_control(void);
void Clipping_LED(__IO float * values);
static void prepare_write_values(float * RawRead, float * Output_Buffer);
static void reverse_process(void);
static void reverse_toggle(void);
static void playback_process(void);
static void apply_faders_on_output(void);
static uint32_t update_hold_fader_start(Ring_Handle_TypeDef* pRing, Delay_DirectionkModes_EnumTypeDef Direction);
void IO_IT_Callback(IO_ENUM_TypeDef num, IO_States Current_state);

static void process(void);
static void normal_playback(void);
static void normal_hold(void);
static void normal_add_to_hold(void);
static void normal_add(void);
static void reversed_playback(void);
static void reversed_hold(void);
static void reversed_add_to_hold(void);

void pbpb(void);

static void reversed_add(void);

#endif

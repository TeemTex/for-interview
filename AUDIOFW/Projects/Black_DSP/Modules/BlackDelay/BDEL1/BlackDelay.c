/*
 * BlackDelay.c
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#include "stm32f4xx_it.h"
#include "BlackDelay.h"

static uint16_t latestADC=0;
static uint8_t spread_active;
IO_States Controls[SWPB_NUM];
float adc_vals[4];
static uint8_t spread_blink=0;
static Delay_HandleTypeDef hDelay;

I2C_HandleTypeDef hi2c;
static Feedback_HandleTypeDef Feedback;

static FadeInOut_HandleTypeDef Output_FadeInOut;
//static FadeInOut_HandleTypeDef Fader_ReverseFBPop;
static FadeInOut_ms_HandleTypeDef Fader_ReverseAntiPop;
static FadeInOut_HandleTypeDef Fader_HoldAntiPop;
static FadeInOut_HandleTypeDef Fader_RevPressed;
static FadeInOut_HandleTypeDef Fader_HoldPressed;
//FadeInOut_HandleTypeDef Add_FadeInOut;
//FadeInOut_HandleTypeDef Hold_FadeInOut;
//FadeInOut_HandleTypeDef Hold_FadeEnterExit;


//static __IO uint8_t hDelay.Add_Flag;
//static __IO uint8_t hDelay.Hold_Flag;
static uint32_t mult=1;
static uint32_t divider=1;
static uint8_t div_active = 0;
static uint8_t tap_active = 0;
static uint8_t dir_active = 1;
static uint32_t last_press_time[SWPB_NUM];

static volatile int CV_Feedback_num, POT_Feedback_num, CV_Time_num, POT_Time_num;

static uint32_t ADC_TIME;

static BlackDelay_Glob_ModeEnum mode;

void BDEL_init(void)
{

	mode = BDEL_MODE_SIMPLE ;

	I2C_Init(&hi2c);
	Codec_configure_CodecA();



	OD_Init_all();
	CV_Feedback_num = ADC_PrepareChannel(CV_FEEDBACK, CV_LPF_COEF,CV_TRACK_FOR, CV_MIN_CHANGE);
	CV_Time_num = ADC_PrepareChannel(CV_TIME, CV_LPF_COEF,CV_TRACK_FOR, CV_MIN_CHANGE);
	POT_Feedback_num = ADC_PrepareChannel(POT_FEEDBACK, KNOB_LPF_COEF, KNOB_TRACK_FOR, KNOB_MIN_CHANGE);
	POT_Time_num = ADC_PrepareChannel(POT_TIME,KNOB_LPF_COEF, KNOB_TRACK_FOR, KNOB_MIN_CHANGE);

	ADC_Init();
	TIM2_init();


	hDelay.hMem.MemoryType = MEM_TYPE_SDRAM;
	hDelay.hMem.Addr_max=4000000;
	hDelay.hMem.Read_addr=1;
	hDelay.hMem.Write_addr=0;
	hDelay.hMem.offset=2000000;
	Memory_RAM_Init(&hDelay.hMem);
	Memory_Clean(&hDelay.hMem);

	Delay_init(&hDelay);

	if(!PINGPONG_PB)
	{
		Controls[SW_PP]=OFF;
		Feedback.type = FEEDBACK_DIRECT;
	}
	else
	{
		Controls[SW_PP]=ON;
		Feedback.type = FEEDBACK_PING_PONG;
	}

	hDelay.TimeAlgo = DIRECT;
	if(!DELAY_TYPE_PB)
	{
		Controls[SW_DEL_TYPE]=ON;
		hDelay.mode = DELAY_Digital;
	}
	else
	{
		Controls[SW_DEL_TYPE]=OFF;
		hDelay.mode = DELAY_TAPE;
	}

	if(!HOLD_PB)
	{
		hDelay.Playback = PLAYBACK;
//		hold_pb();
	}
	else
	{
		Controls[PB_HOLD]=OFF;
		hDelay.Playback = HOLD;
		hold_pb();
	}



	IO_configure(SYNC_IN_ENUM,IO_IS_IT,2);
	IO_configure(SYNC_PB_ENUM,IO_IS_INPUT,35);
	IO_configure(ADD_PB_ENUM,IO_IS_INPUT,35);
	IO_configure(ADD_IN_ENUM,IO_IS_IT,2);
	IO_configure(HOLD_PB_ENUM,IO_IS_IT,35);
	IO_configure(HOLD_IN_ENUM,IO_IS_IT,2);
	IO_configure(REV_PB_ENUM,IO_IS_IT,35);
	IO_configure(REV_IN_ENUM,IO_IS_IT,2);
	IO_configure(PINGPONG_PB_ENUM,IO_IS_INPUT,35);
	IO_configure(DELAY_TYPE_ENUM,IO_IS_INPUT,35);

	IO_InitAll();



	feedback_init(&Feedback,1U<<31,0.90f,60,FB_COEF);






	if(isCALIB)
	{
		debug_print("%s\n","Calibration should be activated");
		mode =  BDEL_MODE_CALIB;
		ALL_LED_ON;
		HAL_Delay(1000);
		ALL_LED_OFF;
		HAL_Delay(1000);
		ALL_LED_ON;
		HAL_Delay(1000);
		ALL_LED_OFF;
	}

	if(isALT_MODE)
	{
		debug_print("%s\n","Activating secret mode #1");
		mode =  BDEL_MODE_NORMAL;
		ALL_LED_ON;
		HAL_Delay(1000);
		ALL_LED_OFF;
	}

	ADC_start();
	TIM2_start();
	Codec_start_CodecA();
//	cs4271_MUTE_BOTH_CH();
//	cs4271_UNMUTE_BOTH_CH();
//	return mode;
}

/*
 * Main module loop
 * ReadVal - Values from memory
 * i2sread - Values from ADC
 * ToMemory_f - Values to memory
 * */

//ToDo: array size should be defined in config file. BDSP platform supports up to 4 channels in and out..
static  float ReadVal[3];
static float Output[2];
static float i2sread[2];
static float ToMemory_f[2];
static float Debug_var[2];
void BDEL_Run(void)
{


	/* #2 - READ ADC	*/
	i2sread[0]=CodecA_Read_Val_24b();
	i2sread[1]=CodecA_Read_Val_24b();

	process();
	//	reverse_process();
	//	playback_process();

	Clipping_LED(ToMemory_f);
	//	__IO signed int WriteVal[2];
	//	WriteVal[0]= (int)ReadVal[0];
	//	WriteVal[1]= (int)ReadVal[1];
	//
	//	CodecA_Write_Val_24b(WriteVal[0]);
	//	CodecA_Write_Val_24b(WriteVal[1]);


	if(Feedback.type!=FEEDBACK_PING_PONG)
	{
		//Output0 goes for Right
		Output[0]=ReadVal[2];
		Output[1]=ReadVal[0];
	}
	else
	{
		Output[0]=ReadVal[1];
		Output[1]=ReadVal[0];
	}

	apply_faders_on_output();
	CodecA_Write_Val_24b(Output[0]);
	CodecA_Write_Val_24b(Output[1]);


	//		CodecA_Write_Val_24b(i2sread[0]);
	//		CodecA_Write_Val_24b(i2sread[1]);

}


static void apply_faders_on_output(void)
{
	Output[0] = FadeOut_Stereo(Output[0],&Fader_RevPressed,0);
	Output[1] = FadeOut_Stereo(Output[1],&Fader_RevPressed,1);
//
//
	Output[0] = FadeOut_Stereo(Output[0],&Fader_HoldPressed,0);
	Output[1] = FadeOut_Stereo(Output[1],&Fader_HoldPressed,1);

	Output[0] = FadeInOut_ms_Stereo(Output[0],&Fader_ReverseAntiPop,0);
	Output[1] = FadeInOut_ms_Stereo(Output[1],&Fader_ReverseAntiPop,1);

	Output[0] = FadeInOut_Stereo(Output[0],&Fader_HoldAntiPop,0);
	Output[1] = FadeInOut_Stereo(Output[1],&Fader_HoldAntiPop,1);

//	Output[0] = FadeInOut_Stereo(Output[0],&Fader_HoldAntiPopRev,0);
//	Output[1] = FadeInOut_Stereo(Output[1],&Fader_HoldAntiPopRev,1);

	Output[0] = FadeInOut_Stereo(Output[0],&Output_FadeInOut,0);
	Output[1] = FadeInOut_Stereo(Output[1],&Output_FadeInOut,1);
}

static uint32_t update_hold_fader_start(Ring_Handle_TypeDef* pRing, Delay_DirectionkModes_EnumTypeDef Direction)
{
	if(Direction==DIRECTION_NORMAL)
	{
		if(pRing->size>FADER_LENGTH)
		{
			return pRing->size - FADER_LENGTH/2;
		}
		else
		{
			return pRing->size/2;
		}
	}
	else
	{
		if(pRing->size>FADER_LENGTH)
		{
			return FADER_LENGTH/2;
		}
		else
		{
			return pRing->size/2;
		}
	}


}

static void process(void)
{
	if(hDelay.Direction==DIRECTION_NORMAL)
	{
		switch(hDelay.Playback)
		{
		case PLAYBACK:
			normal_playback();
			break;
		case HOLD:
			normal_hold();
			break;
		case ADD_TO_HOLD:
			normal_add_to_hold();
			break;
		case ADD:
			normal_add();
			break;
		}
	}
	else
	{
		switch(hDelay.Playback)
		{
		case PLAYBACK:
			reversed_playback();
			break;
		case HOLD:
			reversed_hold();
			break;
		case ADD_TO_HOLD:
			reversed_add_to_hold();
			break;
		case ADD:
			reversed_add();
			break;
		}
	}
}

static void normal_playback(void)
{
	Ring_Head_Move(&hDelay.hRing,1);
	Delay_Stereo_Read(&ReadVal[0],&hDelay);


	Feedback_multich(ReadVal,i2sread,ToMemory_f,&Feedback,2);
	//	Feedback_multich(ReadVal,i2sread,Debug_var,&Feedback,2);
	delay_write(&hDelay,ToMemory_f,2);
}
static void normal_hold(void)
{
	Ring_Head_Move(&hDelay.hSubRing,1);
	int32_t fade_start_index = update_hold_fader_start(&hDelay.hSubRing,DIRECTION_NORMAL);
	int32_t current_index;
	static FadeInOut_HandleTypeDef Fader_HoldAntiPop_LPF;
	static volatile float filt_val[2];
	current_index = hDelay.hSubRing.Tail - hDelay.hSubRing.pBuf;

//	if(hDelay.hSubRing.size>hDelay.Fs*95)
	if(1)
	{
		Delay_Stereo_Read(&ReadVal[0],&hDelay);
		if((current_index>=fade_start_index)&&((!Fader_HoldAntiPop.activated))&&hDelay.delay_time>1)
		{
			FadeInOut_Init(&Fader_HoldAntiPop,(hDelay.hSubRing.size-fade_start_index)*2);
		}
//		ADD_LED_OFF;
	}
	else
	{

		static float cc;
		static uint8_t cntr;
		if(cntr==0)
		{
		cc=0.005f*pow(hDelay.delay_time,3)/(hDelay.delay_time*hDelay.Fs);
		}
		cntr++;

		Delay_Stereo_Read(&filt_val[0],&hDelay);
		if((current_index>=fade_start_index)&&(!Fader_HoldAntiPop_LPF.activated))
		{
				FadeInOut_Init(&Fader_HoldAntiPop_LPF,abs(hDelay.hSubRing.size-fade_start_index)*2);
		}
		filt_val[0] = FadeInOut_Stereo(filt_val[0],&Fader_HoldAntiPop_LPF,0);
		filt_val[1] = FadeInOut_Stereo(filt_val[1],&Fader_HoldAntiPop_LPF,1);
		ReadVal[0]=LPF_f32(ReadVal[0],filt_val[0],cc);
		ReadVal[1]=LPF_f32(ReadVal[1],filt_val[1],cc);
	}
}
static void normal_add_to_hold(void)
{
	Delay_stereo_add(&hDelay,i2sread,2);
	normal_hold();
}
static void normal_add(void)
{
	normal_playback();
}

static void reversed_playback(void)
{
	Ring_Head_Move(&hDelay.hRing,1);
	hDelay.virtual_head_ptr = Ring_Point_Offset_Ptr(&hDelay.hRing,-1,hDelay.virtual_head_ptr);

	Delay_Stereo_Read(ReadVal,&hDelay);

	float fb_buf[2];
	fb_buf[0] = Fader_f(i2sread[0],Fader_ReverseAntiPop.level);
	fb_buf[1] = Fader_f(i2sread[1],Fader_ReverseAntiPop.level);
	Feedback_multich(ReadVal,fb_buf,ToMemory_f,&Feedback,2);
	//	ToMemory_f[0] = ReadVal[0];
	//	ToMemory_f[1] = ReadVal[1];
	delay_write(&hDelay,ToMemory_f,2);
	//		delay_write(&hDelay,i2sread,2);


	uint32_t reset_period = hDelay.delay_time*hDelay.Fs;

	//	uint32_t current_period = hDelay.reverse_counter;

	if(hDelay.delay_time>FADER_TIME_MS)
	{
		uint32_t threshold = (reset_period-(FADER_LENGTH/2));
		if((hDelay.reverse_counter>=threshold)&&(!Fader_ReverseAntiPop.activated))
		{
			//			FadeInOut_Init(&Fader_ReverseFBPop,FADER_LENGTH);
			FadeInOut_ms_Init(&Fader_ReverseAntiPop,FADER_TIME_MS,hDelay.Fs);
		}
	}
	else
	{
		uint32_t threshold = reset_period/2;
		if ((hDelay.reverse_counter>=threshold)&&(!Fader_ReverseAntiPop.activated))
		{
			//			FadeInOut_Init(&Fader_ReverseFBPop,hDelay.delay_time*hDelay.Fs);
			FadeInOut_ms_Init(&Fader_ReverseAntiPop,hDelay.delay_time,hDelay.Fs);
		}
	}

	if((hDelay.reverse_counter>=reset_period)&&(Fader_ReverseAntiPop.level<0.001f))
	{
		hDelay.reverse_counter=0;
		hDelay.hRevRing.size = reset_period;
		hDelay.virtual_head_ptr = hDelay.hRing.Tail;
		hDelay.hRevRing.Tail = hDelay.hRing.Head;
		hDelay.hRevRing.pBuf = hDelay.hRing.Tail;
		hDelay.rev_ptr = hDelay.hRing.Head;
	}
	else
	{
		hDelay.reverse_counter++;
	}



}
static void reversed_hold(void)
{
	Ring_Head_Move(&hDelay.hSubRing,-1);
	Delay_Stereo_Read(&ReadVal[0],&hDelay);


	uint32_t reset_period = hDelay.delay_time*hDelay.Fs-1;
	if(hDelay.delay_time>FADER_TIME_MS)
	{
		uint32_t threshold = (reset_period-(FADER_LENGTH/2));
		if((hDelay.reverse_counter>=threshold)&&(!Fader_ReverseAntiPop.activated))
		{
			FadeInOut_ms_Init(&Fader_ReverseAntiPop,FADER_TIME_MS,hDelay.Fs);
		}
	}
	else
	{
		uint32_t threshold = reset_period/2;
		if ((hDelay.reverse_counter>=threshold)&&(!Fader_ReverseAntiPop.activated))
		{
			FadeInOut_ms_Init(&Fader_ReverseAntiPop,hDelay.delay_time,hDelay.Fs);
		}
	}

	if((hDelay.reverse_counter>=reset_period))
	{
		hDelay.reverse_counter=0;
	}
	else
	{
		hDelay.reverse_counter++;
	}

	uint32_t fade_start_index = update_hold_fader_start(&hDelay.hSubRing,DIRECTION_REVERSE);
	uint32_t current_index;
	current_index = hDelay.hSubRing.Tail - hDelay.hSubRing.pBuf;

	if((current_index<=fade_start_index)&&(!Fader_HoldAntiPop.activated))
	{
		FadeInOut_Init(&Fader_HoldAntiPop,(fade_start_index)*2);
	}
}
static void reversed_add_to_hold(void)
{
	Delay_stereo_add(&hDelay,i2sread,2);
	reversed_hold();
}
static void reversed_add(void)
{

}


static void playback_process(void)
{
	//
	//
	//
	//	switch (hDelay.Playback)
	//	{
	//	case PLAYBACK:
	//	{
	//		Feedback_multich((float)ReadVal,i2sread,ToMemory_f,&Feedback,2);
	//
	//		Ring_Head_Move(&hDelay.hRing,1);
	//		if(hDelay.Direction==DIRECTION_REVERSE)
	//		{
	//			hDelay.virtual_head_ptr = Ring_Ptr_Move(&hDelay.hRing,-1,hDelay.virtual_head_ptr);
	//			hDelay.reverse_counter++;
	//		}
	//		delay_write(&hDelay,ToMemory_f,2);
	//		break;
	//	}
	//
	//	case HOLD:
	//	{
	//		Ring_Head_Move(&hDelay.hSubRing,1);
	//		uint32_t fade_start_index = update_hold_fader_start(&hDelay.hSubRing,hDelay.Direction);
	//		uint32_t current_index;
	//		current_index = hDelay.hSubRing.Tail - hDelay.hSubRing.pBuf;
	//		if(hDelay.Direction==DIRECTION_NORMAL)
	//		{
	//
	//			if((current_index>=fade_start_index)&&(!Fader_HoldAntiPop.activated))
	//			{
	//				FadeInOut_Init(&Fader_HoldAntiPop,(hDelay.hSubRing.size-fade_start_index)*2);
	//			}
	//		}
	//		else
	//		{
	//
	//			hDelay.virtual_head_ptr = Ring_Ptr_Move(&hDelay.hRing,-1,hDelay.virtual_head_ptr);
	//			if((current_index<=fade_start_index)&&(!Fader_HoldAntiPop.activated))
	//			{
	//				FadeInOut_Init(&Fader_HoldAntiPop,(fade_start_index)*2);
	//			}
	//		}
	//
	//		break;
	//	}
	//
	//	case ADD:
	//		break;
	//	case ADD_TO_HOLD:
	//		break;
	//	}
}
static void reverse_process(void)
{
	//	static __IO uint32_t counter = 0;
	//	counter++;
	//	if(hDelay.Direction==DIRECTION_REVERSE)
	//	{
	//		if(hDelay.Playback==PLAYBACK)
	//		{
	//			static __IO uint32_t rev_timestamp;
	//			uint32_t temp_rev_time  =counter/hDelay.Fs;
	//			if(!Fader_ReverseAntiPop.activated)
	//			{
	//				if(hDelay.delay_time>FADER_TIME_MS)
	//				{
	//					if((temp_rev_time+(uint32_t)FADER_TIME_MS/2)>=(hDelay.delay_time))
	//					{
	//						FadeInOut_ms_Init(&Fader_ReverseAntiPop,FADER_TIME_MS,hDelay.Fs);
	//					}
	//				}
	//				else
	//				{
	//					if(temp_rev_time>=(hDelay.delay_time/2))
	//					{
	//						FadeInOut_ms_Init(&Fader_ReverseAntiPop,hDelay.delay_time,hDelay.Fs);
	//					}
	//				}
	//
	//
	//			}
	//			else if((temp_rev_time>=hDelay.delay_time))
	//			{
	//				counter=0;
	//				rev_timestamp = HAL_GetTick();
	//				//				if(hDelay->Direction==DIRECTION_NORMAL)
	//				{
	//					//									reverse_toggle();
	//					hDelay.isReverse ^=0x1;
	//				}
	//				hDelay.isReverse =1;
	//				hDelay.reverse_counter=hDelay.delay_time*hDelay.Fs;
	//				hDelay.virtual_head_ptr = Ring_Point_Offset_From_Head(&hDelay.hRing,-1*hDelay.delay_time*hDelay.Fs);
	//
	//			}
	//		}
	//		else if(hDelay.hSubRing.Head==hDelay.hSubRing.pBuf)
	//		{
	//			hDelay.virtual_head_ptr = Ring_Point_Offset_Ptr(&hDelay.hRing,-1*hDelay.hSubRing.size,hDelay.hSubRing.pBuf);
	//			hDelay.reverse_counter=0;
	//		}
	//	}
}

static void reverse_toggle(void)
{
	hDelay.isReverse ^=0x1;
	Ring_Swap(&hDelay.hRing);


	//	debug_print("%s\n","Rev toggle");
}

static void prepare_write_value(float * RawRead, float * Output_Buffer, float * FeedOut_Value, size_t size)
{
	switch(hDelay.Playback)
	{
	case PLAYBACK:

		break;
	case ADD_TO_HOLD:
		break;
	case HOLD:
		break;
	case ADD:
		break;
	}


}


void Clipping_LED(__IO float * values)
{
	static uint8_t clip_cnt=0;
	{
		float clipval = 2147483648.0f*0.90f;
		float temp[2];
		temp[0]=fabs(values[0]);
		temp[1]=fabs(values[1]);
		if((temp[0]>clipval)||(temp[1]>clipval))
		{
			clip_cnt+=2;
		}

		if (clip_cnt>1)
		{

			clip_cnt--;
			hDelay.isClipped=1;
		}
		else
		{
			hDelay.isClipped=0;
		}
	}
}

void TIM_Callback(TIM_HandleTypeDef *htim)
{
	if (htim->Instance == TIM2)
	{

		if(mode==BDEL_MODE_SIMPLE)
		{
			if(!isSYNC)	SYNC_LED_OFF;
		}

		IO_States sync_pb_state = IO_Read_Pin(SYNC_PB_ENUM);
		if((sync_pb_state!=Controls[PB_SYNC])&&(!SYNC_IN))
		{
			if(sync_pb_state==LONG_ON)
			{
				Controls[PB_SYNC]=ON;
			}
			else
			{
				Controls[PB_SYNC]=sync_pb_state;
				if(Controls[PB_SYNC]==ON)
				{
					last_press_time[PB_SYNC]=HAL_GetTick();
				}
				sync();
			}

		}

		IO_States dt_state = IO_Read_Pin(DELAY_TYPE_ENUM);
		if(dt_state!=Controls[SW_DEL_TYPE])
		{
			Controls[SW_DEL_TYPE]=dt_state;
			if(Controls[SW_DEL_TYPE]==OFF)
			{
				hDelay.mode = DELAY_Digital;
			}
			else
			{
				hDelay.mode = DELAY_TAPE;
			}
		}

		IO_States pp_state = IO_Read_Pin(PINGPONG_PB_ENUM);
		if(pp_state!=Controls[SW_PP])
		{
			Controls[SW_PP]=pp_state;
			if(Controls[SW_PP]==OFF)
			{
				Feedback.type = FEEDBACK_DIRECT;
			}
			else
			{
				Feedback.type = FEEDBACK_PING_PONG;
			}
		}

		IO_States adpb_state = IO_Read_Pin(ADD_PB_ENUM);
		if(adpb_state!=Controls[PB_ADD])
		{
			Controls[PB_ADD]=adpb_state;
			//			if(Controls[PB_ADD]!=OFF)
			{
				add_pb();
			}
		}

		ADC_Val_Update(adc_vals);
		tap_control();


		Feedback.level=get_level_f(adc_vals[CV_Feedback_num],adc_vals[POT_Feedback_num],1.0f);
		static uint32_t press_cnt = 0;
		static uint32_t cnt =0;
		static uint32_t spread_timestamp=0;
		static uint32_t add_press_timestamp=0;
		if((hDelay.Playback==PLAYBACK)&&(ADD_PB))
		{
			ADD_LED_ON;
			cnt++;
			uint32_t temp = (uint32_t)Spread_Shape[(int)adc_vals[POT_Time_num]];
			if(!spread_active)
			{
				dir_active = 0;
				ADC_TIME=5000;
				spread_active = abs((signed int)hDelay.interchannel_delay-(signed int)temp)>5 ? 0:1;
				spread_blink=1;
				spread_timestamp = HAL_GetTick();
			}
			if(spread_active)
			{
				hDelay.interchannel_delay = temp;
			}

			uint32_t tdif=HAL_GetTick()-add_press_timestamp;
			if(tdif>35&&cnt==1)
			{
				add_press_timestamp = HAL_GetTick();
				press_cnt=press_cnt==2? 0:press_cnt+1;
			}

			if((35<tdif&&tdif<500)&&(press_cnt==1))
			{
				hDelay.interchannel_delay=0;
				spread_active=0;
				spread_blink=1;
				press_cnt=0;
			}
			else if(tdif>=500)
			{
				press_cnt=0;
			}
		}
		else
		{
			ADD_LED_OFF;
			ADC_TIME = get_level(adc_vals[CV_Time_num],adc_vals[POT_Time_num],1.0f);
			spread_active = 0;
			cnt=0;
		}

		if (spread_blink)
		{
			uint32_t timedif=HAL_GetTick()-spread_timestamp;
			if(timedif<200){ CLIP_LED_OFF;}
			else if(timedif<400){ CLIP_LED_ON;}
			else if(timedif<600){ CLIP_LED_OFF;}
			else if(timedif<800){ CLIP_LED_ON;}
			else if(timedif<1000){ CLIP_LED_OFF; spread_blink=0;}
		}

		if(hDelay.TimeAlgo==TAPPED)
		{
			int adc_time_shaped = ADC_TIME;
			ADC_TIME-=CALIB_VAL;
			if ((adc_time_shaped>0xFFFF)||(adc_time_shaped<0)) adc_time_shaped=0;
			if (adc_time_shaped>4095) adc_time_shaped=4095;
			adc_time_shaped = Time_Shape[adc_time_shaped];

			int tap_compare;
			hDelay.delay_time = hDelay.sync_time*mult/divider;
			if(mode==BDEL_MODE_NORMAL)
			{
				tap_compare = abs((signed int)((signed int)adc_time_shaped - (signed int)hDelay.sync_time));
			}
			else
			{
				tap_compare = abs((signed int)latestADC-(signed int)adc_time_shaped)>5 ? 0:1;
			}

			if (tap_compare==0)
			{
				tap_active=1;
			}
			if ((latestADC!=hDelay.delay_time)&&(tap_active))
			{

				hDelay.TimeAlgo=DIRECT;
				if(mode!=BDEL_MODE_NORMAL) SYNC_LED_OFF;
				//				hDelay.delay_time=KNOB_VALS[TIME];
				//				latestADC=KNOB_VALS[TIME];
			}
		}
		else if (hDelay.TimeAlgo==KNOB_DIVMULT)
		{

			mult=Knobs_get_multiplier((int)ADC_TIME);
			divider=Knobs_get_divider((int)ADC_TIME);
			if(!div_active)
			{
				if((mult==1)&&(divider==1))
				{
					div_active=1;
				}
				else
				{
					mult=1;
					divider=1;
				}
			}
			hDelay.delay_time=(hDelay.sync_time*mult)/divider;
			//			hDelay.delay_time-=CALIB_VAL;


			while(hDelay.delay_time>(MAX_DELAY_TIME_MS))
			{
				if (mult>1)
				{
					mult--;
					hDelay.delay_time=((hDelay.sync_time)*mult)/divider;
					//					hDelay.delay_time-=CALIB_VAL;
				}
				else
				{
					hDelay.delay_time = hDelay.delay_time / 2;
				}
			}

			hDelay.delay_time+=CALIB_VAL;
			if ((hDelay.delay_time>0xFFFF)||(hDelay.delay_time<1)) hDelay.delay_time=1;
			if (hDelay.delay_time>4095) hDelay.delay_time=4095;
		}
		else
		{
			uint32_t temp=ADC_TIME;
			temp-=CALIB_VAL;
			temp = (temp>0xFFFF)||(temp<0) ? 0 : temp>4095 ? temp=4095 : temp;
			temp = Time_Shape[temp];
			if(!dir_active)
			{
				dir_active = abs((signed int)hDelay.delay_time-(signed int)temp)>5 ? 0:1;
			}

			if(dir_active)
			{
			hDelay.delay_time=temp;
			}
		}

		if (hDelay.Playback==PLAYBACK)
		{
			HOLD_LED_OFF;
		}
		else
		{
			HOLD_LED_ON;
		}

		if (hDelay.Playback==ADD_TO_HOLD)
		{
			ADD_LED_ON;
		}
//		else
//		{
//			ADD_LED_OFF;
//		}

		if(!spread_blink)
		{
			if ((hDelay.isClipped==1))
			{
				CLIP_LED_ON;
			}
			else
			{
				CLIP_LED_OFF;
			}

		}
	}
}


static __IO uint32_t Sync_timestamp;
static uint8_t SyncIsBlink;
static uint32_t LongOnTimestamp;
static  uint8_t reset;
static  uint8_t toggle;
static  uint32_t l_timestamp;
static void tap_control(void)
{


	if ((hDelay.TimeAlgo == KNOB_DIVMULT)&&(!SYNC_PB))
	{
		uint32_t   temp=HAL_GetTick();
		uint32_t  timedif=temp-Sync_timestamp;

		if(timedif>(SYNC_RESET_TIME))
		{
			reset=1;
		}


		//		if(reset==1)
		{

			uint32_t tempdif = abs((int)temp-(int)l_timestamp);
			if (tempdif>=(hDelay.delay_time/2))
			{
				l_timestamp=temp;
				toggle=toggle^0x1;
				//				debug_print("%s\n","toggle");


			}
			if(mode!=BDEL_MODE_SIMPLE)
			{
				if(toggle){
					SYNC_LED_ON;
				}
				else
				{
					SYNC_LED_OFF;
				}
			}

		}
		//		else
		{
			//			SYNC_LED_OFF;

		}
	}

	if(Controls[PB_SYNC]==ON)
	{
		uint32_t timedif=HAL_GetTick()-last_press_time[PB_SYNC];
		if(timedif>LONG_ON_TIME_MS)
		{

			tap_active = 0;
			SyncIsBlink=1;
			LongOnTimestamp = HAL_GetTick();
			Controls[PB_SYNC] = LONG_ON;
			if(hDelay.TimeAlgo!=KNOB_DIVMULT)
			{
				div_active = 0;
				hDelay.TimeAlgo=KNOB_DIVMULT;
				hDelay.sync_time=hDelay.delay_time;
			}
			else
			{
				hDelay.TimeAlgo=TAPPED;
			}

		}

	}
	//	if(SYNC_PB){SYNC_LED_ON;}

	if (SyncIsBlink)
	{
		uint32_t timedif=HAL_GetTick()-LongOnTimestamp;
		if(timedif<200){ SYNC_LED_OFF;}
		else if(timedif<400){ SYNC_LED_ON;}
		else if(timedif<600){ SYNC_LED_OFF;}
		else if(timedif<800){ SYNC_LED_ON;}
		else if(timedif<1000){ SYNC_LED_OFF; SyncIsBlink=0;}
	}
}



static int32_t get_level(float CV, float POT, float scale)
{

	CV = CV - 2048.0f-132.0f;
	if(CV<-2048.0f)
	{
		CV=-2047.0f;
	}
	if(CV>=2048.0f)
	{
		CV=2047.0f;
	}
	if(fabs(CV)<15.0f) CV=0;

	if(POT<=0.0f)
	{
		POT=0.0f;
	}
	if(POT>=4096.0f)
	{
		POT=4095.0f;
	}

	float result=0;
	//	if(POT>CV)
	{
		result = POT + CV;
//		result = POT;
		if(result>=4095.0f)
		{
			return (int32_t)(4095.0f*scale);
		}
		else if (result<0)
		{
			return 0;
		}
		else
		{
			return (int32_t)(result*scale);
		}

	}

}
//
static float get_level_f(float CV, float POT, float scale)
{

	CV = CV - 2048.0f-150.0f;
	if(CV<=-2048.0f)
	{
		CV=-2047.0f;
	}
	if(CV>=2048.0f)
	{
		CV=2047.0f;
	}

	if(POT<=0.0f)
	{
		POT=0.0f;
	}
	if(POT>=4096.0f)
	{
		POT=4095.0f;
	}

	float result=0;
	//	if(POT>CV)
	{
		result = POT + CV;
		if(result>=4095.0f)
		{
			return (4095.0f*scale);
		}
		else if (result<0)
		{
			return 0;
		}
		else
		{
			return (result*scale);
		}
	}
}


static uint32_t lastsync;
static void sync(void)
{
	debug_print("%s\n","Sync");
	if(isSYNC)
	{
		//		debug_print("%s\n","Sync is ON!");
		//		Controls[PB_SYNC]=ON;
		uint32_t current_time = HAL_GetTick();

		uint32_t new_sync_time = current_time -lastsync;
		lastsync=current_time;
		if(SYNC_PB)
		{
			SYNC_LED_ON;
		}
		if(SYNC_IN&&mode==BDEL_MODE_SIMPLE)
		{
			SYNC_LED_ON;
		}

		if (new_sync_time<SYNC_RESET_TIME)
		{

			tap_active = 0;// = mode==BDEL_MODE_NORMAL ? 0 : 1;
			if ((hDelay.TimeAlgo==DIRECT)||(hDelay.TimeAlgo==TAPPED))
			{
				div_active=0;
				latestADC= hDelay.TimeAlgo==DIRECT ? hDelay.delay_time : latestADC;
				hDelay.TimeAlgo= mode==BDEL_MODE_NORMAL ? KNOB_DIVMULT : TAPPED;

			}

//			if(new_sync_time>MAX_DELAY_TIME_MS)
//			{
//				new_sync_time=new_sync_time%MAX_DELAY_TIME_MS;
//			}

			uint32_t absdif=abs((signed int)new_sync_time-(signed int)hDelay.sync_time);
			if (absdif>1)
			{
//				debug_print("%s%d\r\n","new_sync_time:",new_sync_time);
				Sync_timestamp = current_time;
				if(reset==1)
				{
					hDelay.sync_time=new_sync_time;
					debug_print("%s%d\r\n","New tapped tempo:",new_sync_time);
					reset=0;
				}
				else
				{
					if(SYNC_IN&&mode==BDEL_MODE_SIMPLE)
					{
						hDelay.sync_time=new_sync_time;
						debug_print("%s%d\r\n","New tapped tempo:",new_sync_time);
					}
					else
					{
						hDelay.sync_time=(hDelay.sync_time+new_sync_time)/2;
						debug_print("%s%d\r\n","New tapped avg tempo:",hDelay.sync_time);
					}

				}
			}
		}
		else
		{
			reset=1;
		}
	}
	else
	{
		//		debug_print("%s\n","Sync is OFF!");
		//		Controls[PB_SYNC]=OFF;
		if(!SYNC_PB)
		{
			if(mode==BDEL_MODE_SIMPLE)
			{
				if(!SYNC_IN)	SYNC_LED_OFF;
			}
			else
			{
				SYNC_LED_OFF;
			}
		}

	}

}


static void rev_pb(void)
{
	//	debug_print("%s\n","Rev!");
	if(isREV)
	{
		Controls[PB_REV]=ON;
		switch(hDelay.Direction)
		{
		case DIRECTION_NORMAL:
		{
			hDelay.Direction=DIRECTION_REVERSE;
			FadeInOut_Init(&Fader_RevPressed,FADER_LENGTH);
			hDelay.isReverse =1;
			hDelay.virtual_head_ptr = hDelay.hRing.Tail; //Ring_Point_Offset_From_Head(&hDelay.hRing,-1*hDelay.hRing.Tail);
			signed int TailLength = hDelay.delay_time*hDelay.Fs;
			Ring_Sub_Init(&hDelay.hRevRing,&hDelay.hRing,TailLength);
			hDelay.hRevRing.pBuf = hDelay.hRing.Tail;
			hDelay.hRevRing.Tail = hDelay.hRing.Head;
			hDelay.hRevRing.Head = hDelay.hRing.Tail;
			REV_LED_ON;
			break;
		}
		case DIRECTION_REVERSE:
		{
			hDelay.Direction=DIRECTION_NORMAL;
			hDelay.isReverse =0;
			FadeInOut_Init(&Fader_RevPressed,FADER_LENGTH);
			REV_LED_OFF;
			break;
		}

		default:
		{
			Error_Handler();
			break;
		}
		}
	}
	else
	{
		Controls[PB_REV]=OFF;
		//		LED_REV_OFF;
	}
}
static void add_pb(void)
{
	//	debug_print("%s\n","Add!");
	if(isADD)
	{
		//		Controls[PB_ADD]=ON;
		switch(hDelay.Playback)
		{
		case PLAYBACK:
			break;
		case HOLD:
			debug_print("%s\n","ADD: ON");
			hDelay.Playback=ADD_TO_HOLD;
			hDelay.Add_Flag = 1;
			FadeInOut_Init(&hDelay.Add_FadeInOut,FADER_LENGTH);
			break;
		case ADD_TO_HOLD:
			break;
		}
	}
	else
	{
		//		Controls[PB_ADD]=OFF;
		switch(hDelay.Playback)
		{
		case PLAYBACK:
			break;
		case HOLD:
			break;
		case ADD_TO_HOLD:
			debug_print("%s\n","ADD: OFF");

			//setting flag to 2 will initiate fadein process which will turn state to HOLD later on.
			hDelay.Add_Flag = 2;
			FadeInOut_Init(&hDelay.Add_FadeInOut,FADER_LENGTH);
			break;
		}
	}
}

void pbpb(void)
{
	switch(hDelay.Playback)
	{
	case PLAYBACK:
		debug_print("%s\n","HOLD: ON");
		if(hDelay.Direction==DIRECTION_NORMAL)
		{
			hDelay.Playback=HOLD;
			FadeInOut_Init(&Fader_HoldPressed, FADER_LENGTH);
			signed int TailLength = hDelay.delay_time*hDelay.Fs;
			Ring_Sub_Init(&hDelay.hSubRing, &hDelay.hRing, TailLength );
		}
		else
		{
			hDelay.Playback=HOLD;
			FadeInOut_Init(&Fader_HoldPressed,FADER_LENGTH);
			signed int TailLength = hDelay.delay_time*hDelay.Fs;
			if(!hDelay.isReverse)
			{
				Ring_Sub_Init(&hDelay.hSubRing, &hDelay.hRing, TailLength );
			}
			else
			{
				//						Ring_Swap(&hDelay.hRing);
				reverse_toggle();
				Ring_Sub_Init(&hDelay.hSubRing, &hDelay.hRing, TailLength );
				//						Ring_Swap(&hDelay.hRing);
			}
		}
		break;
	case HOLD:
		hDelay.Playback=PLAYBACK;
		hDelay.Hold_Flag=2;
		FadeInOut_Init(&Fader_HoldPressed, FADER_LENGTH);
		hDelay.hRing.hParent=(Ring_Handle_TypeDef *)NULL;
		debug_print("%s\n","HOLD: OFF");
		break;
	case ADD_TO_HOLD:
		hDelay.Add_Flag=3;
		FadeInOut_Init(&hDelay.Add_FadeInOut,FADER_LENGTH);
		hDelay.Playback=ADD_TO_HOLD;
		FadeInOut_Init(&Output_FadeInOut,100);
		debug_print("%s\n","HOLD: OFF");
		break;
	}
	Controls[PB_HOLD]=ON;
}

static uint8_t last_input = 0;
static int last_type = 0;
static void hold_pb(void)
{

	if(HOLD_PB)
	{
		last_input = 0;
		pbpb();

	}
	else if (HOLD_IN)
	{

		if(hDelay.Playback!=PLAYBACK)
		{

			Controls[PB_HOLD]=ON;
			hDelay.hSubRing.Head=hDelay.hSubRing.pBuf;
			signed int TailLength = hDelay.delay_time*hDelay.Fs;
			Ring_Set_Tail(&hDelay.hSubRing,TailLength);

			hDelay.hRevRing.Tail = hDelay.hSubRing.pBuf;
		}
		else
		{

			pbpb();
			last_input=1;
		}

	}
	else
	{

		Controls[PB_HOLD]=OFF;
//		if(last_input==0) Controls[PB_HOLD]=OFF;
	}
//
//	static uint8_t last_input = 0;
//	debug_print("%s\n","HOLD");
//	//	debug_print("%s\n","Hold!");
//	if(isHOLD)
//	{
//		if(Controls[PB_HOLD]==OFF)
//		{
//			if(HOLD_IN)
//			{
//				last_input = 1;
//			}
//			else
//			{
//				last_input=0;
//			}
//
//			switch(hDelay.Playback)
//			{
//			case PLAYBACK:
//				debug_print("%s\n","HOLD: ON");
//				if(hDelay.Direction==DIRECTION_NORMAL)
//				{
//					hDelay.Playback=HOLD;
//					FadeInOut_Init(&Fader_HoldPressed, FADER_LENGTH);
//					signed int TailLength = hDelay.delay_time*hDelay.Fs;
//					Ring_Sub_Init(&hDelay.hSubRing, &hDelay.hRing, TailLength );
//				}
//				else
//				{
//					hDelay.Playback=HOLD;
//					FadeInOut_Init(&Fader_HoldPressed,FADER_LENGTH);
//					signed int TailLength = hDelay.delay_time*hDelay.Fs;
//					if(!hDelay.isReverse)
//					{
//						Ring_Sub_Init(&hDelay.hSubRing, &hDelay.hRing, TailLength );
//					}
//					else
//					{
//						//						Ring_Swap(&hDelay.hRing);
//						reverse_toggle();
//						Ring_Sub_Init(&hDelay.hSubRing, &hDelay.hRing, TailLength );
//						//						Ring_Swap(&hDelay.hRing);
//					}
//				}
//				break;
//			case HOLD:
//				hDelay.Playback=PLAYBACK;
//				hDelay.Hold_Flag=2;
//				FadeInOut_Init(&Fader_HoldPressed, FADER_LENGTH);
//				hDelay.hRing.hParent=(Ring_Handle_TypeDef *)NULL;
//				debug_print("%s\n","HOLD: OFF");
//				break;
//			case ADD_TO_HOLD:
//				hDelay.Add_Flag=3;
//				FadeInOut_Init(&hDelay.Add_FadeInOut,FADER_LENGTH);
//				hDelay.Playback=ADD_TO_HOLD;
//				FadeInOut_Init(&Output_FadeInOut,100);
//				debug_print("%s\n","HOLD: OFF");
//				break;
//			}
//			Controls[PB_HOLD]=ON;
//		}
//	}
//	else
//	{
//		if(last_input)
//		{
//			hDelay.hSubRing.Head=hDelay.hSubRing.pBuf;
//			signed int TailLength = hDelay.delay_time*hDelay.Fs;
//			Ring_Set_Tail(&hDelay.hSubRing,TailLength);
//		}
//		else
//		{
//			Controls[PB_HOLD]=OFF;
//		}
//	}
}


static void tapeordigi(void)
{
	//	debug_print("%s\n","SW1!");
	if(DELAY_TYPE_PB)
	{
		debug_print("%s\n","MODE: DIGI");
		Controls[SW_DEL_TYPE]=ON;
		hDelay.mode = DELAY_TAPE;

	}
	else
	{
		debug_print("%s\n","MODE: TAPE");
		Controls[SW_DEL_TYPE]=OFF;
		hDelay.mode = DELAY_Digital;
	}
}

static void pingpong(void)
{
	//	debug_print("%s\n","SW1!");
	if(PINGPONG_PB)
	{
		debug_print("%s\n","PP");
		Controls[SW_PP]=ON;
		Feedback.type = FEEDBACK_PING_PONG;

	}
	else
	{
		debug_print("%s\n","No PP");
		Controls[SW_PP]=OFF;
		Feedback.type = FEEDBACK_DIRECT;
	}
}


void IO_IT_Callback(IO_ENUM_TypeDef num, IO_States Current_state)
{
	switch(num)
	{
	case SYNC_PB_ENUM:
	case SYNC_IN_ENUM:
		sync();
		break;
	case ADD_PB_ENUM:
	case ADD_IN_ENUM:
		add_pb();
		break;
	case HOLD_PB_ENUM:
	case HOLD_IN_ENUM:
		hold_pb();
		break;
	case REV_PB_ENUM:
	case REV_IN_ENUM:
		rev_pb();
		break;
	case PINGPONG_PB_ENUM:
		pingpong();
		break;
	case DELAY_TYPE_ENUM:
		tapeordigi();
		break;
	}
}


/*
 * BDEL.h
 *
 *  Created on: 24 Oct 2018
 *      Author: Tim
 */

#ifndef TESTS_BDEL_H_
#define TESTS_BDEL_H_

#include "debug.h"
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include <stdlib.h>
#include <BDEL1_Conf.h>

//#include "BDSP_RevA_Configuration.h"
#include "OD_Outs.h"
#include "adc.h"
#include "i2c.h"
#include "codec.h"
#include "gpio.h"
#include "memory.h"
#include "timers.h"
//#include "debug.h"
void BDEL_init(void);


//static void TIM2_init(void);
//static void TIM2_start(void);

void BDEL_Run_Full_Test(void);
int codecs_test(void);
static void leds_test(uint32_t speed);
int adc_tests(uint8_t ADC_Channel);

#endif /* TESTS_BDEL_H_ */

/*
 * BDEL.c
 *
 *  Created on: 24 Oct 2018
 *      Author: Tim
 */

#include <BDEL.h>

static float adc_vals[4];
I2C_HandleTypeDef hi2c;
uint8_t datareg[5];
Memory_Handle_TypeDef RAM_Handle;

uint32_t testval;

void BDEL_init(void)
{
	OD_Init_all();
	ADC_PrepareChannel(CV_FEEDBACK);
	ADC_PrepareChannel(CV_TIME);
	ADC_PrepareChannel(POT_FEEDBACK);
	ADC_PrepareChannel(POT_TIME);

	ADC_Init();
	ADC_start();

	TIM2_init();
	TIM2_start();

	I2C_Init(&hi2c);
	Codec_configure_CodecA();
	Codec_start_CodecA();

	RAM_Handle.MemoryType = MEM_TYPE_SDRAM;
	RAM_Handle.Addr_max=4000000;
	RAM_Handle.Read_addr=1;
	RAM_Handle.Write_addr=0;
	RAM_Handle.offset=4000000;
	Memory_RAM_Init(&RAM_Handle);
	Memory_Clean(&RAM_Handle);

	IO_configure(SYNC_IN_ENUM,IO_IS_IT);
	IO_configure(SYNC_PB_ENUM,IO_IS_IT);
	IO_configure(ADD_PB_ENUM,IO_IS_IT);
	IO_configure(ADD_IN_ENUM,IO_IS_IT);
	IO_configure(HOLD_PB_ENUM,IO_IS_IT);
	IO_configure(HOLD_IN_ENUM,IO_IS_IT);
	IO_configure(REV_PB_ENUM,IO_IS_IT);
	IO_configure(REV_IN_ENUM,IO_IS_IT);
	IO_configure(PINGPONG_PB_ENUM,IO_IS_INPUT);
	IO_configure(DELAY_TYPE_ENUM,IO_IS_INPUT);
	IO_InitAll();
//
//	I2C_Read_register(0x20,0x4,&datareg[0],&hi2c);
//	I2C_Read_register(0x20,0x5,&datareg[1],&hi2c);
//	I2C_Read_register(0x20,0x6,&datareg[2],&hi2c);
//	I2C_Read_register(0x20,0x1,&datareg[3],&hi2c);
//	I2C_Read_register(0x20,0x7,&datareg[4],&hi2c);

//    int i=0;
//	while(1)
//	{
//		signed int ReadA_R = CodecA_Read_Val_24b();
//		signed int ReadA_L = CodecA_Read_Val_24b();
//		CodecA_Write_Val_24b(ReadA_R);
//		CodecA_Write_Val_24b(ReadA_L);
//	}
//	I2C_scan_dbg(&hi2c);

//	while(1)
//	{
//		leds_test(3000);
//	}
}


void BDEL_Run_Full_Test(void)
{
//	while(1)
	{
		leds_test(50);
	}

	debug_print("%s\n","Press Any key to start next test.");
//	Debug_getchar();
	HAL_Delay(500);
	if(codecs_test()!=0)
	{
		while(1){
			OD_All_Toggle();
			HAL_Delay(500);
		}
	}
	if(memory_test(&RAM_Handle)==1)
	{
		OD_ON(LED_HOLD);
	}
	else
	{
		while(1){OD_All_Toggle();
		HAL_Delay(500);}
	}
	OD_blink_blocking(150,4);

	adc_tests(POT_TIME_NUMBER);
	OD_blink_blocking(150,4);
	adc_tests(POT_FEEDBACK_NUMBER);
	OD_blink_blocking(150,4);
	adc_tests(CV_TIME_NUMBER);
	OD_blink_blocking(150,4);
	adc_tests(CV_FEEDBACK_NUMBER);
	OD_blink_blocking(150,4);





	debug_print("%s\n","Test End!");
}


/*
 * this test should blink all LEDs at once
 * */
static void leds_test(uint32_t speed)
{
	debug_print("%s\n","leds_test");
	debug_print("%s\n","All LEDs should blink for 16 times now");
	debug_print("%s\n","If this does not happens - inspect U9 or U3, Y1. Test OD_x outputs manually with tester");
	OD_blink_blocking(speed,16);

}

/*	codecs_test
	HAL_Delay(10);
	Please send some full-range sine wave to the Left-In
	Please Have audio/visual control at the L&R outputs
	TAP LED will light if codecA registers are OK
	REV LED will light if codecA audio SEEMS to be OK
	HOLD LED will light if codecB registers are  OK"
	ADD LED will light if codecB audio SEEMS to be OK
*/
int codecs_test(void)
{
	uint8_t err=0;
	OD_All_OFF();

	HAL_Delay(10);
//	Debug_getchar();
	if(Codecs_test(1)==1) OD_ON(LED_SYNC);
	else err++;
	if(Codecs_test(2)==1) OD_ON(LED_REV);
	else err++;
	if(Codecs_test(3)==1) OD_ON(LED_ADD);
	else err++;

	return err;
}

int adc_tests(uint8_t ADC_Channel)
{
	uint8_t cmplt=1;
	int8_t condition1=0;
	int8_t condition2=0;
	int8_t condition3=0;
	int8_t condition4=0;
	int8_t condition5=0;
	OD_All_OFF();
	while(cmplt)
	{
		OD_All_OFF();
		if(adc_vals[ADC_Channel]<1000)
		{
			OD_All_OFF();
			condition1=1;
		}
		else if(adc_vals[ADC_Channel]<2000)
		{
			OD_ON(LED_HOLD);
			condition2=1;
		}
		else if(adc_vals[ADC_Channel]<3000)
		{

			OD_ON(LED_HOLD);
			OD_ON(LED_ADD);
			condition3=1;
		}
		else if(adc_vals[ADC_Channel]<4000)
		{

			OD_ON(LED_HOLD);
			OD_ON(LED_ADD);
			OD_ON(LED_REV);
			condition4=1;
		}
		else
		{
			OD_ON(LED_HOLD);
			OD_ON(LED_ADD);
			OD_ON(LED_REV);
			OD_ON(LED_SYNC);
			condition5=1;
		}
		if(condition1) if(condition2) if(condition3) if(condition4)
			if(condition5) cmplt=0;
	}


	return 0;
}

//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
void TIM_Callback(TIM_HandleTypeDef *htim)
{

	if (htim->Instance == TIM2)
	{
		ADC_Val_Update(adc_vals);
	}
}


//static void TIM2_init(void)
//{
//	//uint16_t uwPrescalerValue = 0;
//
//	//uwPrescalerValue = (uint32_t) ((SystemCoreClock /2) / 10000) - 1;
//
//	TIM2_handler.Instance               = TIM2;
//	TIM2_handler.Init.Prescaler         = 30;
//	TIM2_handler.Init.CounterMode       = TIM_COUNTERMODE_UP;
//	TIM2_handler.Init.Period            = 100;
//	TIM2_handler.Init.ClockDivision     = TIM_CLOCKDIVISION_DIV1;
//	//TIM2_handler.Init.RepetitionCounter = ;
//	//TIM2_handler.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
//
//	HAL_TIM_Base_Init(&TIM2_handler);
//}
//
//static void TIM2_start(void)
//{
//	HAL_TIM_Base_Start_IT(&TIM2_handler);
//}


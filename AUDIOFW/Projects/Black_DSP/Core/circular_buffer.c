/*
 * circular_buffer.c
 *
 *  Created on: 8 ????. 2019 ?.
 *      Author: Tim
 */

#include "circular_buffer.h"

void ring_buffer_init(Ring_Handle_TypeDef * pRing, float * ptr, uint32_t size)
{
	assert_param(size>0);
	assert_param(ptr!=0);
	assert_param(pRing!=0);

	pRing->pBuf=ptr;
	pRing->size=size;
	pRing->Head=pRing->pBuf+1;
	pRing->Tail=pRing->pBuf;
//	pRing->Dir=RING_DIR_CW;
	pRing->hParent = NULL;
}

float * Ring_Move_Tail(Ring_Handle_TypeDef * pRing, signed int Positions)
{
	float * retval;
//	if(pRing->hSub_Buffer!=NULL)
//	{
//		Ring_Move_Tail(pRing->hSub_Buffer,Positions);
//	}
	retval = point_index(pRing,pRing->Tail,Positions);
	pRing->Tail = retval;
	return retval;
}

//uint32_t * Ring_buf_(Ring_Handle_TypeDef * pRing)
//{
//	return
//}

float * Ring_Head_Move(Ring_Handle_TypeDef * pRing, signed int Positions)
{
	pRing->Head = point_index(pRing,pRing->Head,Positions);
	return pRing->Head;
}

float * Ring_Ptr_Move(Ring_Handle_TypeDef * pRing, signed int Positions, float * Ptr)
{
//	assert_param(abs(Positions)<pRing->size);
//	assert_param(pRing->size>0);

//	uint32_t * retval = point_index(pRing,Ptr,Positions);
	Ptr =  point_index(pRing,Ptr,Positions);
//	set_youngest_head(pRing,retval);
	return Ptr;
}

void Ring_Move(Ring_Handle_TypeDef * pRing, signed int Positions)
{
	assert_param(Positions<pRing->size);
	assert_param(pRing->size>0);


	Ring_Head_Move(pRing,Positions);
	Ring_Move_Tail(pRing,Positions);
}


float * Ring_Set_Tail(Ring_Handle_TypeDef * pRing, signed int Length)
{
	float * retval;
//	assert_param(abs(Length)<=pRing->size);
	Length = Length % pRing->size;
	retval = point_index(pRing,pRing->Head,-1*Length);

	pRing->Tail = retval;
//	set_youngest_tail(pRing,retval);
	return retval;
}

float * Ring_Set_Tail_From_Ptr(Ring_Handle_TypeDef * pRing, signed int Length, float * Ptr)
{
	float * retval;
	assert_param(abs(Length)<=pRing->size);
	retval = point_index(pRing,Ptr,-1*Length);

	pRing->Tail = retval;
//	set_youngest_tail(pRing,retval);
	return retval;
}

static inline void set_youngest_tail(Ring_Handle_TypeDef * pRing, float * Ptr)
{
	if(pRing->hParent!=NULL)
	{
		set_youngest_tail(pRing->hParent,Ptr);
	}
	else
	{
		pRing->Tail=Ptr;
	}
}

static inline void set_youngest_head(Ring_Handle_TypeDef * pRing, float * Ptr)
{
	if(pRing->hParent!=NULL)
	{
		set_youngest_head(pRing->hParent,Ptr);
	}
	else
	{
		pRing->Head=Ptr;
	}
}

static inline void set_youngest_pBuf(Ring_Handle_TypeDef * pRing, float * Ptr)
{
	if(pRing->hParent!=NULL)
	{
		set_youngest_pBuf(pRing->hParent,Ptr);
	}
	else
	{
		pRing->pBuf=Ptr;
	}
}

float * Ring_Point_Offset_From_Tail(Ring_Handle_TypeDef * pRing, int offset)
{
	float * retval = point_index(pRing,pRing->Tail,offset);
	return retval;
}

float * Ring_Point_Offset_From_Head(Ring_Handle_TypeDef * pRing, int offset)
{
	float * retval = point_index(pRing,pRing->Head,offset);
	return retval;
}

float * Ring_Point_Offset_Ptr(Ring_Handle_TypeDef * pRing, int offset, float * Ptr)
{
	return point_index(pRing,Ptr,offset);
}

void Ring_Swap(Ring_Handle_TypeDef * pRing)
{
	float * temp = pRing->Tail;
//	pRing->Tail = pRing->Head;
	pRing->Head = temp;
}


void Ring_Sub_Init(Ring_Handle_TypeDef * pRing, Ring_Handle_TypeDef * pParent, signed int TailLength)
{
	assert_param(abs(TailLength)<pParent->size);

	pRing->size = abs(TailLength);
	pRing->pBuf = pParent->Head;
	pRing->Head = pParent->Head;
	pRing->Tail = pParent->Tail;
	pRing->hParent = pParent;

}

//static inline uint32_t * point_index(Ring_Handle_TypeDef * pRing, uint32_t * FromAddr, uint32_t offset)
//{
//	uint32_t * retval;
//	uint32_t index;
//	if(pRing->hParent!=NULL)
//	{
//		FromAddr = point_index(pRing->hParent,FromAddr,offset);
//		index = (FromAddr - pRing->pBuf); //head index
//		index = (index + pRing->size ) % pRing->size;
//		retval = &pRing->pBuf[index];
//
//	}
//	else
//	{
//		index = (FromAddr - pRing->pBuf); //head index
//		index = (index + (pRing->size + offset)) % pRing->size;
//		retval = &pRing->pBuf[index];
//	}
//
//
//	return retval;
//}

static inline float * point_index(Ring_Handle_TypeDef * pRing, float * FromAddr, signed int offset)
{
	float * retval;
	uint32_t index;

//	if(pRing->hParent!=NULL)
//	{
//		index = FromAddr - pRing->hParent->pBuf;
//		index = index % (pRing->hParent->pBuf - pRing->pBuf);
//		FromAddr = &pRing->pBuf[index];
//	}
	if(pRing->hParent!=NULL)
	{
		if(FromAddr<pRing->pBuf)
		{
			uint32_t temp_index;
			temp_index = FromAddr - pRing->hParent->pBuf;
			temp_index = pRing->hParent->size+temp_index;
			FromAddr = &pRing->hParent->pBuf[temp_index];
		}
	}

	index = (FromAddr - pRing->pBuf); //head index
	index = (index + (pRing->size + offset)) % pRing->size;
//	assert_param(index<=pRing->size);
	retval = &pRing->pBuf[index];

	if(pRing->hParent!=NULL)
	{
		retval = point_index(pRing->hParent,pRing->pBuf,index);
	}


	return retval;
}

/*
 * circular_buffer.h
 *
 *  Created on: 8 ????. 2019 ?.
 *      Author: Tim
 */

#ifndef BSP_BDSP_REVA_CIRCULAR_BUFFER_H_
#define BSP_BDSP_REVA_CIRCULAR_BUFFER_H_

#include "stdint.h"
#include "stdlib.h"
#include "debug.h"

typedef enum {

	RING_DIR_CW=0,
	RING_DIR_CCW,
	RING_DIR_NUM
}Ring_Dir_TypeDef;

typedef struct Ring_Handle_TypeDef
{
	float *pBuf;
	size_t  size;
	float  *Head;
	float  *Tail;
	struct Ring_Handle_TypeDef * hParent;

}Ring_Handle_TypeDef;

typedef struct RingSub_Handle
{
	uint32_t *pBuf;
	uint32_t  size;
	uint32_t  *Head;
	uint32_t  *Tail;
	Ring_Handle_TypeDef *pRing;

}RingSub_Handle_TypeDef;

void ring_buffer_init(Ring_Handle_TypeDef * pRing, float * ptr, uint32_t size);
float * Ring_Move_Tail(Ring_Handle_TypeDef * pRing, signed int Positions);
float * Ring_Head_Move(Ring_Handle_TypeDef * pRing, signed int Positions);
void Ring_Move(Ring_Handle_TypeDef * pRing, signed int Positions);
float * Ring_Set_Tail(Ring_Handle_TypeDef * pRing, signed int Length);
float * Ring_Point_Offset_From_Tail(Ring_Handle_TypeDef * pRing, int offset);
void Ring_Swap(Ring_Handle_TypeDef * pRing);
float * Ring_Point_Offset_From_Head(Ring_Handle_TypeDef * pRing, int offset);
void Ring_Sub_Init(Ring_Handle_TypeDef * pRing, Ring_Handle_TypeDef * pParent, signed int TailLength);
static inline float * point_index(Ring_Handle_TypeDef * pRing, float * FromAddr, signed int offset);
float * Ring_Point_Offset_Ptr(Ring_Handle_TypeDef * pRing, int offset, float * Ptr);
float * Ring_Ptr_Move(Ring_Handle_TypeDef * pRing, signed int Positions, float * Ptr);
float * Ring_Set_Tail_From_Ptr(Ring_Handle_TypeDef * pRing, signed int Length, float * Ptr);

static inline void set_youngest_head(Ring_Handle_TypeDef * pRing, float * Ptr);
static inline void set_youngest_tail(Ring_Handle_TypeDef * pRing, float * Ptr);
static inline void set_youngest_pBuf(Ring_Handle_TypeDef * pRing, float * Ptr);

#endif /* BSP_BDSP_REVA_CIRCULAR_BUFFER_H_ */

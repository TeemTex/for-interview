/*
 * debug.c
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: Teem
 */

#include "debug.h"

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */


char Debug_getchar(void)
{
	char input;
	scanf(" %c", &input);

	return input;
}

uint32_t Debug_gethex(void)
{
	int input;
	scanf(" %x", &input);

	return input;
}

void _Error_Handler(char *file, int line)
{

	printf("Error: file %s on line %d\r\n", file, line);
//  while(1)
//  {
//
//  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
#ifdef DEBUG
	printf("Wrong parameters value: file %s on line %d\r\n", file, line);
#else
	while(0);
#endif

}

#endif

/*
 * debug.h
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: Teem
 */


#ifndef DEBUG_H_
#define DEBUG_H_

#define DEBUG 0

#include "stdint.h"
#include "stdio.h"

#define debug_print(fmt, ...) do {\
	if (DEBUG) { printf(fmt, __VA_ARGS__);\
	fflush(stdout);\
	}\
            } while (0)

#define Error_Handler() \
	do { if (DEBUG) _Error_Handler(__FILE__, __LINE__); } while(0)

void _Error_Handler(char *, int);
void assert_failed(uint8_t* file, uint32_t line);



#define USE_FULL_ASSERT    0U

#ifdef  USE_FULL_ASSERT

  #define assert_param(expr) ((expr) ? (void)0U : assert_failed((uint8_t *)__FILE__, __LINE__))
/* Exported functions ------------------------------------------------------- */
  void assert_failed(uint8_t* file, uint32_t line);
#else
  #define assert_param(expr) ((void)0U)
#endif /* USE_FULL_ASSERT */

char Debug_getchar(void);
uint32_t Debug_gethex(void);

#endif /* DEBUG_H_ */

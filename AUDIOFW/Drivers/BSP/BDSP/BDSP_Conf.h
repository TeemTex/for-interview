/*
 * BDSP_RevA_Configuration.h
 *
 *  Created on: 23 Oct 2018
 *      Author: Tim
 */

#ifndef BDSP_REVA_CONFIGURATION_H_
#define BDSP_REVA_CONFIGURATION_H_

#include "stm32f4xx_hal.h"

#define I2S_SAMPLING_FREQ       I2S_AUDIOFREQ_48K
#define I2S_DATA_FORMAT         I2S_DATAFORMAT_24B
#define I2S_STANDARD            I2S_STANDARD_PHILIPS
#define CODEC_CS4271 1

/*
	BUILT IN ADC PERIPH DEFINITON
*/

#define KNOB_MIN_CHANGE				0.7f
#define KNOB_TRACK_FOR				9000
#define KNOB_LPF_COEF				1.0f/1000.0f
#define CV_LPF_COEF					1.0f/25.0f
#define CV_MIN_CHANGE				15.0f
#define CV_TRACK_FOR				250

/*
	SDRAM
*/
#define SDRAM_CLK_90MHZ 0x1
#define SDRAM_CLK SDRAM_CLK_90MHZ

/*
	GPIO PIN DEFINITIONS
*/

#define IO_NVIC_PRIO 					10
#define IO_NVIC_SUBPRIO 				0

#define IO_INT0						EXTI2_IRQn
#define IO_INT1						EXTI3_IRQn
#define IO_INT2						EXTI4_IRQn
#define IO_INT3						EXTI9_5_IRQn
#define IO_INT4						EXTI15_10_IRQn

#define IO0_PIN 					GPIO_PIN_4
#define IO0_PORT					GPIOB
#define IO0_IRQ						IO_INT2

#define IO1_PIN 					GPIO_PIN_2
#define IO1_PORT					GPIOB
#define IO1_IRQ						IO_INT0

#define IO2_PIN 					GPIO_PIN_11
#define IO2_PORT					GPIOD
#define IO2_IRQ						IO_INT4

#define IO3_PIN 					GPIO_PIN_13
#define IO3_PORT					GPIOD
#define IO3_IRQ						IO_INT4

#define IO4_PIN 					GPIO_PIN_3
#define IO4_PORT					GPIOG
#define IO4_IRQ						IO_INT1

#define IO5_PIN 					GPIO_PIN_6
#define IO5_PORT					GPIOG
#define IO5_IRQ						IO_INT3

#define IO6_PIN 					GPIO_PIN_7
#define IO6_PORT					GPIOG
#define IO6_IRQ						IO_INT3

#define IO7_PIN 					GPIO_PIN_7
#define IO7_PORT					GPIOC
#define IO7_IRQ						IO_INT3

#define IO8_PIN 					GPIO_PIN_9
#define IO8_PORT					GPIOG
#define IO8_IRQ						IO_INT3

#define IO9_PIN 					GPIO_PIN_7
#define IO9_PORT					GPIOD
#define IO9_IRQ						IO_INT3

#define IO10_PIN 					GPIO_PIN_6
#define IO10_PORT					GPIOD
#define IO10_IRQ					IO_INT3

#define IO11_PIN 					GPIO_PIN_5
#define IO11_PORT					GPIOD
#define IO11_IRQ					IO_INT3

#define IO12_PIN 					GPIO_PIN_4
#define IO12_PORT					GPIOD
#define IO12_IRQ					IO_INT2

#define IO13_PIN 					GPIO_PIN_3
#define IO13_PORT					GPIOD
#define IO13_IRQ					IO_INT1

#define IO14_PIN 					GPIO_PIN_15
#define IO14_PORT					GPIOA
#define IO14_IRQ					IO_INT4

#define IO15_PIN 					GPIO_PIN_8
#define IO15_PORT					GPIOA
#define IO15_IRQ					IO_INT3

#define IO0_ENABLE			IO0_PORT->BSRR = IO0_PIN
#define IO0_DISABLE			IO0_PORT->BRR = IO0_PIN
#define IO0					((IO0_PORT->IDR & IO0_PIN))

#define IO1_ENABLE			IO1_PORT->BSRR = IO1_PIN
#define IO1_DISABLE			IO1_PORT->BRR = IO1_PIN
#define IO1					((IO1_PORT->IDR & IO1_PIN))

#define IO2_ENABLE			IO2_PORT->BSRR = IO2_PIN
#define IO2_DISABLE			IO2_PORT->BRR = IO2_PIN
#define IO2					((IO2_PORT->IDR & IO2_PIN))

#define IO3_ENABLE			IO3_PORT->BSRR = IO3_PIN
#define IO3_DISABLE			IO3_PORT->BRR = IO3_PIN
#define IO3					((IO3_PORT->IDR & IO3_PIN))

#define IO4_ENABLE			IO4_PORT->BSRR = IO4_PIN
#define IO4_DISABLE			IO4_PORT->BRR = IO4_PIN
#define IO4					((IO4_PORT->IDR & IO4_PIN))

#define IO5_ENABLE			IO5_PORT->BSRR = IO5_PIN
#define IO5_DISABLE			IO5_PORT->BRR = IO5_PIN
#define IO5					((IO5_PORT->IDR & IO5_PIN))

#define IO6_ENABLE			IO6_PORT->BSRR = IO6_PIN
#define IO6_DISABLE			IO6_PORT->BRR = IO6_PIN
#define IO6					((IO6_PORT->IDR & IO6_PIN))

#define IO7_ENABLE			IO7_PORT->BSRR = IO7_PIN
#define IO7_DISABLE			IO7_PORT->BRR = IO7_PIN
#define IO7					((IO7_PORT->IDR & IO7_PIN))

#define IO8_ENABLE			IO8_PORT->BSRR = IO8_PIN
#define IO8_DISABLE			IO8_PORT->BRR = IO8_PIN
#define IO8					((IO8_PORT->IDR & IO8_PIN))

#define IO9_ENABLE			IO9_PORT->BSRR = IO9_PIN
#define IO9_DISABLE			IO9_PORT->BRR = IO9_PIN
#define IO9					((IO9_PORT->IDR & IO9_PIN))

#define IO10_ENABLE			IO10_PORT->BSRR = IO10_PIN
#define IO10_DISABLE			IO10_PORT->BRR = IO10_PIN
#define IO10					((IO10_PORT->IDR & IO10_PIN))

#define IO11_ENABLE			IO11_PORT->BSRR = IO11_PIN
#define IO11_DISABLE			IO11_PORT->BRR = IO11_PIN
#define IO11					((IO11_PORT->IDR & IO11_PIN))

#define IO12_ENABLE			IO12_PORT->BSRR = IO12_PIN
#define IO12_DISABLE			IO12_PORT->BRR = IO12_PIN
#define IO12					((IO12_PORT->IDR & IO12_PIN))

#define IO13_ENABLE			IO13_PORT->BSRR = IO13_PIN
#define IO13_DISABLE			IO13_PORT->BRR = IO13_PIN
#define IO13					((IO13_PORT->IDR & IO13_PIN))

#define IO14_ENABLE			IO14_PORT->BSRR = IO14_PIN
#define IO14_DISABLE			IO14_PORT->BRR = IO14_PIN
#define IO14					((IO14_PORT->IDR & IO14_PIN))

#define IO15_ENABLE			IO15_PORT->BSRR = IO15_PIN
#define IO15_DISABLE			IO15_PORT->BRR = IO15_PIN
#define IO15					((IO15_PORT->IDR & IO15_PIN))

#define DIRECT_IO0_PIN				GPIO_PIN_10
#define DIRECT_IO0_PORT				GPIOG
#define DIRECT_IO1_PIN				GPIO_PIN_15
#define DIRECT_IO1_PORT				GPIOC
#define DIRECT_IO2_PIN				GPIO_PIN_14
#define DIRECT_IO2_PORT				GPIOC
#define DIRECT_IO3_PIN				GPIO_PIN_10
#define DIRECT_IO3_PORT				GPIOF
#define DIRECT_IO4_PIN				GPIO_PIN_12
#define DIRECT_IO4_PORT				GPIOD
#define DIRECT_IO5_PIN				GPIO_PIN_11
#define DIRECT_IO5_PORT				GPIOB

#define CODEC_RST_PIN				GPIO_PIN_9
#define CODEC_RST_PORT				GPIOB

/*
	I2C DEFINITIONS
*/
#define I2C_SCL_PIN					GPIO_PIN_8
#define I2C_SDA_PIN					GPIO_PIN_7
#define I2C_PORT					GPIOB



#endif /* BDSP_REVA_CONFIGURATION_H_ */

/*
 * BDSP_Init.h
 *
 *  Created on: 3 ???. 2019 ?.
 *      Author: Tim
 */

#ifndef BSP_BDSP_INIT_H_
#define BSP_BDSP_INIT_H_

#include <BSP_Base.h>
#include <BDSP/BDSP_Conf.h>
#include "BSP_Debug.h"

void BDSP_Init_SysClk(void);


#endif /* BSP_BDSP_INIT_H_ */

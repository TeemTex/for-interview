/*
 * timers.h
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#ifndef BDSP_REVA_TIMERS_H_
#define BDSP_REVA_TIMERS_H_

#include <BDSP/BDSP_Conf.h>
#include "stm32f4xx_hal.h"
//#include <BDEL1_Conf.h>

void TIM2_init(void);
void TIM2_start(void);

void TIM_Callback(TIM_HandleTypeDef *htim);
//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);


#endif /* BDSP_REVA_TIMERS_H_ */

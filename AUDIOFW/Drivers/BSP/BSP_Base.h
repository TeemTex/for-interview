/*
 * BDSP_BSP_Base.h
 *
 *  Created on: 25 Oct 2018
 *      Author: Tim
 */

#ifndef BSP_BDSP_BDSP_BSP_BASE_H_
#define BSP_BDSP_BDSP_BSP_BASE_H_

#include "stm32f4xx_hal.h"
#include "BSP_Debug.h"

//#include "../leds.h"
//
//typedef struct
//{
//	BSP_Led_Struct 		*pLeds;
////	GPIO_TypeDef		*GPIOx;
////	GPIO_InitTypeDef 	GPIO_init;
////	IRQn_Type			IRQn;
////	uint8_t  			IRQ_enable;
////	uint8_t				NVIC_Prio;
////	uint8_t				NVIC_SubPrio;
//
//}BDSP_BSP_Struct;


#define I2S_INPUT_SAMPLES_PER_BUFFER (48)
#define I2S_INPUT_BUFFER_SIZE   I2S_INPUT_SAMPLES_PER_BUFFER
#define I2S_INPUT_SAMPLES_NUM   I2S_INPUT_BUFFER_SIZE*2

typedef enum {
  IO_BUFFER_STATE_NOT_INITED=0,
  IO_BUFFER_STATE_EMPTY,
  IO_BUFFER_STATE_REFILLING,
  IO_BUFFER_STATE_FULL,
  IO_BUFFER_STATE_OVERFLOW,
  BUFFER_READY,
  BUFFER_BUSY
} IO_Buffer_StateTypeDef;

typedef enum {
	IO_BUFFER_TYPE_RX = 0,
	IO_BUFFER_TYPE_TX
} IO_Buffer_Type;

typedef enum {
  BUFFER_OFFSET_NONE = 0,
  BUFFER_OFFSET_HALF,
  BUFFER_OFFSET_FULL,
} DMA_Buff_StateTypeDef;


//ToDo: This struct should be revised and defined as IO_buffer.c..
typedef struct {
  //Size parameter means the number of 16-bit data length, HAL multiples it by itself
  uint16_t					buffer_tx[I2S_INPUT_BUFFER_SIZE*2+I2S_INPUT_BUFFER_SIZE*2];
  uint16_t					IO_buffer_tx[I2S_INPUT_BUFFER_SIZE*2];
  uint16_t					buffer_rx[I2S_INPUT_BUFFER_SIZE*2+I2S_INPUT_BUFFER_SIZE*2];
  uint16_t					IO_buffer_rx[I2S_INPUT_BUFFER_SIZE*2];
  DMA_Buff_StateTypeDef		DMA_state_tx;
  DMA_Buff_StateTypeDef		DMA_state_rx;
  uint32_t					IO_index_tx;
  uint32_t					IO_index_rx;
  __IO IO_Buffer_StateTypeDef 	IO_state_tx;
  __IO IO_Buffer_StateTypeDef 	IO_state_rx;
  uint16_t            		*sdram;//output buffer


} Codec_Data_TypeDef;

typedef struct Codec_i2s_Handle
{
  I2S_HandleTypeDef   i2s;
  DMA_HandleTypeDef   dma_rx;
  DMA_HandleTypeDef   dma_tx;
  Codec_Data_TypeDef   data;

} Codec_i2s_HandleTypeDef;


typedef enum {
  BUFFER_BG = 0,
  BUFFER_FG
} Buffer_CurBuffer;




typedef enum
{
	I2S_ERROR=0,
	I2S_OK,
	I2S_BUSY
} I2S_Base_TypeDef;


#endif /* BSP_BDSP_BDSP_BSP_BASE_H_ */

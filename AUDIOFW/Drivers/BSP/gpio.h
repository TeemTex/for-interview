/*
 * gpio.h
 *
 *  Created on: 30 Oct 2018
 *      Author: Tim
 */

#ifndef BSP_GPIO_H_
#define BSP_GPIO_H_

#include <BSP_Base.h>
#include <BDSP/BDSP_Conf.h>

#define IO_IS_OUTPUT	GPIO_MODE_OUTPUT_PP
#define IO_IS_INPUT		GPIO_MODE_INPUT
#define IO_IS_IT		GPIO_MODE_IT_RISING|GPIO_MODE_IT_FALLING|GPIO_MODE_INPUT
typedef enum
{
	OFF=0,
	ON,
	LONG_ON,
	NUM_SWPB_STATES
}IO_States;

typedef enum
{
	IO_DISABLED=0,
	IO_ENABLED
}IO_Inited;

//typedef enum
//{
//	IO_IS_INPUT=0,
//	IO_IS_OUTPUT
//}IO_Dir_Enum;
typedef struct __GPIO_TypeDef {

	IO_States state;
	int8_t qeue;
	uint16_t pin;
	GPIO_TypeDef* GPIOx;
	uint32_t  Mode;
	uint32_t timestamp;
	IRQn_Type			IRQn;
//	uint8_t  			IRQ_enable;
	uint8_t				NVIC_Prio;
	uint8_t				NVIC_SubPrio;
	IO_Inited			isInit;
	uint32_t 			debounce;
	uint32_t 			debounce_time;
}
GPIO_BSP_TypeDef;

typedef enum
{
	IO0_ENUM=0,
	IO1_ENUM,
	IO2_ENUM,
	IO3_ENUM,
	IO4_ENUM,
	IO5_ENUM,
	IO6_ENUM,
	IO7_ENUM,
	IO8_ENUM,
	IO9_ENUM,
	IO10_ENUM,
	IO11_ENUM,
	IO12_ENUM,
	IO13_ENUM,
	IO14_ENUM,
	IO15_ENUM,
//	IO0_3V3_ENUM,
	NUM_IO_ENUM
}IO_ENUM_TypeDef;



void GPIO_Clock_Check(GPIO_TypeDef * GPIOX);
void IO_InitAll(void);
void IO_configure(IO_ENUM_TypeDef IO_n,uint32_t  Mode, uint32_t debounce_time);
void IO_IT(IO_ENUM_TypeDef num, IO_States Current_state);
IO_States IO_Read_State(IO_ENUM_TypeDef num);

void EXTI15_10_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void EXTI3_IRQHandler(void);
void EXTI2_IRQHandler(void);
void EXTI0_IRQHandler(void);
void EXTI4_IRQHandler(void);

void IO_IT_Callback(IO_ENUM_TypeDef num, IO_States Current_state);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void IO_Update_State(IO_ENUM_TypeDef num, IO_States New_state);
IO_States IO_Read_Pin(IO_ENUM_TypeDef num);
#endif /* BSP_GPIO_H_ */

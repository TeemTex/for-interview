#include <codec.h>

static  uint8_t codec_inited=0;
 Codec_i2s_HandleTypeDef CodecA;
 Codec_SAI_HandleTypeDef CodecB;
//Pointer to the ADC device structure
//This is used because I can't pass device struct to the interrupt callbacks

/*
Configures Input or output I2S devices
 */
void Codec_configure_CodecA(void)
{
	memset(&CodecA.data.buffer_tx, 0, I2S_INPUT_BUFFER_SIZE*4);
	memset(&CodecA.data.buffer_rx, 0, I2S_INPUT_BUFFER_SIZE*4);

	memset(&CodecA.data.IO_buffer_rx, 0, I2S_INPUT_BUFFER_SIZE*2);
	memset(&CodecA.data.IO_buffer_tx, 0, I2S_INPUT_BUFFER_SIZE*2);
	CodecA.data.IO_state_rx=IO_BUFFER_STATE_FULL;
	CodecA.data.IO_state_tx=IO_BUFFER_STATE_FULL;


#ifdef CODEC_CS4271
		cs4271_Init_I2S(&CodecA);
#else
#error "something not defined"
#endif


}

/*
Starts  I2S devices
*/
void Codec_start_CodecA(void)
{
	//Set Index at max to wait for DMA data
	CodecA.data.IO_index_rx=I2S_INPUT_BUFFER_SIZE*2;
	CodecA.data.IO_index_rx=I2S_INPUT_BUFFER_SIZE*2;
	cs4271_I2S_Start(&CodecA);
}

//int Codecs_test(uint8_t test_n)
//{
//	switch(test_n)
//	{
//	case 1:
//		return cs4271_I2C_Test(0x20);
//		break;
//	case 2:
//		return Codec_Audio_Test(&CodecA,1);
//		break;
//	case 3:
//		return Codec_Audio_Test(&CodecA,0);
//		break;
//	case 4:
////		return Codec_Audio_Test(&CodecB);
//		return 0;
//		break;
//	default:
//		BSP_Error_Handler();
//		return 0;
//	}
//}

int Codec_Audio_Test(Codec_i2s_HandleTypeDef *  Codec, uint8_t ch)
{
	for (uint32_t i=0; i<48000; i++)
	{
		signed int ReadVal = CodecA_Read_Val_24b();
		CodecA_Write_Val_24b(ReadVal);
	}
	float temp_buf[1000];
	float temp_buf2[48*5];
	float result;

	for (uint32_t __IO i=0; i<48*5; i++)
	{
		for(int __IO k = 0; k<1000; k++)
		{
			signed int ReadVal =((signed int)CodecA_Read_Val_24b()>>8);
			signed int ReadVal2 = ((signed int)CodecA_Read_Val_24b()>>8);
			CodecA_Write_Val_24b(ReadVal);
			CodecA_Write_Val_24b(ReadVal);

			if(ch)
			{
				temp_buf[k]=(float)ReadVal;
			}
			else
			{
				temp_buf[k]=(float)ReadVal2;
			}

		}
		arm_rms_f32(temp_buf,1000,&temp_buf2[i]);
	}
	arm_rms_f32(temp_buf2,48*5,&result);

	float upper_lim = 2.9e6;
	float lower_lim = 2.3e6;
	if(result<upper_lim)
	{
		if(result>lower_lim)
		{
			return 1;
		}
	}
	return 0;
}

Codec_Base_TypeDef CodecA_Write_Val_24b(float Data)
{

	buffer_io_f(&CodecA.data.IO_state_tx,
			&CodecA.data.DMA_state_tx,
			I2S_INPUT_BUFFER_SIZE*2,
			&CodecA.data.IO_index_tx,
			&CodecA.data.IO_buffer_tx[0],
			&CodecA.data.buffer_tx[0],
			IO_BUFFER_TYPE_TX,
			Data);
	return Codec_OK;
}

float CodecA_Read_Val_24b(void)
{
	if(!codec_inited)
	{
//		DMA_Buff_StateTypeDef state = CodecA.data.DMA_state_rx;
		while(CodecA.data.DMA_state_rx==BUFFER_OFFSET_NONE)
		{

		}
		cs4271_UNMUTE_BOTH_CH();
		codec_inited=1;
	}

	return buffer_io_f(
			&CodecA.data.IO_state_rx,
			&CodecA.data.DMA_state_rx,
			I2S_INPUT_BUFFER_SIZE*2,
			&CodecA.data.IO_index_rx,
			&CodecA.data.IO_buffer_rx[0],
			&CodecA.data.buffer_rx[0],
			IO_BUFFER_TYPE_RX,0);
}

static float buffer_io_f(__IO IO_Buffer_StateTypeDef * pIO_State,DMA_Buff_StateTypeDef * pDMA_State, uint32_t buffer_size,
		uint32_t * pIndex, __IO uint16_t * IO_Buf, __IO uint16_t * DMA_Buff, IO_Buffer_Type type, float TxData)
{
	uint16_t Val1, Val2;
	if(pIndex[0]>=buffer_size)
	{
		//Wait until DMA finishes transaction and changes state flag
//		*pIO_State=IO_BUFFER_STATE_EMPTY;
		while(*pIO_State!=BUFFER_READY)
		{
//			assert_param(*pIO_State!=IO_BUFFER_STATE_OVERFLOW);
			assert_param(*pIO_State!=IO_BUFFER_STATE_NOT_INITED);

			//Waring: This section enters endless loop, which can be broken only by interrupt callback implementation, or by using RTOS thread.
			//Might be good idea is to use I2S_BUSY return, but I have not implemented this yet.
//			printf(" r too fast");
//			return I2S_BUSY;
		}
		*pIO_State=IO_BUFFER_STATE_FULL;
		uint32_t offset=0;
		if(*pDMA_State!=BUFFER_OFFSET_HALF)
		{
			offset=buffer_size;
		}
		else
		{
			offset=0;
		}
		uint16_t tval1;
		if (type==IO_BUFFER_TYPE_RX)
		{
			for(__IO uint32_t i=0; i<buffer_size;i++)
			{
				tval1=DMA_Buff[i+offset];
				IO_Buf[i]=tval1;
			}
		}
		else
		{
			for(__IO uint32_t i=0; i<buffer_size;i++)
			{
				tval1 = IO_Buf[i];
				DMA_Buff[i+offset]=tval1;
			}
		}

		*pIndex=0;
	}
	if(type==IO_BUFFER_TYPE_RX)
	{

		Val1=IO_Buf[pIndex[0]];
		Val2=IO_Buf[pIndex[0]+1];
		pIndex[0]+=2;
		return codec_to_float(Val1, Val2);
	}
	else
	{
		float_to_codec(TxData, &Val1, &Val2);
//		sign_to_unsign(TxData, &Val1, &Val2);
		IO_Buf[pIndex[0]]=Val1;
		IO_Buf[pIndex[0]+1]=Val2;
		pIndex[0]+=2;
		return 0;
	}
}



static inline void sign_to_unsign(signed int sVal, uint16_t *Val1, uint16_t *Val2)
{
  uint32_t temp;
  temp =(uint32_t)(sVal)>>16;
  temp=temp&0x0000FFFF;

  *Val1=(uint16_t)temp;
  temp = (uint32_t)(sVal)&0x0000FF00;
  *Val2=(uint16_t)temp;
}


static inline signed int usign24_to_sign(uint16_t Val1, uint16_t Val2)
{
  uint32_t temp;

  temp = (uint32_t)Val1;
  temp=temp<<16;
  temp&=0xFFFF0000;
  temp=temp+((uint32_t)Val2&0x0000FF00);
  return (signed int)temp;
}


static inline void float_to_codec(float fVal,  uint16_t *Val1, uint16_t *Val2)
{
  signed int sVal = (signed int)(fVal);
  uint32_t temp;
  temp =(uint32_t)(sVal)>>16;
  temp=temp&0x0000FFFF;

  *Val1=(uint16_t)temp;
  temp = (uint32_t)(sVal)&0x0000FF00;
  *Val2=(uint16_t)temp;
}


static inline float codec_to_float(uint16_t Val1, uint16_t Val2)
{
  uint32_t temp;

  temp = (uint32_t)Val1;
  temp=temp<<16;
  temp&=0xFFFF0000;
  temp=temp+((uint32_t)Val2&0x0000FF00);
  signed int s_temp = (signed int)temp;
  return (float)s_temp;
}
static void Codec_Callback(void)
{
//	if(CodecA.data.IO_state_rx!=IO_BUFFER_STATE_EMPTY)
//	{	//Error
//		bsp_dbg_print("Buffer overflow");
//		CodecA.data.IO_state_rx=IO_BUFFER_STATE_OVERFLOW;
//		CodecA.data.IO_state_tx=IO_BUFFER_STATE_OVERFLOW;
//	}
//	else
//	if((CodecA.data.IO_state_rx!=IO_BUFFER_STATE_FULL)||(CodecA.data.IO_state_tx!=IO_BUFFER_STATE_FULL))
//	{
//		static uint8_t cnt;
//		cnt++;
//		debug_print("%s\t","overflow");
//		debug_print("%d\n",cnt);
//	}
	{
		CodecA.data.IO_state_rx=BUFFER_READY;
		CodecA.data.IO_state_tx=BUFFER_READY;
	}
}

void CS4271_TxRxCplt_Callback(void)
{
//	static int init=0;
	//	I2S_cs4271_CompleteCallback(hi2s);
	CodecA.data.DMA_state_rx=BUFFER_OFFSET_FULL;
	CodecA.data.DMA_state_tx=BUFFER_OFFSET_FULL;
	Codec_Callback();
//	Codec_Callback->data.IO_state_rx=BUFFER_READY;
//	pCodec_static->data.IO_state_tx=BUFFER_READY;

}
void CS4271_TxRxHalfCplt_Callback(void)
{
	//	I2S_cs4271_HalfCallback(hi2s);
//	assert_param()
	CodecA.data.DMA_state_tx=BUFFER_OFFSET_HALF;
	CodecA.data.DMA_state_rx=BUFFER_OFFSET_HALF;
	Codec_Callback();
//	pCodec_static->data.IO_state_rx=BUFFER_READY;
//	pCodec_static->data.IO_state_tx=BUFFER_READY;
}


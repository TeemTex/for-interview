/*
 * codec.h
 *
 *  Created on: 30 Oct 2018
 *      Author: Tim
 */

#ifndef BSP_CODEC_H_
#define BSP_CODEC_H_

#include <BSP_Base.h>
#include <BDSP/BDSP_Conf.h>
#include <string.h>
#include "Components/CS4271/CS4271.h"
#include "../DSP/Include/arm_math.h"






typedef struct Codec_SAI_Handle
{
  SAI_HandleTypeDef   sai;
  DMA_HandleTypeDef   dma_rx;
  DMA_HandleTypeDef   dma_tx;
  Codec_Data_TypeDef   data;

} Codec_SAI_HandleTypeDef;

typedef enum
{
	Codec_ERROR=0,
	Codec_OK,
	Codec_BUSY
} Codec_Base_TypeDef;

void Codec_configure_CodecA(void);
void Codec_start_CodecA(void);
float CodecA_Read_Val_24b(void);
Codec_Base_TypeDef CodecA_Write_Val_24b(float Data);

static inline void sign_to_unsign(signed int sVal, uint16_t *Val1, uint16_t *Val2);
static inline signed int usign24_to_sign(uint16_t Val1, uint16_t Val2);
static float buffer_io_f(__IO IO_Buffer_StateTypeDef * pIO_State,DMA_Buff_StateTypeDef * pDMA_State, uint32_t buffer_size,
		uint32_t * pIndex, __IO uint16_t * IO_Buf, __IO uint16_t * DMA_Buff, IO_Buffer_Type type, float TxData);

void CS4271_TxRxCplt_Callback(void);
void CS4271_TxRxHalfCplt_Callback(void);
static void Codec_Callback(void);
//int Codecs_test(uint8_t test_n);
int Codec_Audio_Test(Codec_i2s_HandleTypeDef *  Codec, uint8_t ch);

static inline void float_to_codec(float fVal,  uint16_t *Val1, uint16_t *Val2);
static inline float codec_to_float(uint16_t Val1, uint16_t Val2);
#endif /* BSP_CODEC_H_ */

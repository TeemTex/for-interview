/*
 * delay.c
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */


#include "delay.h"

static float ReadAdr1;
static int fader_sample_num;

Compressor_StructTypedef Dig_Delay_Mixer;
Compressor_StructTypedef Delay_compressor;


void Delay_init(Delay_HandleTypeDef * hDelay)
{

	//todo make it as function argument
	fader_sample_num = 250;
	compressor_init_struct(1U<<31,0.95f,&Delay_compressor);
	init_digital_type(hDelay);
	init_tape_type(hDelay);



	hDelay->hMem.Write_addr=0;
	hDelay->hMem.Read_addr=1000;
	hDelay->delay_time=500;
	hDelay->Fs = 48;
	ring_buffer_init(&hDelay->hRing,(float*)SDRAM_BANK_ADDR,2000000);
	Ring_Set_Tail(&hDelay->hRing,hDelay->delay_time*hDelay->Fs);
	hDelay->interchannel_delay = 0;

}

void Delay_Stereo_Read(volatile float * pBuff, Delay_HandleTypeDef * hDelay)
{


	if(hDelay->mode==DELAY_TAPE)
	{


	Tape_Stereo_Read_ring(pBuff,hDelay);
	}
	else
	{

		Digi_Stereo_Read_ring(pBuff,hDelay);
	}
}

void Digi_Stereo_Read_ring(volatile float * pReturn, Delay_HandleTypeDef * hDelay)
{

	uint8_t speed, isLong;
	speed=36;
	isLong=1;

//	if(hDelay->Direction==DIRECTION_NORMAL)
	{
		hDelay->Playback!=PLAYBACK ? Digital_Read_Stereo_rough(hDelay,speed,isLong,pReturn)
				: Digital_Read_Stereo(hDelay,speed,isLong,pReturn);
	}
//	else
//	{
//		Digital_Read_Stereo_rough(hDelay,speed,isLong,pReturn);
//	}
	//	pReturn[0]=Delay_Memory_Read(hDelay,0,speed,isLong);
	//	pReturn[1]=Delay_Memory_Read(hDelay,1,speed,isLong);
}

TapeInterpolator_HandleTypeDef Delay_shift;
TapeInterpolator_HandleTypeDef Spread_shift;
signed int tail_length;
float spread_balance;
signed int spread_offset;

float * pTail;
float * pFlt;
float * pTail_ch2;
float * pFlt_ch2;
float TapeRead[6];
static void Tape_Stereo_Read_ring(volatile float * pReturn, Delay_HandleTypeDef * hDelay)
{

//	uint32_t temp = hDelay->delay_time;
//	if (temp>50)
//	{
//		while(temp>50)
//		{
//			temp = temp /2;
//		}
//	}
//	else if (temp<25)
//	{
//		while(temp<25)
//		{
//			temp = temp *2;
//		}
//	}
//	hDelay->interchannel_delay = temp;

	interpolate(&Delay_shift,hDelay->delay_time*hDelay->Fs);
	interpolate(&Spread_shift,hDelay->interchannel_delay*hDelay->Fs);

	spread_offset = Spread_shift.integers;

	if(hDelay->Direction==DIRECTION_NORMAL)
	{
		spread_balance = Spread_shift.float_part + Delay_shift.float_part;
		if (spread_balance > 1.0f)
		{
			spread_offset+=1.0f;
			spread_balance-=1.0f;
		}
	}
	else
	{
		spread_balance = Spread_shift.float_part - Delay_shift.float_part;
		if (spread_balance < 0.0f)
		{
			spread_offset-=1.0f;
			spread_balance+=1.0f;
		}
	}

	if(hDelay->Playback==PLAYBACK)
	{

		if(hDelay->Direction==DIRECTION_NORMAL)
		{
			tail_length =  Delay_shift.integers;
			pTail = Ring_Set_Tail(&hDelay->hRing,tail_length);
			pFlt = (Ring_Point_Offset_From_Tail(&hDelay->hRing, -1));



			pTail_ch2 = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*spread_offset,pTail);
			pFlt_ch2 = Ring_Point_Offset_Ptr(&hDelay->hRing, -1,pTail_ch2);
		}
		else
		{

			tail_length =  Delay_shift.integers;
			static int prev_length;
			//To keep main Tail following main head
			Ring_Set_Tail(&hDelay->hRing,tail_length);

			int dif = -1*prev_length + tail_length;
			hDelay->reverse_counter = dif<(signed int)hDelay->reverse_counter ? hDelay->reverse_counter-dif : 0;
			hDelay->hRevRing.Tail = Ring_Point_Offset_Ptr(&hDelay->hRevRing,-1*(-1*dif+1),hDelay->hRevRing.Tail);
			pTail = hDelay->hRevRing.Tail;
			pFlt = (Ring_Point_Offset_Ptr(&hDelay->hRing,1,pTail));

			pTail_ch2 = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*spread_offset,pTail);
			pFlt_ch2 = Ring_Point_Offset_Ptr(&hDelay->hRing, -1,pTail_ch2);

			prev_length = tail_length;
		}
	}
	else
	{
		signed int rev_coef = 1;
		if(hDelay->Direction==DIRECTION_REVERSE)
		{
			rev_coef = -1;
			tail_length = Delay_shift.integers;

			signed int size_dif = hDelay->hSubRing.size - tail_length;

			hDelay->hSubRing.pBuf = Ring_Point_Offset_From_Head(&hDelay->hRing,-1*tail_length);
			hDelay->hSubRing.size = tail_length;
			hDelay->virtual_head_ptr = Ring_Ptr_Move(&hDelay->hRing,(size_dif)*rev_coef,hDelay->virtual_head_ptr);
			Ring_Head_Move(&hDelay->hSubRing,-1*size_dif);

			pTail = Ring_Set_Tail(&hDelay->hSubRing,1*tail_length);
			pFlt = (Ring_Point_Offset_From_Tail(&hDelay->hSubRing,1));

			pTail_ch2 = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*spread_offset,pTail);
			pFlt_ch2 = Ring_Point_Offset_Ptr(&hDelay->hRing, -1,pTail_ch2);
		}
		else
		{
			tail_length = Delay_shift.integers;

			signed int size_dif = hDelay->hSubRing.size - (int)tail_length;

			hDelay->hSubRing.pBuf = Ring_Point_Offset_From_Head(&hDelay->hRing,-1*(int)tail_length);
			hDelay->hSubRing.size = tail_length;
			//			hDelay->hSubRing.size += (flt_part>0.5f) ? 1 : 0;
			Ring_Head_Move(&hDelay->hSubRing,size_dif);

			pTail = Ring_Set_Tail(&hDelay->hSubRing,tail_length);
			pFlt = (Ring_Point_Offset_From_Tail(&hDelay->hSubRing,-1));

			pTail_ch2 =Ring_Point_Offset_Ptr(&hDelay->hRing,-1*spread_offset,pTail);
			pFlt_ch2 = Ring_Point_Offset_Ptr(&hDelay->hRing,-1,pTail_ch2);
		}
	}

	pReturn[0]= Mixer_f(pTail[0],						pFlt[0],						Delay_shift.float_part);
	pReturn[1]= Mixer_f(pTail[hDelay->hMem.offset],		pFlt[hDelay->hMem.offset],		Delay_shift.float_part);
	pReturn[2]= Mixer_f(pTail_ch2[hDelay->hMem.offset],	pFlt_ch2[hDelay->hMem.offset],	spread_balance);
//	pReturn[2]=pReturn[1];

}

static inline void interpolate(TapeInterpolator_HandleTypeDef * Interpoator, uint32_t new_value)
{
	if(Interpoator->pParent==NULL)
	{
		float new_val_f = (float)new_value;
		float temp = fabs(Interpoator->current-new_val_f);
		Interpoator->c = temp/(5*480000.0f);
	}
	else
	{
		Interpoator->c = 0.0005f;//Interpoator->pParent->c;
	}

	Interpoator->current = LPF_f32(Interpoator->current,new_value,Interpoator->c);

	float int_part_f;
	Interpoator->float_part = modff(Interpoator->current,&int_part_f);
	Interpoator->integers = (int)int_part_f;
}


float Delay_Memory_Read(Delay_HandleTypeDef * hDelay, uint32_t channel, uint32_t speed, uint8_t isLong)
{
	//	float retval;
	//	retval=Digital_Read_Ch(hDelay,channel,speed,isLong);
	//	return retval;
}

static void init_tape_type(Delay_HandleTypeDef * hDelay)
{
	Delay_shift.c=1;
	Delay_shift.current=0;
	Delay_shift.float_part=0;
	Delay_shift.integers=0;
	Delay_shift.pParent=NULL;

	Spread_shift.c=1;
	Spread_shift.current=48*50;
	Spread_shift.float_part=0;
	Spread_shift.integers=48*50;
	Spread_shift.pParent=&Delay_shift;
}

DigitalInterpolator_HandleTypeDef d_intr;
static void init_digital_type(Delay_HandleTypeDef * hDelay)
{
	compressor_init_struct(1U<<31,0.95f,&Dig_Delay_Mixer);
	d_intr.flag=1;
	d_intr.hDelay=hDelay;

}

static void Digital_Read_Stereo_rough(Delay_HandleTypeDef * hDelay, uint32_t speed, uint8_t isLong,volatile float * pReturn)
{

//	hDelay->interchannel_delay = 50;

	static float old_delay_in_samples2;
	float new_delay_in_samples2 = hDelay->interchannel_delay*hDelay->Fs;
//	new_delay_in_samples2 = LPF_f32(old_delay_in_samples2,new_delay_in_samples2,0.00005f);
	old_delay_in_samples2 = new_delay_in_samples2;
	int spread = (int)new_delay_in_samples2;
	if(hDelay->Playback==PLAYBACK)
	{
		if(hDelay->Direction==DIRECTION_REVERSE)
		{
			float * ptr = Ring_Point_Offset_Ptr(&hDelay->hRing,-1,hDelay->hRevRing.Tail);
			hDelay->hRevRing.Tail = ptr;
			Ring_Set_Tail(&hDelay->hRing,hDelay->delay_time*hDelay->Fs);
			pReturn[0] = ptr[0];
			pReturn[1] = ptr[hDelay->hMem.offset];

			float * Stereo_spread_ptr =  Ring_Point_Offset_Ptr(&hDelay->hRing,-1*spread,ptr);
			pReturn[2] = Stereo_spread_ptr[hDelay->hMem.offset];
		}
		else
		{
			int tail_length = hDelay->delay_time*hDelay->Fs;
			Ring_Set_Tail(&hDelay->hRing,tail_length);
			float * ptr = hDelay->hRing.Tail;
			pReturn[0] = ptr[0];
			pReturn[1] = ptr[hDelay->hMem.offset];
		}
	}
	else
	{
		if(hDelay->Direction==DIRECTION_REVERSE)
		{
			int tail_length = hDelay->delay_time*hDelay->Fs;
//			signed int size_dif = hDelay->hSubRing.size - tail_length;

			hDelay->hSubRing.pBuf = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*tail_length,hDelay->hRing.Head);
			hDelay->hSubRing.size = tail_length;
			Ring_Set_Tail(&hDelay->hRing,hDelay->delay_time*hDelay->Fs);
			float * ptr = Ring_Point_Offset_Ptr(&hDelay->hSubRing,-1,hDelay->hSubRing.Tail);
			hDelay->hRevRing.Tail = ptr;
			hDelay->hSubRing.Tail = ptr;

			pReturn[0] = ptr[0];
			pReturn[1] = ptr[hDelay->hMem.offset];

			float * Stereo_spread_ptr =  Ring_Point_Offset_Ptr(&hDelay->hRing,-1*spread,hDelay->hRevRing.Tail);
			pReturn[2] = Stereo_spread_ptr[hDelay->hMem.offset];
			//			pReturn[0] = 0;
			//			pReturn[1] = 0;
		}
		else
		{
			int tail_length = hDelay->delay_time*hDelay->Fs;
//			signed int size_dif = hDelay->hSubRing.size - tail_length;

			hDelay->hSubRing.pBuf = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*tail_length,hDelay->hRing.Head);
			hDelay->hSubRing.size = tail_length+1;
			float * ptr = Ring_Point_Offset_Ptr(&hDelay->hSubRing,1,hDelay->hSubRing.Tail);
			hDelay->hSubRing.Tail = ptr;
			Ring_Set_Tail(&hDelay->hRing,hDelay->delay_time*hDelay->Fs);
			pReturn[0] = ptr[0];
			pReturn[1] = ptr[hDelay->hMem.offset];

			float * Stereo_spread_ptr =  Ring_Point_Offset_Ptr(&hDelay->hRing,-1*spread,hDelay->hSubRing.Tail);
			pReturn[2] = Stereo_spread_ptr[hDelay->hMem.offset];
		}
	}
}

__IO static uint32_t old_tick=0;
__IO static uint32_t new_tick=0;
static void Digital_Read_Stereo(Delay_HandleTypeDef * hDelay, uint32_t speed, uint8_t isLong,volatile float * pReturn)
{

//	uint32_t temp = hDelay->delay_time;
//	if (temp>50)
//	{
//		while(temp>50)
//		{
//			temp = temp /2;
//		}
//	}
//	else if (temp<25)
//	{
//		while(temp<25)
//		{
//			temp = temp *2;
//		}
//	}
//	hDelay->interchannel_delay = temp;

//	static uint32_t offset, spread_offset;
//	static uint32_t old_offset, old_spread_offset;
	static float mix_balance;

	//Flag state machine:
	//  1 = Ready for new transition
	//	2 = Busy
	if(d_intr.flag==1)
	{
		d_intr.offset_new = hDelay->delay_time*hDelay->Fs;
		d_intr.spread_new = hDelay->Fs*hDelay->interchannel_delay;
		if((d_intr.offset_new!=d_intr.offset_old)||(d_intr.spread_new!=d_intr.spread_old))
		{
			//Set Flag to busy
			d_intr.flag=2;
			mix_balance=1.0f;
			new_tick=0;

//			get_new_locations(hDelay,&d_intr);
		}
		else
		{
			float * ptr[4];
			get_new_and_old_samples(hDelay,&d_intr,ptr);
			pReturn[0]=ptr[1][0];
			pReturn[1]=ptr[1][hDelay->hMem.offset];
			pReturn[2]=ptr[3][hDelay->hMem.offset];
		}
	}
	if(d_intr.flag==2)
	{

//		mix_balance = digital_delay_mixer_tick(speed,mix_balance);
		new_tick++;
		if ((new_tick%speed==0))
		{
			old_tick=new_tick;
			new_tick=0;
			mix_balance-=0.05f;
		}
		//if fully mixed


		/* #5 Read two values from memory - old and new */

		/* #5 MIX! */
		float temp_val[6];
		float * ptr[4];
		get_new_and_old_samples(hDelay,&d_intr,ptr);

		temp_val[0] = ptr[0][0];
		temp_val[1] = ptr[1][0];
		temp_val[2] = ptr[0][hDelay->hMem.offset];
		temp_val[3] = ptr[1][hDelay->hMem.offset];
		temp_val[4] = ptr[2][hDelay->hMem.offset];
		temp_val[5] = ptr[3][hDelay->hMem.offset];


		float val[3];
		val[0] = Mixer_f(temp_val[0],temp_val[1],mix_balance);
		val[0] = compress_struct(val[0],&Dig_Delay_Mixer);

		val[1] = Mixer_f(temp_val[2],temp_val[3],mix_balance);
		val[1] = compress_struct(val[1],&Dig_Delay_Mixer);

		val[2] = Mixer_f(temp_val[4],temp_val[5],mix_balance);
		val[2] = compress_struct(val[2],&Dig_Delay_Mixer);

		pReturn[0]=val[0];
		pReturn[1]=val[1];
		pReturn[2]=val[2];

		if (mix_balance<0.05f)
		{
			fully_mixed(&d_intr);
//			hDelay->hRing.Tail = d_intr.NewAddress[0];
			mix_balance=1.0f;
		}
	}
	Ring_Set_Tail(&hDelay->hRing,d_intr.offset_old);
	hDelay->rev_ptr = Ring_Point_Offset_Ptr(&hDelay->hRing,-1,hDelay->rev_ptr);
}

static inline void get_new_and_old_samples(Delay_HandleTypeDef * hDelay, DigitalInterpolator_HandleTypeDef * Intr, float ** Ptr)
{
	if(hDelay->Direction==DIRECTION_NORMAL)
	{
		Ptr[0]=Ring_Point_Offset_From_Head(&hDelay->hRing,-1*Intr->offset_new);
		Ptr[1]=Ring_Point_Offset_From_Head(&hDelay->hRing,-1*Intr->offset_old);

		Ptr[2]=Ring_Point_Offset_From_Head(&hDelay->hRing,-1*(Intr->offset_new+Intr->spread_new));
		Ptr[3]=Ring_Point_Offset_From_Head(&hDelay->hRing,-1*(Intr->offset_old+Intr->spread_old));
	}
	else
	{
		signed int virtual_head_offset = Intr->offset_old - Intr->offset_new;

		Intr->NewVirtHeadPtr = Ring_Point_Offset_Ptr(&hDelay->hRing,virtual_head_offset,hDelay->virtual_head_ptr);

		Ptr[0]=hDelay->rev_ptr;
		Ptr[1]=hDelay->rev_ptr;

		Ptr[2]=Ring_Point_Offset_Ptr(&hDelay->hRing,-1*Intr->spread_new,Ptr[0]);
		Ptr[3]=Ring_Point_Offset_Ptr(&hDelay->hRing,-1*Intr->spread_old,Ptr[1]);
	}
}

static inline void fully_mixed(DigitalInterpolator_HandleTypeDef * Intr)
{
	Intr->flag=1;
	Intr->offset_old = Intr->offset_new;
	Intr->spread_old = Intr->spread_new;

	signed int virtual_head_offset = Intr->offset_old - Intr->offset_new;
	Intr->hDelay->virtual_head_ptr = Ring_Point_Offset_Ptr(&Intr->hDelay->hRing,virtual_head_offset,Intr->hDelay->virtual_head_ptr);

}
//static inline void update_locations(Delay_HandleTypeDef * hDelay, DigitalInterpolator_HandleTypeDef * Intr)
//{
//	if(hDelay->Direction==DIRECTION_NORMAL)
//	{
//		Intr->NewAddress[0] = Ring_Point_Offset_Ptr(&hDelay->hRing,1,Intr->NewAddress[0]);
//		Intr->NewAddress[1] = Ring_Point_Offset_Ptr(&hDelay->hRing,1,Intr->NewAddress[1]);
//		Intr->OldAddress[0] = Ring_Point_Offset_Ptr(&hDelay->hRing,1,Intr->OldAddress[0]);
//		Intr->OldAddress[1] = Ring_Point_Offset_Ptr(&hDelay->hRing,1,Intr->OldAddress[1]);
//	}
//	else
//	{
//		Intr->NewAddress[0] = Ring_Point_Offset_Ptr(&hDelay->hRing,Intr->offset_new,hDelay->virtual_head_ptr);
//		Intr->NewAddress[1] = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*Intr->spread_new,Intr->NewAddress[0]);
//		Intr->OldAddress[0] = Ring_Point_Offset_Ptr(&hDelay->hRing,Intr->offset_old,hDelay->virtual_head_ptr);
//		Intr->OldAddress[1] = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*Intr->spread_old,Intr->NewAddress[0]);
//	}
//}

static inline float digital_delay_mixer_tick(uint32_t speed, float mix_balance)
{


	new_tick++;
	if ((new_tick%speed==0))
	{
		old_tick=new_tick;
		new_tick=0;
//		mix_balance-=0.05f;
		mix_balance = mix_balance < 0.05f ? 1.0f : mix_balance-0.05f;
	}
	return mix_balance;
}
static inline void digital_read_step(Delay_HandleTypeDef * hDelay, uint32_t offset, uint32_t channel_spread, float * pReturn)
{
	float * spread_ptr;
	Ring_Set_Tail(&hDelay->hRing,offset);

	if(hDelay->Direction==DIRECTION_NORMAL)
	{

		pReturn[0]=hDelay->hRing.Tail[0];
		pReturn[1]=hDelay->hRing.Tail[hDelay->hMem.offset];

		spread_ptr = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*channel_spread,hDelay->hRing.Tail);
		pReturn[2]=spread_ptr[hDelay->hMem.offset];
	}
	else
	{
		float * Ptr = Ring_Point_Offset_Ptr(&hDelay->hRing,offset,hDelay->virtual_head_ptr);
		pReturn[0]=Ptr[0];
		pReturn[1]=Ptr[hDelay->hMem.offset];

		spread_ptr = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*channel_spread,hDelay->hRing.Tail);
		pReturn[2]=spread_ptr[hDelay->hMem.offset];
	}
}
static inline void get_new_locations(Delay_HandleTypeDef * hDelay, DigitalInterpolator_HandleTypeDef * pd_intr)
{

//	if(hDelay->Direction==DIRECTION_REVERSE)
//	{
//		pd_intr->NewAddress[0] = Ring_Point_Offset_Ptr(&hDelay->hRing,pd_intr->offset_new,hDelay->virtual_head_ptr);
//		pd_intr->NewAddress[1] = Ring_Point_Offset_Ptr(&hDelay->hRing,-1*pd_intr->spread_new,pd_intr->NewAddress[0]);
//	}
//	else
//	{
//		pd_intr->NewAddress[0] = Ring_Point_Offset_From_Head(&hDelay->hRing,-1*pd_intr->offset_new);
//		pd_intr->NewAddress[1] = Ring_Point_Offset_From_Head(&hDelay->hRing,-1*(pd_intr->offset_new+pd_intr->spread_new));
//	}
}


static signed int Tape_Read(Delay_HandleTypeDef * hDelay, uint32_t channel)
{
	signed int TapeRead;
	signed int ReadVal_2;
	signed int balance;

	static	float int_R_a=0;
	__IO static float fR_a;
	static uint32_t R_a;
	static uint32_t Next_R_A;
	signed int retval;

	if(channel==0)
	{
		ReadAdr1=Tape_AddrCalc(ReadAdr1,hDelay);
		fR_a= modff(ReadAdr1,&int_R_a);

		if (fR_a<0.1f)
		{
			fR_a=0;
		}
		//	else
		{
			R_a = (uint32_t)int_R_a ;
		}
		if(hDelay->Playback != PLAYBACK)
		{
			hDelay->Sampler.Loop_End_a = R_a;
			int whatever=read_step(0,hDelay);
			R_a = hDelay->hMem.Read_addr;
			//			hDelay->hMem.Read_addr +=1;
			//			hDelay->hMem.Read_addr = sampler_boundaries(hDelay->hMem.Read_addr,hDelay);
		}
		else
		{
			hDelay->hMem.Read_addr = R_a;
		}

		if(hDelay->Direction == DIRECTION_REVERSE)
		{

		}


		Next_R_A = ((uint32_t)R_a+1) % hDelay->hMem.Addr_max;

		TapeRead=SDRAM_Read32((uint32_t)(R_a));
		ReadVal_2=SDRAM_Read32((uint32_t)(Next_R_A));
		balance= (uint32_t)(fR_a*100);
		retval= Mixer_sign_long(TapeRead,ReadVal_2,balance);
	}
	else
	{
		TapeRead=SDRAM_Read32((uint32_t)(R_a+hDelay->hMem.offset));
		ReadVal_2=SDRAM_Read32((uint32_t)(Next_R_A+hDelay->hMem.offset));
		balance= (uint32_t)(fR_a*100);
		retval= Mixer_sign_long(TapeRead,ReadVal_2,balance);
	}


	return retval;
}


static float old_delay_samples=10000.0f;
static float Tape_AddrCalc(float ReadAddr, Delay_HandleTypeDef * hDelay)
{
	float RetAddr;
	float new_delay_samples= 48*hDelay->delay_time;

	float c = 0.001f;
	if(hDelay->Playback==PLAYBACK)
	{

		new_delay_samples = LPF_f32(old_delay_samples,new_delay_samples,c);
		old_delay_samples=new_delay_samples;

		RetAddr =  (float)(hDelay->hMem.Write_addr+hDelay->hMem.Addr_max);
	}
	else
	{
		new_delay_samples = 0;
		RetAddr =  (float)(hDelay->Sampler.Loop_Start_a+hDelay->hMem.Addr_max);
		//		read_step(RetAddr,hDelay);
		//		RetAddr = sampler_boundaries(RetAddr,hDelay);
		//		RetAddr =  (float)(hDelay->Sampler.Loop_End_a+hDelay->hMem.Addr_max);
	}

	if(!hDelay->isReverse)
	{
		RetAddr = RetAddr - new_delay_samples;
	}
	else
	{
		RetAddr = RetAddr + new_delay_samples;
	}

	RetAddr = fmodf(RetAddr,(float)hDelay->hMem.Addr_max);

	return RetAddr;

}

inline static uint32_t read_step(uint32_t aux_read,  Delay_HandleTypeDef * hDelay)
{
	if(!hDelay->isReverse)
	{
		hDelay->hMem.Read_addr = (hDelay->hMem.Read_addr + 1);
		hDelay->hMem.Read_addr = (hDelay->hMem.Read_addr) % (hDelay->hMem.Addr_max);

		if(hDelay->Playback!=PLAYBACK)
		{
			hDelay->hMem.Read_addr = sampler_boundaries(hDelay->hMem.Read_addr,hDelay);
		}


		aux_read = (aux_read + 1);
		aux_read = (aux_read) % (hDelay->hMem.Addr_max);
		if(hDelay->Playback!=PLAYBACK)
		{
			aux_read = sampler_boundaries(aux_read,hDelay);
		}
		return aux_read;
	}
	else
	{
		hDelay->hMem.Read_addr = (uint64_t)((hDelay->hMem.Read_addr + (hDelay->hMem.Addr_max) - 1) % hDelay->hMem.Addr_max);
		if(hDelay->Playback!=PLAYBACK)
		{
			hDelay->hMem.Read_addr = sampler_boundaries(hDelay->hMem.Read_addr,hDelay);
		}
		aux_read =  (uint64_t)(aux_read + hDelay->hMem.Addr_max - 1) % hDelay->hMem.Addr_max;

		if(hDelay->Playback!=PLAYBACK)
		{
			aux_read = sampler_boundaries(aux_read,hDelay);
		}
		return aux_read;

	}

}
static void delay_write_step( Delay_HandleTypeDef * hDelay)
{
	if(!hDelay->isReverse)
	{
		hDelay->hMem.Write_addr = (hDelay->hMem.Write_addr + 1);
		hDelay->hMem.Write_addr = (hDelay->hMem.Write_addr) % (hDelay->hMem.Addr_max);
		if(hDelay->Playback!=PLAYBACK)
		{
			hDelay->hMem.Write_addr = sampler_boundaries(hDelay->hMem.Write_addr,hDelay);
		}
	}
	else
	{
		hDelay->hMem.Write_addr = (uint64_t)(hDelay->hMem.Write_addr + hDelay->hMem.Addr_max - 1) % (hDelay->hMem.Addr_max);
		if(hDelay->Playback!=PLAYBACK)
		{
			hDelay->hMem.Write_addr = sampler_boundaries(hDelay->hMem.Write_addr,hDelay);
		}
	}
	//		uint32_t t_w_adr = (hDelay->hMem.Write_addr - 1);
	//		if(t_w_adr>=hDelay->hMem.Addr_max)
	//		{
	//			t_w_adr=hDelay->hMem.Addr_max-1;
	//		}
	//		hDelay->hMem.Write_addr = t_w_adr;
	//	}
}

static uint32_t Calculate_loop(Delay_HandleTypeDef * hDelay ,uint32_t offset)
{
	uint32_t t_offset = offset+(0);
	{
		if(!hDelay->isReverse)
		{
			{
				return (hDelay->Sampler.Loop_End_a+hDelay->hMem.Addr_max-(48*hDelay->delay_time)
						+t_offset) % hDelay->hMem.Addr_max;
			}

		}
		else
		{
			return (hDelay->Sampler.Loop_End_a+hDelay->hMem.Addr_max-(48*hDelay->delay_time)
					+t_offset) % hDelay->hMem.Addr_max;
		}
	}
}

static uint32_t sampler_boundaries(uint32_t in_addr, Delay_HandleTypeDef * hDelay)
{
	uint32_t retval = in_addr;
	if(!hDelay->isReverse)
	{
		if (hDelay->Sampler.Loop_Start_a<hDelay->Sampler.Loop_End_a)
		{
			if(in_addr>=hDelay->Sampler.Loop_End_a)
			{
				retval=hDelay->Sampler.Loop_Start_a;
			}
			if(in_addr<hDelay->Sampler.Loop_Start_a)
			{
				retval=hDelay->Sampler.Loop_Start_a;
			}
		}
		else
		{
			if((in_addr>=hDelay->Sampler.Loop_End_a)&&(in_addr<hDelay->Sampler.Loop_Start_a))
			{
				retval=hDelay->Sampler.Loop_Start_a;
			}
		}
	}
	else
	{
		if (hDelay->Sampler.Loop_Start_a<hDelay->Sampler.Loop_End_a)
		{
			if(in_addr>hDelay->Sampler.Loop_End_a)
			{
				retval=hDelay->Sampler.Loop_End_a;
			}
			if(in_addr<hDelay->Sampler.Loop_Start_a)
			{
				retval=hDelay->Sampler.Loop_End_a;
			}
		}
		else
		{
			if((in_addr>=hDelay->Sampler.Loop_End_a)&&(in_addr<hDelay->Sampler.Loop_Start_a))
			{
				retval=hDelay->Sampler.Loop_End_a;
			}
		}
	}

	return retval;
}

void delay_write(Delay_HandleTypeDef * hDelay,volatile float * Data, uint8_t channels_number)
{
	//	__IO int ToMemInt[2];
	//	ToMemInt[0] = (int)Data[0];
	//	ToMemInt[1] = (int)Data[1];
	//	hDelay->Direction == DIRECTION_NORMAL ? (write_ptr = hDelay->hRing.Head) : (write_ptr = hDelay->virtual_head_ptr)
	hDelay->hRing.Head[0]=Data[0];
	hDelay->hRing.Head[hDelay->hMem.offset]=Data[1];

}
static void write_to_delay(Delay_HandleTypeDef * hDelay,volatile float * Data, uint8_t channels_number)
{

}
void Delay_stereo_add(Delay_HandleTypeDef * hDelay,volatile float * RawIn, uint8_t channels_number)
{

	float  temp[2];
	float  fAdder[2];
	float  Adder[2];
	__IO int Adder_limited[2];
	__IO int Adder_IO[2];

	Adder_IO[0] = hDelay->hSubRing.Head[0];
	Adder_IO[1] = hDelay->hSubRing.Head[hDelay->hMem.offset];

	temp[0]=(float)Adder_IO[0];
	temp[1]=(float)Adder_IO[1];

	if(hDelay->Add_Flag==1)
	{

		Adder[0] = FadeOut_Stereo(RawIn[0],&hDelay->Add_FadeInOut,0);
		Adder[1] = FadeOut_Stereo(RawIn[1],&hDelay->Add_FadeInOut,1);
		if(hDelay->Add_FadeInOut.activated==0)
		{
			hDelay->Add_Flag=0;
		}

	}
	else if (hDelay->Add_Flag==2)
	{
		Adder[0] = FadeIn_Stereo(RawIn[0],&hDelay->Add_FadeInOut,0);
		Adder[1] = FadeIn_Stereo(RawIn[1],&hDelay->Add_FadeInOut,1);

		if(hDelay->Add_FadeInOut.activated==0)
		{
			hDelay->Add_Flag=0;
			hDelay->Playback=HOLD;
		}
	}
	else if (hDelay->Add_Flag==3)
	{
		Adder[0] = FadeIn_Stereo(RawIn[0],&hDelay->Add_FadeInOut,0);
		Adder[1] = FadeIn_Stereo(RawIn[1],&hDelay->Add_FadeInOut,1);

		if(hDelay->Add_FadeInOut.activated==0)
		{
			hDelay->Add_Flag=0;
			hDelay->Playback=PLAYBACK;
		}
	}
	else
	{
		Adder[0] = RawIn[0];
		Adder[1] = RawIn[1];
	}

	//tt is to avoid compilator working on addition of two volatiles..
	float tt[2];
	tt[0]=temp[0];
	tt[1]=temp[1];

	Adder[0]+=tt[0];
	Adder[1]+=tt[1];
	fAdder[0]=compress_struct(Adder[0],&Delay_compressor);
	fAdder[1]=compress_struct(Adder[1],&Delay_compressor);
	Adder_limited[0] = (int)fAdder[0];
	Adder_limited[1] = (int)fAdder[1];

	hDelay->hSubRing.Head[0]=Adder_limited[0];
	hDelay->hSubRing.Head[hDelay->hMem.offset]=Adder_limited[1];

}
//static uint32_t Sync_timestamp;
//static uint32_t last_press_time;
//static uint16_t latestADC=0;

//void delay_sync(Delay_HandleTypeDef * hDelay)
//{
//	uint32_t current_time = HAL_GetTick();
//	Sync_timestamp = current_time;
//	uint32_t new_sync_time = current_time -last_press_time[PB_SYNC];
//	last_press_time[PB_SYNC]=current_time;
//
//
//	if (new_sync_time<SYNC_RESET_TIME)
//	{
//		if (hDelay->TimeAlgo==DIRECT)
//		{
//			hDelay->TimeAlgo=KNOB_DIVMULT;
//			latestADC=KNOB_VALS[TIME];
//		}
//		while(new_sync_time>MAX_DELAY_TIME_MS)
//		{
//			new_sync_time=new_sync_time/2;
//		}
//
//		uint32_t absdif=abs((signed int)new_sync_time-(signed int)hDelay->sync_time);
//		//			if (new_sync_time!=hDelay->sync_time)
//		if (absdif>1)
//		{
//			hDelay->sync_time=new_sync_time;
//			debug_print("%s%d\r\n","New tapped tempo:",new_sync_time);
//		}
//	}
//}


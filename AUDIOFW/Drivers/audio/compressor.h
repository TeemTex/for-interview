/*
 * compressor.h
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#ifndef COMPRESSOR_H_
#define COMPRESSOR_H_

#include "stdint.h"

typedef struct __Compressor_Struct
{
	float THRESHOLD_COMPILED;
	float THRESHOLD_VALUE;
	float MAX_SAMPLEVAL;

}Compressor_StructTypedef;


//static


//public
void compressor_init_struct(uint32_t max_sample_val, float threshold_percent, Compressor_StructTypedef * pComp);
float compress_struct(float  val, Compressor_StructTypedef * pComp);
void compressor_init(uint32_t max_sample_val, float threshold_percent);

#endif /* COMPRESSOR_H_ */

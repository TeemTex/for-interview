/*
 * mixer.h
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#ifndef MIXER_H_
#define MIXER_H_

#include "math.h"
#include "debug.h"
//100 balance = fully data1, balance 0 -> fully data2
signed int Mixer_sign_long(signed long long  Data2, signed long long Data1, signed int balance);
float Mixer_f(float Data2, float Data1, float balance);

#endif /* MIXER_H_ */

/*
 * feedback.h
 *
 *  Created on: 14 ????. 2018 ?.
 *      Author: Tim
 */

#ifndef AUDIO_FEEDBACK_H_
#define AUDIO_FEEDBACK_H_

#include "compressor.h"

#define FB_COEF 3300.0f

typedef enum
{
	FEEDBACK_DIRECT=0,
	FEEDBACK_PING_PONG,
	FEEDBACK_CROSSOVER,
	FEEDBACK_NUM_ENUM
} Feedback_Type_EnumTypeDef;


typedef struct
{
	float fb_coef;
	Compressor_StructTypedef Feedback_compressor;
	uint32_t bottom_val;
	float level;
	Feedback_Type_EnumTypeDef type;
}Feedback_HandleTypeDef;


void feedback_init(Feedback_HandleTypeDef * pHandle, uint32_t compress_max_sample, float compressor_treshold, uint32_t bottom_val, float fb_strength);
float apply_feedback(float outval, float inval, Feedback_HandleTypeDef * pHandle);

void Feedback_multich(float *OutputValue,float * InputValue,float * pTarget, Feedback_HandleTypeDef * pHandle, uint8_t channels_number);
#endif /* AUDIO_FEEDBACK_H_ */

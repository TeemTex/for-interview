/*
 * wavetable_init.c
 *
 *  Created on: 3 ???. 2019 ?.
 *      Author: Tim
 */

#include "wavetable_init.h"


void sine_init(float * pArray, int size, float Amplitude, float freq)
{

	double radians;
	float relative;
	for(int i=0; i<size; i++)
	{
		relative = freq*(float)i/(float)size;
		radians =  2.0f*PI_CONST*relative;
		pArray[i]=Amplitude*sin(radians);
	}

}

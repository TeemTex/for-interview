/*
 * filters.c
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#include "filters.h"

float LPF_f32(float Current, float New, float c)
{
	float temp = 1.0f-c;
	temp=temp*Current;
	float temp2 = New * c;
	temp = temp + temp2;
	return temp;
	//	return (Current*(1.0f-c))+(New*c);
}

/*
 * delay.h
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#ifndef DELAY_H_
#define DELAY_H_

#include <BDSP/memory.h>
#include "compressor.h"
#include "mixer.h"
#include "filters.h"
#include "fader.h"
#include "feedback.h"
#include "math.h"
#define CH_NUM 2


//#define MAX_DELAY_TIME_MS 24000

typedef enum Delay_Type
{
	DELAY_TAPE=0,
	DELAY_Digital,
	NUM_DELAY_ALGORITHMS
} Delay_Type_EnumTypeDef;

typedef enum Delay_Direction
{
	DIRECTION_NORMAL=0,
	DIRECTION_REVERSE=1,
	NUM_DELAY_DIRECTION_MODES
}Delay_DirectionkModes_EnumTypeDef;

typedef enum Delay_PLayBack
{
	PLAYBACK=0,
	HOLD,
	ADD,
	ADD_TO_HOLD,
	NUM_DELAY_PLAYBACK_MODES
}Delay_Playbac_EnumTypeDef;

typedef struct
{
	uint32_t  Loop_Start_a;
	uint32_t  Loop_End_a;
//	Memory_Handle_TypeDef * pMem;
}Loop_handleTypeDef;

typedef enum Delay_Time_Mode
{
	DIRECT=0,
	TAPPED,
	KNOB_DIVMULT,
	NUM_DELAY_TIMESOURCE_MODES
}Delay_TimeMode_EnumTypedef;


typedef struct
{
	Memory_Handle_TypeDef hMem;
//	SW_PB_States_TypeDef SWPB[SWPB_NUM];
	uint32_t delay_time;
	signed int interchannel_delay;
	uint32_t sync_time;
	Codec_i2s_HandleTypeDef i2s;
	Delay_Playbac_EnumTypeDef Playback;
	Loop_handleTypeDef Sampler;
	Delay_Type_EnumTypeDef mode;
	Delay_TimeMode_EnumTypedef TimeAlgo;
	Delay_DirectionkModes_EnumTypeDef Direction;
	uint8_t isReverse;
	uint8_t Add_Flag;
	uint8_t Hold_Flag;
	FadeInOut_HandleTypeDef Add_FadeInOut;
	FadeInOut_HandleTypeDef Hold_FadeInOut;
	FadeInOut_HandleTypeDef Hold_FadeEnterExit;
	uint32_t rev_w_entry_point;
	uint32_t rev_r_entry_point;
	uint32_t isClipped;
	Ring_Handle_TypeDef hRing;
	Ring_Handle_TypeDef hSubRing;
	Ring_Handle_TypeDef hRevRing;
	RingSub_Handle_TypeDef hHold;
	uint32_t Fs;
	float *virtual_head_ptr;

	float * rev_ptr;
	__IO uint32_t reverse_counter;
}Delay_HandleTypeDef;

typedef struct TapeInterpolator_HandleTypeDef
{
	float current;
	uint32_t integers;
	float float_part;
	struct TapeInterpolator_HandleTypeDef * pParent;
	float c;


//	uint32_t
}TapeInterpolator_HandleTypeDef;

typedef struct DigitalInterpolator_HandleTypeDef
{

	float * NewVirtHeadPtr;
	uint32_t offset_new, offset_old;
	uint32_t spread_new, spread_old;

	uint8_t flag;
	Delay_HandleTypeDef * hDelay;

}DigitalInterpolator_HandleTypeDef;
static void get_new_locations(Delay_HandleTypeDef * hDelay, DigitalInterpolator_HandleTypeDef * pd_intr);

//public
void Delay_init(Delay_HandleTypeDef * hDelay);
void delay_write(Delay_HandleTypeDef * hDelay,volatile float * Data, uint8_t channels_number);
void Delay_Stereo_Read(volatile float * pReturn, Delay_HandleTypeDef * hDelay);
// OLD DELAY READ: signed int Delay_Memory_Read(Delay_HandleTypeDef * hDelay, uint32_t channel);
float Delay_Memory_Read(Delay_HandleTypeDef * hDelay, uint32_t channel, uint32_t speed, uint8_t isLong);
void Delay_stereo_add(Delay_HandleTypeDef * hDelay,volatile float * RawIn, uint8_t channels_number);


//private
static void Digital_Read_Stereo_rough(Delay_HandleTypeDef * hDelay, uint32_t speed, uint8_t isLong,volatile float * pReturn);

static void Digi_Stereo_Read_ring(volatile float * pReturn, Delay_HandleTypeDef * hDelay);
static void Tape_Stereo_Read_ring(volatile float * pReturn, Delay_HandleTypeDef * hDelay);
inline static uint32_t read_step(uint32_t aux_read,  Delay_HandleTypeDef * hDelay);
static void init_digital_type(Delay_HandleTypeDef * hDelay);
static void init_tape_type(Delay_HandleTypeDef * hDelay);
static void Digital_Read_Stereo(Delay_HandleTypeDef * hDelay, uint32_t speed, uint8_t isLong,volatile float * pReturn);
static signed int Tape_Read(Delay_HandleTypeDef * hDelay, uint32_t channel);
static float Tape_AddrCalc(float ReadAddr, Delay_HandleTypeDef * hDelay);
static uint32_t Calculate_loop(Delay_HandleTypeDef * hDelay ,uint32_t offset);
inline static uint32_t read_step(uint32_t aux_read,  Delay_HandleTypeDef * hDelay);
static uint32_t Calculate_Delay(Delay_HandleTypeDef * hDelay ,uint32_t offset);
static uint32_t sampler_boundaries(uint32_t in_addr, Delay_HandleTypeDef * hDelay);
static void delay_write_step( Delay_HandleTypeDef * hDelay);
static void interpolate(TapeInterpolator_HandleTypeDef * Interpoator, uint32_t new_value);
static void digital_read_step(Delay_HandleTypeDef * hDelay, uint32_t offset, uint32_t channel_spread, float * pReturn);
//static void update_locations(Delay_HandleTypeDef * hDelay, DigitalInterpolator_HandleTypeDef * Interpolator);
static inline float digital_delay_mixer_tick(uint32_t speed, float mix_balance);
static inline void fully_mixed(DigitalInterpolator_HandleTypeDef * Interpolator);
static void get_new_and_old_samples(Delay_HandleTypeDef * hDelay, DigitalInterpolator_HandleTypeDef * Intr, float ** Ptr);
#endif /* DELAY_H_ */

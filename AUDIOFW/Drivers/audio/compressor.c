/*
 * compressor.c
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#include "compressor.h"

float MAX_SAMPLEVAL;
float THRESHOLD_COMPILED;
int32_t THRESHOLD_VALUE;

void compressor_init_struct(uint32_t max_sample_val, float threshold_percent, Compressor_StructTypedef * pComp)
{
	pComp->MAX_SAMPLEVAL=max_sample_val;

	float m = (float)max_sample_val;
	pComp->THRESHOLD_COMPILED = m * m * threshold_percent * (1.0 - threshold_percent);

	pComp->THRESHOLD_VALUE = threshold_percent*max_sample_val;
}

float compress_struct(float  val, Compressor_StructTypedef * pComp)
{
	float tv = pComp->THRESHOLD_COMPILED / (val);
	if (val > pComp->THRESHOLD_VALUE) return (pComp->MAX_SAMPLEVAL - tv);
	else if (val < -pComp->THRESHOLD_VALUE) return (-pComp->MAX_SAMPLEVAL - tv);
	else return val;

}

void compressor_init(uint32_t max_sample_val, float threshold_percent)
{
	MAX_SAMPLEVAL=max_sample_val;

	float m = (float)max_sample_val;
	THRESHOLD_COMPILED = m * m * threshold_percent * (1.0 - threshold_percent);

	THRESHOLD_VALUE = (int32_t)threshold_percent*max_sample_val;
}

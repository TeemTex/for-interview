/*
 * fader.c
 *
 *  Created on: 13 ????. 2018 ?.
 *      Author: Tim
 */

#include "fader.h"

void FadeInOut_ms_Init(FadeInOut_ms_HandleTypeDef * hFadeInOut, float transition_length_ms, uint32_t Fs)
{
	hFadeInOut->step_num = transition_length_ms*(float)Fs;
	hFadeInOut->min_level=0.000001f;
	hFadeInOut->level = 1.0f;
	hFadeInOut->step_size = 2*(hFadeInOut->level-hFadeInOut->min_level)/(transition_length_ms*Fs);


	hFadeInOut->counter=hFadeInOut->step_num;
	hFadeInOut->activated=1;
	hFadeInOut->Transition_Fs = (float)Fs;
	hFadeInOut->transition_length_ms = transition_length_ms;

//	hFadeInOut->step_len_samples = transition_length_ms/hFadeInOut->step_num;

}

float FadeInOut_ms(float InVal, FadeInOut_ms_HandleTypeDef * hFadeInOut)
{
	float RetVal;

	//FadeIn out
	if(hFadeInOut->activated)
	{
		//Fade In if <50 or Fade Out if > 50
		if(hFadeInOut->counter<50)
		{
			RetVal = Fader(InVal,(float)2*hFadeInOut->counter);
		}
		else
		{
			RetVal = Fader(InVal,(float)100-2*(hFadeInOut->counter-50));
		}
		hFadeInOut->subcounter++;
		if ((hFadeInOut->subcounter*hFadeInOut->Transition_Fs)>=(hFadeInOut->transition_length_ms)/100.0f)
		{
			hFadeInOut->subcounter=0;
			hFadeInOut->counter--;
			if (hFadeInOut->counter==0)
			{
				hFadeInOut->activated=0;
			}
		}

	}
	else
	{
		RetVal = InVal;
	}

	return RetVal;

}

float FadeInOut_ms_Stereo(float InVal, FadeInOut_ms_HandleTypeDef * hFadeInOut, uint32_t channel)
{
	float RetVal;
	//FadeIn out
	if(hFadeInOut->activated)
	{
		if(channel==0)
		{
			hFadeInOut->counter--;
			if((hFadeInOut->counter*2)>=(hFadeInOut->step_num))
			{
				hFadeInOut->level=hFadeInOut->level - hFadeInOut->step_size;
			}
			else
			{
				hFadeInOut->level+=hFadeInOut->step_size;
				if(hFadeInOut->level>=1)
				{
					hFadeInOut->level=1.0f;
				}
			}
			if (hFadeInOut->counter==0) hFadeInOut->activated=0;
		}


		if(hFadeInOut->level>0.0001f)
		{
			RetVal = Fader_f(InVal,hFadeInOut->level);

		}
		else
		{
			RetVal=0;
		}
	}
	else
	{
		RetVal = InVal;
	}

	return RetVal;

}


void FadeInOut_Init(FadeInOut_HandleTypeDef * hFadeInOut, uint32_t transition_length)
{
	if(transition_length<100)
	{
		hFadeInOut->counter=transition_length;
		hFadeInOut->subcounter=1;
	}
	else
	{
		hFadeInOut->counter=100;
		hFadeInOut->subcounter=transition_length/100;
	}
	hFadeInOut->activated=1;
	hFadeInOut->transition_length = transition_length;

	assert_param(hFadeInOut->transition_length >= hFadeInOut->counter);

}

float FadeInOut(float InVal, FadeInOut_HandleTypeDef * hFadeInOut)
{
	float RetVal;
	if(hFadeInOut->transition_length<100) hFadeInOut->transition_length=100;
	//FadeIn out
	if(hFadeInOut->activated)
	{
		//Fade In if <50 or Fade Out if > 50
		if(hFadeInOut->counter<50)
		{
			RetVal = 0;
//			RetVal = Fader(InVal,(float)2*hFadeInOut->counter);
		}
		else
		{
			RetVal = 0;
//			RetVal = Fader(InVal,(float)100-2*(hFadeInOut->counter-50));
		}
		hFadeInOut->subcounter++;
		if ((100*hFadeInOut->subcounter)>=hFadeInOut->transition_length)
		{
			hFadeInOut->subcounter=0;
			hFadeInOut->counter--;
			if (hFadeInOut->counter==0)
			{
				hFadeInOut->activated=0;
			}
		}

	}
	else
	{
		RetVal = InVal;
	}

	return RetVal;

}

float FadeInOut_Stereo(float InVal, FadeInOut_HandleTypeDef * hFadeInOut, uint8_t channel)
{
	float RetVal;
	float level;
	if(hFadeInOut->transition_length<100) hFadeInOut->transition_length=100;
	//FadeIn out
	if(hFadeInOut->activated)
	{
		float f_counter = (float)(hFadeInOut->counter);
		f_counter = f_counter / 100.0f;
		//Fade In if >50 or Fade Out if < 50
		if(hFadeInOut->counter<50)
		{
			level = (1.0f-f_counter*2.0f);
			RetVal = Fader_f(InVal,level);
//			RetVal = Fader(InVal,(float)(-1*hFadeInOut->counter*2+100));
		}
		else
		{
			level = 2.0f*f_counter-1.0f;
			RetVal = Fader_f(InVal,level);
//			RetVal = Fader(InVal,(float)100-2*(hFadeInOut->counter-50));
		}
//		if(level<0.01f)
//		{
//			RetVal=0.0f;
//		}
		if(channel==0)
		{
			hFadeInOut->subcounter++;
			if (hFadeInOut->subcounter>=(hFadeInOut->transition_length/100))
			{
				hFadeInOut->subcounter=0;
				hFadeInOut->counter--;
				if (hFadeInOut->counter==0)
				{
					hFadeInOut->activated=0;
				}
			}
		}


	}
	else
	{
		RetVal = InVal;
	}

	return RetVal;

}

float FadeIn(float InVal, FadeInOut_HandleTypeDef * hFadeInOut)
{
	float RetVal;
	if(hFadeInOut->transition_length<100) hFadeInOut->transition_length=100;
	//FadeIn out
	if(hFadeInOut->activated)
	{
		hFadeInOut->subcounter++;
		if (hFadeInOut->subcounter>=(hFadeInOut->transition_length/100))
		{
			hFadeInOut->subcounter=0;
			hFadeInOut->counter--;
			if (hFadeInOut->counter==0)
			{
				hFadeInOut->activated=0;
			}
		}
		RetVal = Fader(InVal,hFadeInOut->counter);
	}
	else
	{
		RetVal = InVal;
	}

	return RetVal;

}

float FadeIn_Stereo(float InVal, FadeInOut_HandleTypeDef * hFadeInOut, uint8_t channel)
{
	float RetVal;
	if(hFadeInOut->transition_length<100) hFadeInOut->transition_length=100;
	//FadeIn out
	if(hFadeInOut->activated)
	{
		if(channel==0)
		{
			hFadeInOut->subcounter++;
			if (hFadeInOut->subcounter>=(hFadeInOut->transition_length/100))
			{
				hFadeInOut->subcounter=0;
				hFadeInOut->counter--;
				if (hFadeInOut->counter==0)
				{
					hFadeInOut->activated=0;
				}
			}
		}
		RetVal = Fader(InVal,hFadeInOut->counter);
	}
	else
	{
		RetVal = InVal;
	}

	return RetVal;

}

float FadeOut(float InVal, FadeInOut_HandleTypeDef * hFadeInOut)
{
	float RetVal;
	if(hFadeInOut->transition_length<100) hFadeInOut->transition_length=100;
	//FadeIn out
	if(hFadeInOut->activated)
	{


		hFadeInOut->subcounter++;
		if (hFadeInOut->subcounter>=(hFadeInOut->transition_length/100))
		{
			hFadeInOut->subcounter=0;
			hFadeInOut->counter--;
			if (hFadeInOut->counter==0)
			{
				hFadeInOut->activated=0;
			}
		}
		RetVal = Fader(InVal,100-hFadeInOut->counter);

	}
	else
	{
		RetVal = InVal;
	}

	return RetVal;

}

float FadeOut_Stereo(float InVal, FadeInOut_HandleTypeDef * hFadeInOut, uint8_t channel)
{
	float RetVal;
	if(hFadeInOut->transition_length<100) hFadeInOut->transition_length=100;

	//FadeIn out
	if(hFadeInOut->activated)
	{
		if(channel==0)
		{
			hFadeInOut->subcounter++;
			if (hFadeInOut->subcounter>=(hFadeInOut->transition_length/100))
			{
				hFadeInOut->subcounter=0;
				hFadeInOut->counter--;
				if (hFadeInOut->counter==0)
				{
					hFadeInOut->activated=0;
				}
			}
		}
		RetVal = Fader(InVal,100-hFadeInOut->counter);
	}
	else
	{
		RetVal = InVal;
	}

	return RetVal;

}

//float FadeOut_Stereo_f_ms(float InVal, FadeInOut_f_ms_HandleTypeDef * hFadeInOut, uint8_t channel)
//{
//	float RetVal;
//	if(hFadeInOut->transition_length<100) hFadeInOut->transition_length=100;
//
//	//FadeIn out
//	if(hFadeInOut->activated)
//	{
//		if(channel==0)
//		{
//			hFadeInOut->subcounter++;
//			if (hFadeInOut->subcounter>=(hFadeInOut->transition_length/100))
//			{
//				hFadeInOut->subcounter=0;
//				hFadeInOut->counter--;
//				if (hFadeInOut->counter==0)
//				{
//					hFadeInOut->activated=0;
//				}
//			}
//		}
//		RetVal = Fader(InVal,100-hFadeInOut->counter);
//	}
//	else
//	{
//		RetVal = InVal;
//	}
//
//	return RetVal;
//
//}

float Fader_f(float Sample, float Level)
{

//	assert_param(Level<=1000.0f);
	{
		float retval = (Sample * Level);
		return retval;
	}
	//	printf("Sample: %d, Divider: %d, Result: %d \n", Sample,Divider,retval);

}

float Fader(float Sample, float Divider)
{

	assert_param(Divider<=100.0f);
	{
		float retval = (Sample * Divider / 100.0f);
		return retval;
	}
	//	printf("Sample: %d, Divider: %d, Result: %d \n", Sample,Divider,retval);

}

float Fader_log(float Sample, uint32_t db_level)
{

	//	assert_param(Divider<=100.0f);
	{
		static float old_gain,old_val;
		float retval;
		float gain = gain_array[db_level];
		//		gain = LPF_f32(gain,old_gain,0.1f);
		retval = (Sample * gain);
		retval = LPF_f32(retval,old_val,0.001f);
		old_gain = gain;
		old_val = retval;

		return retval;
	}
	//	printf("Sample: %d, Divider: %d, Result: %d \n", Sample,Divider,retval);

}

/*
 * wavetable_init.h
 *
 *  Created on: 3 ???. 2019 ?.
 *      Author: Tim
 */

#ifndef AUDIO_WAVETABLE_INIT_H_
#define AUDIO_WAVETABLE_INIT_H_

#include "math.h"

#define PI_CONST 3.142f
void sine_init(float * pArray, int size, float Amplitude,float freq);

#endif /* AUDIO_WAVETABLE_INIT_H_ */

/*
 * feedback.c
 *
 *  Created on: 14 ????. 2018 ?.
 *      Author: Tim
 */

#include "feedback.h"


void feedback_init(Feedback_HandleTypeDef * pHandle, uint32_t compress_max_sample, float compressor_treshold, uint32_t bottom_val, float fb_strength)
{
	compressor_init_struct(compress_max_sample,compressor_treshold,&pHandle->Feedback_compressor);
	pHandle->bottom_val=bottom_val;
	pHandle->fb_coef=fb_strength;
}

inline float apply_feedback(float outval,float inval, Feedback_HandleTypeDef * pHandle)
{
	float retval;
	float fb_level = pHandle->level/FB_COEF;// /pHandle->fb_coef;

	fb_level = fb_level<=0.014f ? 0.0f : (fb_level > 1.1f ? 1.1f : fb_level-0.014);
	retval = outval*fb_level;
	retval=(inval+retval);

	retval=compress_struct(retval,&pHandle->Feedback_compressor);

	//	retval=limiter(retval);
	return retval;
}


void Feedback_multich(float *OutputValue, float * InputValue,float * pTarget, Feedback_HandleTypeDef * pHandle, uint8_t channels_number)
{
//	float Out_Buffer[channels_number];
	switch (pHandle->type)
	{
	case FEEDBACK_DIRECT:
		pTarget[0] = apply_feedback(OutputValue[0],InputValue[0],pHandle);
		pTarget[1] = apply_feedback(OutputValue[1],InputValue[1],pHandle);
//		for(int i=0; i<channels_number; i++)
//		{
//			pTarget[i] = apply_feedback(OutputValue[i],InputValue[i],pHandle);
//		}
		break;
	case FEEDBACK_PING_PONG:
		for(int i=0; i<channels_number; i+=2)
		{
			float mono_val = (InputValue[i]+InputValue[i+1])/2.0f;
			pTarget[i] = apply_feedback(OutputValue[i+1],mono_val,pHandle);
			pTarget[i+1] = OutputValue[0];
		}
		break;
	case FEEDBACK_CROSSOVER:
		for(int i=0; i<channels_number; i+=2)
		{
			float mono_val = (InputValue[i]+InputValue[i+1])/2.0f;
			pTarget[i] = apply_feedback(OutputValue[i+1],InputValue[i],pHandle);
			pTarget[i+1] = apply_feedback(OutputValue[i],InputValue[i+1],pHandle);
		}
		break;
	default: while(1){}
	}
}

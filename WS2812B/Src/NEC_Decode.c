#include "NEC_Decode.h"

DMA_HandleTypeDef hdma_tim3_ch1;
TIM_HandleTypeDef htim3;
NEC nec;

void NEC_Init(void (*LED_IR_capture_cmd)(uint16_t address, uint8_t cmd),void (*LED_IR_capture_repeat)(uint8_t mode))
{
  NEC_Periph_Init(&hdma_tim3_ch1,&htim3);
  HAL_Delay(10);
  
  nec.timerHandle = &htim3;  
  nec.timerChannel = TIM_CHANNEL_1;
  nec.timerChannelActive = HAL_TIM_ACTIVE_CHANNEL_1;  
  nec.timingBitBoundary = 150;
  nec.timingAgcBoundary = 1250;
  nec.type = NEC_EXTENDED;  
  nec.NEC_DecodedCallback = LED_IR_capture_cmd;
  nec.NEC_ErrorCallback = myNecErrorCallback;
  nec.NEC_RepeatCallback = LED_IR_capture_repeat;
  
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 4, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
}

void NEC_Periph_Init(DMA_HandleTypeDef *DMA_NEC_Handle, TIM_HandleTypeDef *htim3)
{
  __HAL_RCC_DMA1_CLK_ENABLE();
  
  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_SlaveConfigTypeDef sSlaveConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_IC_InitTypeDef sConfigIC;
  
  htim3->Instance = TIM3;
  htim3->Init.Prescaler = 640;
  htim3->Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3->Init.Period = 0xFFFF;
  htim3->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  HAL_TIM_Base_Init(htim3);
  
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(htim3, &sClockSourceConfig);
  
  HAL_TIM_IC_Init(htim3);
  
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_RESET;
  sSlaveConfig.InputTrigger = TIM_TS_TI1FP1;
  sSlaveConfig.TriggerPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  sSlaveConfig.TriggerFilter = 2;
  HAL_TIM_SlaveConfigSynchronization(htim3, &sSlaveConfig);
  
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(htim3, &sMasterConfig);
  
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 2;
  HAL_TIM_IC_ConfigChannel(htim3, &sConfigIC, TIM_CHANNEL_1);
  
  
}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim)
{
  
  if(htim->Instance==TIM3)
  {
    __TIM3_CLK_ENABLE();
    DMA_Channel_TypeDef *dma_ch = DMA1_Channel6;
    hdma_tim3_ch1.Instance = dma_ch;
    hdma_tim3_ch1.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_tim3_ch1.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_tim3_ch1.Init.MemInc = DMA_MINC_ENABLE;
    hdma_tim3_ch1.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
    hdma_tim3_ch1.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
    hdma_tim3_ch1.Init.Mode = DMA_NORMAL;
    hdma_tim3_ch1.Init.Priority = DMA_PRIORITY_LOW;
    HAL_DMA_Init(&hdma_tim3_ch1);  // DMA INIT IS MODIFIED TO REMOVE HALF TRANSFER INTS!!!!
    
    __HAL_LINKDMA(htim,hdma[TIM_DMA_ID_CC1],hdma_tim3_ch1);
    
    HAL_NVIC_SetPriority(TIM3_IRQn, 3, 0);
    HAL_NVIC_EnableIRQ(TIM3_IRQn);
    TIM3->DIER |= TIM_DIER_CC1IE;
    
  }
}


void myNecErrorCallback(void) {
  HAL_Delay(10);
  NEC_Read();
}

void DMA1_Channel6_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&hdma_tim3_ch1);
  
  NEC* handle = &nec;
  
  if (handle->state == NEC_INIT) {
    
    HAL_TIM_IC_Stop_DMA(handle->timerHandle, handle->timerChannel);
    
    if (handle->rawTimerData[1] < handle->timingAgcBoundary) {
      handle->state = NEC_OK;
      handle->NEC_RepeatCallback(1);
    } else {
      handle->state = NEC_AGC_OK;
      handle->rawTimerData[0]=0;
      handle->rawTimerData[1]=0;
      DMA1_Channel6->CNDTR=32;
      HAL_TIM_IC_Start_DMA(handle->timerHandle, handle->timerChannel,
			   (uint32_t*) handle->rawTimerData, 32);
    }
    
  } else if (handle->state == NEC_AGC_OK) {
    
    HAL_TIM_IC_Stop_DMA(handle->timerHandle, handle->timerChannel);
    
    for (int pos = 0; pos < 32; pos++) {
      int time = handle->rawTimerData[pos];
      if (time > handle->timingBitBoundary) {
	handle->decoded[pos / 8] |= 1 << (pos % 8);
      } else {
	handle->decoded[pos / 8] &= ~(1 << (pos % 8));
      }
    }
    
    uint8_t valid = 1;
    
    uint8_t naddr = ~handle->decoded[0];
    uint8_t ncmd = ~handle->decoded[2];
    
    if (handle->type == NEC_NOT_EXTENDED && handle->decoded[1] != naddr)
      valid = 0;
    if (handle->decoded[3] != ncmd)
      valid = 0;
    
    handle->state = NEC_OK;
    
    if (valid)
      handle->NEC_DecodedCallback(handle->decoded[0], handle->decoded[2]);
    else
      handle->NEC_ErrorCallback();
  }
}

void TIM3_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim3);
}

void NEC_Read(void) { //void NEC_Read(NEC* handle) { removed handle
  nec.state = NEC_INIT;
  HAL_TIM_IC_Start_DMA(nec.timerHandle, nec.timerChannel,
		       (uint32_t*) nec.rawTimerData, 2);
}

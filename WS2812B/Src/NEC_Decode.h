
//*********************************Boring shit******************************
// IR NEC control driver
// Credits - TeemTek 2016. 
//*********************************Saftey first******************************
#ifndef INC_NEC_DECODE_H_
#define INC_NEC_DECODE_H_
//*********************************Includes******************************
#include <stm32f3xx_hal.h>
//*********************************Defines******************************
#define REPEAT_CMD      0xFF
//*********************************Structures******************************
typedef enum {
    NEC_NOT_EXTENDED, NEC_EXTENDED
} NEC_TYPE;

typedef enum {
    NEC_INIT, NEC_AGC_OK, NEC_AGC_FAIL, NEC_FAIL, NEC_OK
} NEC_STATE;

typedef struct {
    int rawTimerData[32];
    uint8_t decoded[4];

    NEC_STATE state;

    TIM_HandleTypeDef *timerHandle;

    uint32_t timerChannel;
    HAL_TIM_ActiveChannel timerChannelActive;

    uint16_t timingBitBoundary;
    uint16_t timingAgcBoundary;
    NEC_TYPE type;

    void (*NEC_DecodedCallback)(uint16_t, uint8_t);
    void (*NEC_ErrorCallback)(void);
    void (*NEC_RepeatCallback)(uint8_t);
} NEC;

typedef struct {
  uint16_t address;
  uint8_t cmd;
} NEC_INSTR;
//*********************************Static prototypes******************************
//*********************************Prototypes******************************

//Initializes drier. Pass a callback functions:
// IRfun -> Function for IR successfull command decode 
// IRrepeat -> Function for repeat decode
void NEC_Init(void (*IRfun)(uint16_t address, uint8_t cmd),void (*LED_IR_repeat)(uint8_t mode));
//Initializes driver peripherials
//You ned a timer and DMA for that
void NEC_Periph_Init(DMA_HandleTypeDef *DMA_NEC_Handle, TIM_HandleTypeDef *htim3);

//Case of error in decoding
void NEC_ErrorCallback(void);

//void NEC_RepeatCallback(void);

//Start listening for IR port
void NEC_Read(void);

//Callbacks
void myNecDecodedCallback(uint16_t address, uint8_t cmd);
void myNecErrorCallback(void);
void myNecRepeatCallback(void);



//Peripherials init. 
//ToDo: I guess it would be neat to have these somewhere not in the driver.
void Timer3_init(TIM_HandleTypeDef *phtim3);
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim);
void Timer3_init(TIM_HandleTypeDef *phtim3);
//Peripherials interrupt handles. 
//ToDo: I guess it would be neat to have these somewhere not in the driver.
void DMA1_Channel6_IRQHandler(void);
void TIM3_IRQHandler(void);
//*********************************END******************************
#endif /* INC_NEC_DECODE_H_ */

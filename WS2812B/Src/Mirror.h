
//*********************************Boring shit******************************
//Infinity mirror control driver
// Uses WS2812 LED driver + NEC IR remote control
// Lets you to have 4x different modes
// With different brightness and speeds
// Uses HSV colorspace for interacting with LEDs
// Credits - TeemTek 2016. 

//*********************************Saftey first******************************
#ifndef _MIRROR_
#define _MIRROR_
//*********************************Includes******************************
#include "IO_funcs.h"
#include "NEC_Decode.h"
#include "WS2812.h"

//*********************************Defines******************************
//speeds definitions
#define SOFT_SPEED_L    1
#define SOFT_SPEED_H  	360
#define SOFT_SPEED_STP  5

#define STROBE_SPEED_L	1
#define STROBE_SPEED_H	100


//IR COMMANDS
#define CMD_LIGHT_UP	0x00
#define CMD_LIGHT_DOWN	0x01
#define CMD_OFF		0x02
#define CMD_ON		0x03
#define CMD_RED1	0x04
#define CMD_GRN1	0x05
#define CMD_BLU1	0x06
#define CMD_WHITE	0x07
#define CMD_RED2	0x08
#define CMD_GRN2	0x09
#define CMD_BLU2	0xA
#define CMD_FLASH	0xB
#define CMD_RED3	0xC
#define CMD_GRN3	0xD
#define CMD_BLU3	0xE
#define CMD_STROBE	0xF
#define CMD_ORN1	0x10
#define CMD_CYA1	0x11
#define CMD_PUR1	0x12
#define CMD_FADE	0x13
#define CMD_ORN2	0x14
#define CMD_CYA2	0x15
#define CMD_PUR2	0x16
#define CMD_SMOOTH	0x17
#define REPEAT_CMD      0xFF

//*********************************Structures******************************
typedef enum {
   LED_STATE_INIT
} LED_STATE;
typedef enum {
   LED_FLASH_MODE, LED_STROBE_MODE, LED_FADE_MODE, LED_SMOOTH_MODE
} LED_MODE;
typedef enum {
   LED_OFF, LED_ON
} LED_ONOFF;
typedef enum {
   COLORMODE_RED1, COLORMODE_RED2, COLORMODE_RED3, COLORMODE_ORANGE1,
   COLORMODE_ORANGE2, COLORMODE_GREEN1, COLORMODE_GREEN2, COLORMODE_CYAN1,
   COLORMODE_CYAN2, COLORMODE_BLUE1, COLORMODE_BLUE2, COLORMODE_BLUE3,
   COLORMODE_PURPLE1, COLORMODE_PURPLE2, COLORMODE_RAINBOW, COLORMODE_WHITE
} LED_COLORMODE;

typedef struct {
  
    LED_STATE state;
    LED_MODE mode; 
    LED_ONOFF power;
    LED_COLORMODE colormode;
    LED_COLORMODE colormode_aux;
    HSV LED_Color_main;
    HSV LED_Color_aux;
    uint8_t prev_cmd[5];
    uint16_t speed_smooth;
    uint8_t speed_strobe;
    uint32_t speed_flash;
    uint8_t speed_fade;
} LEDstrip;

//*********************************Static prototypes******************************
static void Mirror_Speed_Setup(void);

static void Mirror_Repeat_exc(int cmd);
static void Mirror_Repeat(uint8_t mode);

static void Mirror_Stobe_Mode(void);
static void Mirror_Flash_Mode(void);
static void Mirror_Smooth_Mode(void);

//*********************************Prototypes******************************
void Mirror_Init(void);
void Mirror_Start(void);

void Mirror_Capture_Cmd(uint16_t address, uint8_t cmd);
void Mirror_Capture_Repeat(uint8_t mode);

void Mirror_Control_Process(uint16_t address, uint8_t cmd);

void Mirror_hue_rotate(uint16_t Hstep, uint16_t range_low, uint16_t range_high);
//*********************************END******************************
#endif
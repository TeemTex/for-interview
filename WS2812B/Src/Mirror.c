
#include "Mirror.h"
LEDstrip Mirror1;		//This contains all setup for mirror 



uint8_t repeat=0;		//some globals I was too stupid to avoid
uint8_t held_cycle_cnt=0;
uint8_t held_onhold=0;
uint8_t speed_warning=0;

static void Mirror_Speed_Setup(void)
{
  if (Mirror1.speed_smooth>SOFT_SPEED_H-SOFT_SPEED_STP)
  {   
    if (speed_warning==1)
    {
      Mirror1.speed_smooth=SOFT_SPEED_L;  //Set lowermost speed 
      Mirror1.speed_strobe=STROBE_SPEED_L;
      speed_warning=0;
    }
    else
    {
      Mirror1.speed_strobe=STROBE_SPEED_H;
      Mirror1.speed_smooth=SOFT_SPEED_H;
      HSV ZeroHSV;
      ZeroHSV.hue=0;
      ZeroHSV.sat=0;
      ZeroHSV.val=0;
      WS2812_fill_color(ZeroHSV,0,LEDS_NUM);      //Blink with all off for 500ms to warn
      HAL_Delay(500);                 
      speed_warning=1;
    }
  }
  else
  {
    if (speed_warning==0)
    {
      if (Mirror1.speed_smooth<50 || Mirror1.speed_smooth>304)
      {
	Mirror1.speed_smooth+=SOFT_SPEED_STP;
      }
      else
      {
	Mirror1.speed_smooth+=51;
      }
      if (Mirror1.speed_strobe<38)
      {
	Mirror1.speed_strobe+=2;
      }
      else
      {
	Mirror1.speed_strobe+=8;
      }
      if (Mirror1.mode==LED_FLASH_MODE)
      {
	Mirror1.speed_flash+=3;
	if (Mirror1.speed_flash>30) Mirror1.speed_flash=1;
      }
    }
    
  }
  //  printf("Soft: %d; ", Mirror1.speed_smooth);	//NEC code analiz
  //  printf("Strobe: %d; ", Mirror1.speed_strobe);	//NEC code analiz 
  //  printf("\n");
}
static void Mirror_Repeat_exc(int cmd)              //TODO: fix this shit
{
  
  switch (cmd) 
  {
   case CMD_FLASH:
   case CMD_STROBE:
   case CMD_FADE:
   case CMD_SMOOTH:
    Mirror_Speed_Setup();
    break;
   case CMD_ON:
   case CMD_OFF: 
    break; 
   case CMD_LIGHT_UP:
    if (Mirror1.LED_Color_main.val<238) Mirror1.LED_Color_main.val+=17;
    break;
   case CMD_LIGHT_DOWN:
    if (Mirror1.LED_Color_main.val>34) Mirror1.LED_Color_main.val-=17;
    break;
   default:      //COLORMODE
    repeat++;
    if (repeat==1) Mirror1.colormode_aux=Mirror1.colormode;
    if (repeat==2) {Mirror1.colormode = COLORMODE_RAINBOW; repeat=0;}
    break;
  }
}
void Mirror_Repeat(uint8_t mode)
{
  
  if (mode==0)          //Same button pressed again
  {
    Mirror_Repeat_exc(Mirror1.prev_cmd[0]);
    held_cycle_cnt=0;
  }
  else  //Button is being held
  {  
    if  (held_cycle_cnt>20)
    {
      Mirror_Repeat_exc(Mirror1.prev_cmd[0]);
      held_cycle_cnt=10;
    }
    else
    {
      held_cycle_cnt++;
    }  
  }
}


void Mirror_Capture_Cmd(uint16_t address, uint8_t cmd)
{
  Mirror_Control_Process(address,cmd);
  HAL_Delay(10);
  NEC_Read();
}

void Mirror_Capture_Repeat(uint8_t mode)
{
  Mirror_Control_Process(0,REPEAT_CMD);
  HAL_Delay(10);
  NEC_Read();
}

void Mirror_Control_Process(uint16_t address, uint8_t cmd)
{ 
  if (address==0) //IF address OK
  { 
    //Check wether button is held 
    if (cmd==REPEAT_CMD)
    { 
      Mirror_Repeat(cmd);
      return;
    }
    //or being pressed repitedley
    if (Mirror1.prev_cmd[0]==cmd) 
    {
      Mirror_Repeat(0x00);
      return;
    }
    held_cycle_cnt=0;
    //Switch for commands
    switch (cmd)
    {
     case CMD_LIGHT_DOWN:
      if (Mirror1.LED_Color_main.val>34) Mirror1.LED_Color_main.val-=17;
      break;
     case CMD_LIGHT_UP:
      if (Mirror1.LED_Color_main.val<238) Mirror1.LED_Color_main.val+=17;
      break;
     case CMD_OFF:
      Mirror1.power = LED_OFF;
      break;
     case CMD_ON:
      Mirror1.power = LED_ON;
      break;    
     case CMD_RED1:
      Mirror1.LED_Color_main.hue = LED_HUE_RED1;
      Mirror1.colormode=COLORMODE_RED1;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_GRN1:
      Mirror1.LED_Color_main.hue = LED_HUE_GREEN1;
      Mirror1.colormode=COLORMODE_GREEN1;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_BLU1:
      Mirror1.LED_Color_main.hue = LED_HUE_BLUE1;
      Mirror1.colormode=COLORMODE_BLUE1;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_WHITE:
//      Mirror1.LED_Color_main.val = 255;
      Mirror1.colormode=COLORMODE_WHITE;
      break;    
     case CMD_RED2:
      Mirror1.LED_Color_main.hue = LED_HUE_RED2;
      Mirror1.colormode=COLORMODE_RED2;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_GRN2:
      Mirror1.LED_Color_main.hue = LED_HUE_GREEN2;
      Mirror1.colormode=COLORMODE_GREEN2;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_BLU2:
      Mirror1.LED_Color_main.hue = LED_HUE_BLUE2;
      Mirror1.colormode=COLORMODE_BLUE2;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_FLASH:
      Mirror1.mode = LED_FLASH_MODE;
      break;    
     case CMD_RED3:
      Mirror1.LED_Color_main.hue = LED_HUE_RED3;
      Mirror1.colormode=COLORMODE_RED3;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_GRN3:
      Mirror1.LED_Color_main.hue = LED_HUE_GREEN2;
      Mirror1.colormode=COLORMODE_GREEN2;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_BLU3:
      Mirror1.LED_Color_main.hue = LED_HUE_BLUE3;
      Mirror1.colormode=COLORMODE_BLUE3;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_STROBE:
      Mirror1.mode = LED_STROBE_MODE;
      break;    
     case CMD_ORN1:
      Mirror1.LED_Color_main.hue = LED_HUE_ORANGE1;
      Mirror1.colormode=COLORMODE_ORANGE1;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_CYA1:
      Mirror1.LED_Color_main.hue = LED_HUE_CYAN1;
      Mirror1.colormode=COLORMODE_CYAN1;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_PUR1:
      Mirror1.LED_Color_main.hue = LED_HUE_PURPLE1;
      Mirror1.colormode=COLORMODE_PURPLE1;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;    
     case CMD_FADE:
      Mirror1.mode = LED_FADE_MODE;
      break;
     case CMD_ORN2:
      Mirror1.LED_Color_main.hue = LED_HUE_ORANGE2;
      Mirror1.colormode=COLORMODE_ORANGE2;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;
     case CMD_CYA2:
      Mirror1.LED_Color_main.hue = LED_HUE_CYAN2;
      Mirror1.colormode=COLORMODE_CYAN2;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;
     case CMD_PUR2:
      Mirror1.LED_Color_main.hue = LED_HUE_PURPLE2;
      Mirror1.colormode=COLORMODE_PURPLE2;
      //      WS2812_fill_color(Mirror1.LED_Color_main,0,LEDS_NUM);
      break;
     case CMD_SMOOTH:
      Mirror1.mode = LED_SMOOTH_MODE;
      break;
     default:
      break;
    }
    Mirror1.prev_cmd[0]=cmd;
  }
}

static void Mirror_Stobe_Mode(void)
{
  
//  if (Mirror1.colormode==COLORMODE_WHITE) Mirror_hue_rotate(10,0,360);
  WS2812_fill_color(Mirror1.LED_Color_main, 0,LEDS_NUM);

  HAL_Delay(Mirror1.speed_strobe);
  WS2812_Fast_Zeros();
  if (Mirror1.colormode==COLORMODE_WHITE) Mirror_hue_rotate(30,0,360);
  HAL_Delay(Mirror1.speed_strobe*10);

}
static void Mirror_Flash_Mode(void)		//chaser
{
  Mirror1.LED_Color_main.sat = 255;
  int hue_cycles=3;
  int hue_per_cycle=360/hue_cycles;
  int LEDS_per_hue=LEDS_NUM/hue_cycles;
  int val_step=255/LEDS_per_hue;
  int extra_leds = LEDS_NUM % hue_cycles;
  
  int i,j;
  uint8_t k;
  uint8_t counter1;
  while(Mirror1.mode==LED_FLASH_MODE)
  {
    for (i=0; i<hue_cycles;  i++)	//Full circle with hue_cycles inside
    {
      Mirror_hue_rotate(120,0,360);	//Rotate color in beggingin of new cycle
      Mirror1.LED_Color_main.val=0;	//New cycle always starts with max light
      
      if (extra_leds>=i)	counter1=LEDS_per_hue+1;//leftover LEDs
      else			counter1=LEDS_per_hue;	//+1 to get shift moving
	
      for (j=0; j<counter1; j++)		//Iterate all LEDs in a color cycle
      {
	k++;					//Increment LED
	if (k>=LEDS_NUM)				//LEDS_NUM HERE!! ToDo! 
	{ 
	  Mirror_hue_rotate(10,0,360);
	  k=0;
	}
	
	WS2812_Set_LED(k,Mirror1.LED_Color_main);	//output
	Mirror1.LED_Color_main.val+=val_step;		//Decrement light for the next step
	
	int delay=0; 			//HAL_Delay sucks here (because of interrupts?)
	while (delay<Mirror1.speed_flash*250)	//Time to sit back and enjoy 
	{
	  delay++;
	}
      }     
    }
  }  
}

//Smooth mode rotates trought hue of colors. 
//Use hue_p_cycle to define speed and colors  
//360 or 0 -> stand still
//1 or 359 -> super smooth rotation
//120 - fast shit
static void Mirror_Smooth_Mode(void)
{
  int hue_p_led = Mirror1.speed_smooth/LEDS_NUM;
  int extra_leds = Mirror1.speed_smooth % LEDS_NUM;
  
  for (int i=0; i<LEDS_NUM; i++)
  {
    if (i<(LEDS_NUM-extra_leds))
    {
      Mirror1.LED_Color_main.hue += hue_p_led;
    }
    else 
    {
      Mirror1.LED_Color_main.hue += hue_p_led+1;
    }
    if (Mirror1.LED_Color_main.hue>359)  Mirror1.LED_Color_main.hue=Mirror1.LED_Color_main.hue-360; 
    WS2812_Set_LED(i,Mirror1.LED_Color_main);
  }
  
  int i = 0;
  while(Mirror1.mode==LED_SMOOTH_MODE)      //endless loop of rainbow unicorns
  {
    hue_p_led = Mirror1.speed_smooth/LEDS_NUM;
    extra_leds = Mirror1.speed_smooth % LEDS_NUM;
    if (i<(LEDS_NUM-extra_leds))
    {
      Mirror1.LED_Color_main.hue += hue_p_led;
    }
    else 
    {
      Mirror1.LED_Color_main.hue += hue_p_led+1;
    }
    if (Mirror1.LED_Color_main.hue>359)  
    {Mirror1.LED_Color_main.hue=Mirror1.LED_Color_main.hue-360; }
    WS2812_Set_LED(i,Mirror1.LED_Color_main);
    HAL_Delay(5);  //Mirror_delay vozvrashaet aktualnoe znacenie 
    
    if (i<LEDS_NUM) i++;
    else i=0;
    
  }
  
}

void Mirror_Start(void)       //entry point
{
  NEC_Read(); 	//Start listening for remote control
  while(1)
  {
    if (Mirror1.power == LED_ON)
    {
      switch (Mirror1.mode)
      {
       case LED_SMOOTH_MODE:
	Mirror_Smooth_Mode();
	break;
	
       case LED_STROBE_MODE:
	Mirror_Stobe_Mode();
	break;
	
       case LED_FLASH_MODE:
	Mirror_Flash_Mode();
	break;
       case LED_FADE_MODE:
	Mirror_Stobe_Mode();
	break;
	
       default:
	Mirror_Smooth_Mode();
	break;
      } 
    }
    else
    {
      HSV ZeroHSV;
      ZeroHSV.hue=0;
      ZeroHSV.sat=0;
      ZeroHSV.val=0;
      WS2812_fill_color(ZeroHSV,0,LEDS_NUM);
    }
  }
}

void Mirror_Init(void)
{
  Mirror1.power = LED_OFF;
  Mirror1.mode = LED_SMOOTH_MODE;
  Mirror1.speed_smooth = 1;
  Mirror1.speed_strobe = 1;
  Mirror1.speed_flash = 1;
  Mirror1.speed_fade = 1;
  Mirror1.LED_Color_main.val = 256/2;
  Mirror1.LED_Color_main.hue = LED_HUE_GREEN1;
  Mirror1.LED_Color_main.sat = 255;
  Mirror1.state = LED_STATE_INIT;
  
  
  IO_pins_init();
  WS2812_Init();  
  
  NEC_Init(Mirror_Capture_Cmd,Mirror_Capture_Repeat);
  HAL_Delay(10);
}

void Mirror_hue_rotate(uint16_t Hstep, uint16_t range_low, uint16_t range_high)
{
  Mirror1.LED_Color_main.hue+=Hstep;
  if (Mirror1.LED_Color_main.hue>=range_high) Mirror1.LED_Color_main.hue = range_low + Mirror1.LED_Color_main.hue % range_high;
  if (Mirror1.LED_Color_main.hue<range_low) Mirror1.LED_Color_main.hue = range_low;
}
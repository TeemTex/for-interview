#ifndef __IO_FUNCS
  #define __IO_FUNCS

#include "stm32f3xx.h"

#define STATE_INPUT(gpio,bit) ((gpio##->IDR >> bit) & 1) 
#define STATE_PIN(gpio,bit) ((gpio##->ODR >> bit) & 1) 
#define SET_PIN(gpio,bit, val)   {if ( val != 0 ) gpio##->BSRRL = (1 << bit); else gpio##->BSRRH = (1 << bit);}
#define TOGGLE_PIN(gpio,bit) {if ( ((gpio##->ODR >> bit) & 1) == 0 ) gpio##->BSRRL = (1 << bit); else gpio##->BSRRH = (1 << bit);}

#define   INPUT__       0
#define   OUTPUT_       1
#define   ALTFUNC       2
#define   ANALOG_       3

#define   PUSH_PULL     0
#define   OPEN_DRAIN    1

#define   S_002MHZ      0
#define   S_025MHZ      1
#define   S_050MHZ      2
#define   S_100MHZ      3

#define   NOPUPD___     0
#define   PULL_UP__     1
#define   PULL_DOWN     2

typedef struct
{
  GPIO_TypeDef *port;
  int pin_num;
  int mode;
  int type;
  int speed;
  int pupd;
  int alt_func_id;
  int initv;

} T_IO_pins_configuration;

void IO_pins_init(void);

#endif

#include "IO_funcs.h"

const T_IO_pins_configuration pins_conf[] =
{
  { GPIOB, 3, ALTFUNC, OPEN_DRAIN, S_050MHZ, NOPUPD___, 1, 0 },// WS2812 OPEN_DRAIN. 5V tolerant!
  { GPIOB, 4, ALTFUNC, PUSH_PULL, S_050MHZ, NOPUPD___, 2, 0 }, // IR NEC DMA reciever
};


// GPIO init code 
void IO_pins_init(void)
{
  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

    int i;
  
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
  
    for (i = 0; i < (sizeof(pins_conf) / sizeof(pins_conf[0])); i++)
    {
      if ( pins_conf[i].mode == ALTFUNC )
      {
        int n;
        if ( pins_conf[i].pin_num < 8 )
        {
          n = pins_conf[i].pin_num;
          pins_conf[i].port->AFR[0] = (pins_conf[i].port->AFR[0] & ~(0x0F << (n * 4))) | (pins_conf[i].alt_func_id << (n * 4));
        }
        else
        {
          n = pins_conf[i].pin_num - 8;
          pins_conf[i].port->AFR[1] = (pins_conf[i].port->AFR[1] & ~(0x0F << (n * 4))) | (pins_conf[i].alt_func_id << (n * 4));
        }
      }
      pins_conf[i].port->OSPEEDR = (pins_conf[i].port->OSPEEDR & ~(0x03 << (pins_conf[i].pin_num * 2))) | (pins_conf[i].speed << (pins_conf[i].pin_num * 2));
      pins_conf[i].port->OTYPER = (pins_conf[i].port->OTYPER & ~(0x01 << (pins_conf[i].pin_num))) | (pins_conf[i].type << (pins_conf[i].pin_num));
      pins_conf[i].port->PUPDR = (pins_conf[i].port->PUPDR & ~(0x03 << (pins_conf[i].pin_num * 2))) | (pins_conf[i].pupd << (pins_conf[i].pin_num * 2));
      pins_conf[i].port->ODR = (pins_conf[i].port->ODR & ~(0x01 << (pins_conf[i].pin_num))) | (pins_conf[i].initv << (pins_conf[i].pin_num));
      pins_conf[i].port->MODER = (pins_conf[i].port->MODER & ~(0x03 << (pins_conf[i].pin_num * 2))) | (pins_conf[i].mode << (pins_conf[i].pin_num * 2));
    }
}



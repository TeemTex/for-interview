#include "WS2812.h"

 uint32_t DMA_buf[LEDS_NUM+2][COLRS][8];
 INT32U LEDS_buf[LEDS_NUM]; //stores copy of DMA buf state 
 INT32U LEDS_buf0[LEDS_NUM]; //stores copy of DMA buf state 
 

const INT8U dim_curve[256] = {
  0, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3,
  3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4,
  4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6,
  6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8,
  8, 8, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 11, 11, 11,
  11, 11, 12, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 14, 15,
  15, 15, 16, 16, 16, 16, 17, 17, 17, 18, 18, 18, 19, 19, 19, 20,
  20, 20, 21, 21, 22, 22, 22, 23, 23, 24, 24, 25, 25, 25, 26, 26,
  27, 27, 28, 28, 29, 29, 30, 30, 31, 32, 32, 33, 33, 34, 35, 35,
  36, 36, 37, 38, 38, 39, 40, 40, 41, 42, 43, 43, 44, 45, 46, 47,
  48, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62,
  63, 64, 65, 66, 68, 69, 70, 71, 73, 74, 75, 76, 78, 79, 81, 82,
  83, 85, 86, 88, 90, 91, 93, 94, 96, 98, 99, 101, 103, 105, 107, 109,
  110, 112, 114, 116, 118, 121, 123, 125, 127, 129, 132, 134, 136, 139, 141, 144,
  146, 149, 151, 154, 157, 159, 162, 165, 168, 171, 174, 177, 180, 183, 186, 190,
  193, 196, 200, 203, 207, 211, 214, 218, 222, 226, 230, 234, 238, 242, 248, 255,
};

INT32U WS2812_HSV_to_RGB(HSV color_HSV) 
{
  int    r;
  int    g;
  int    b;
  int    base;
  INT32U rgb;
  
  color_HSV.val = dim_curve[color_HSV.val];
  color_HSV.sat = 255 - dim_curve[255 - color_HSV.sat];
  
  
  if ( color_HSV.sat == 0 ) // Acromatic color (gray). color_HSV.hue doesn't mind.
  {
    rgb = color_HSV.val | (color_HSV.val<<8) | (color_HSV.val <<16);
  }
  else
  {
    base = ((255 - color_HSV.sat) * color_HSV.val) >> 8;
    switch (color_HSV.hue / 60)
    {
    case 0:
      r = color_HSV.val;
      g = (((color_HSV.val - base) * color_HSV.hue) / 60) + base;
      b = base;
      break;
    case 1:
      r = (((color_HSV.val - base) * (60 - (color_HSV.hue % 60))) / 60) + base;
      g = color_HSV.val;
      b = base;
      break;
    case 2:
      r = base;
      g = color_HSV.val;
      b = (((color_HSV.val - base) * (color_HSV.hue % 60)) / 60) + base;
      break;
    case 3:
      r = base;
      g = (((color_HSV.val - base) * (60 - (color_HSV.hue % 60))) / 60) + base;
      b = color_HSV.val;
      break;
    case 4:
      r = (((color_HSV.val - base) * (color_HSV.hue % 60)) / 60) + base;
      g = base;
      b = color_HSV.val;
      break;
    case 5:
      r = color_HSV.val;
      g = base;
      b = (((color_HSV.val - base) * (60 - (color_HSV.hue % 60))) / 60) + base;
      break;
    }
    rgb = ((r & 0xFF)<<16) | ((g & 0xFF)<<8) | (b & 0xFF);
  }
  return rgb;
}
//*******WS2812 LOW LEVEL DRIVER************
//Sets cell data of DMA

void WS2812_Set_DMA_Cell(INT16U cell_number, uint8_t level_low, uint8_t level_high, INT32U data)
{
  for (int i = 0; i < 8; i++)
  {
    // �������
    if ( ((data >> 8) >> (7 - i)) & 1 ) DMA_buf[cell_number][0][i] = level_high;
    else DMA_buf[cell_number][0][i] = level_low;
    // �������
    if ( ((data >> 16) >> (7 - i)) & 1 ) DMA_buf[cell_number][1][i] = level_high;
    else DMA_buf[cell_number][1][i] = level_low;
    // �����
    if ( ((data >> 0) >> (7 - i)) & 1 ) DMA_buf[cell_number][2][i] = level_high;
    else DMA_buf[cell_number][2][i] = level_low;
  }
}
void WS2812_Set_DMA_Zeros(void)
{
  uint32_t i,j,k;
    for (i=0; i<LEDS_NUM;i++)
    {
      for (j=0; j<3;j++)
      {
	for (k=0; k<8;k++)
	{
	  DMA_buf[i][j][k]=LED_LEV0;
	}
      }
    }
}

void WS2812_Periph_Init(void)
{
  // Init Timer peripherial
  TIM_TypeDef *tim = TIM2               ;
  RCC_TypeDef *rcc = RCC;
  

  rcc->APB1RSTR |= BIT(0);    // ����� ������� 2
  rcc->APB1RSTR &= ~BIT(0);   
  rcc->APB1ENR |= BIT(0);     // ��������� ������������ ������� 2
  tim->CR1 = BIT(7);          //  1: TIMx_ARR register is buffered.
  tim->CR2 = 0;               
  tim->PSC = 0;               // ������������ ���������� ������� 64 ���
  tim->ARR = 80;          // ���������� ������� ������ 1.25 ���
  tim->CCMR1 = 0
               + LSHIFT(6, 12) // OC3M: Output compare 3 mode | 110: PWM mode 1 - In upcounting, channel 1 is active as long as TIMx_CNT<TIMx_CCR1 else inactive.
               + LSHIFT(1, 11) // OC3PE: Output compare 3 preload enable
               + LSHIFT(0, 8) // 00: CC3 channel is configured as output
  ; 
  tim->CNT = 0;
  tim->CCR2 = 40;
  tim->DIER = BIT(10);        // Bit 10 CC3DE: Capture/Compare 2 DMA request enable. ��������� ������� DMA
  tim->CR1 |= BIT(0);         // ��������� ������
  tim->CCER = BIT(4);         // ��������� ������ ������, ����� ��������� ������� ��� DMA
   tim->CNT = 0;
  tim->EGR |= TIM_EGR_UG_Msk;
  
  HAL_NVIC_SetPriority(TIM3_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(TIM3_IRQn);
  
  
  //Init DMA peripherial
    DMA_Channel_TypeDef *dma_ch = DMA1_Channel7;
  
  rcc->AHBENR |= RCC_AHBENR_DMA1EN;               // ��������� DMA1
  
  dma_ch->CCR = 0;    // ��������� �����
  dma_ch->CPAR = (unsigned int)&(TIM2->CCR2) + 1;  // ��������� ����� �������� ������ ADC
  dma_ch->CMAR = (unsigned long)DMA_buf;
  dma_ch->CNDTR = (LEDS_NUM + 2) * COLRS * 32;
  dma_ch->CCR =
    
    LSHIFT(1,14) + // MEM2MEM: Memory to memory mode | 0: Disabled
      LSHIFT(3, 13) + // PL[1:0]: Priority level | 11: Very high.  PL[1:0]: Priority level
        LSHIFT(2, 11) + // MSIZE[1:0]: Memory data size | 00: 8-bit. Memory data size  
          LSHIFT(2, 8) + // PSIZE[1:0]: Peripheral data size | 00: 8-bit. Peripheral data size
            LSHIFT(1, 7) + // MINC: Memory increment mode | 1: Memory address pointer is incremented after each data transfer (increment is done according to MSIZE)
              LSHIFT(0, 6) +  // PINC: Peripheral increment mode | 0: Peripheral address pointer is fixed  
                LSHIFT(1, 5) +  // CIRC: Circular mode | 1: Circular mode enabled
                  LSHIFT(1, 4) +  // DIR[1:0]: Data transfer direction | 01: Memory-to-peripheral
                    LSHIFT(0, 3) +  // TEIE: Transfer error interrupt enable | 0 : TE interrupt disabled
                      LSHIFT(0, 2) +  // HTIE: Half transfer interrupt enable | 0: HT interrupt disabled
                        LSHIFT(1, 1) +  // TCIE: Transfer complete interrupt enable | 1: TC interrupt enabled
                          LSHIFT(0, 0) +  // EN: Stream enable | 1: Stream enabled
                            0;
  
  dma_ch->CCR |= BIT(0); //  1: Stream enabled
  
  
}

void WS2812_Clear_DMA_Flags(void)
{
  DMA_TypeDef *dma = DMA1;
  dma->IFCR = 0x0FFFFFFF;  // ������� ����� �� ���� ������� DMA
}

// LED driver init. Sets all LEDs to 0
void WS2812_Init(void)
{
  WS2812_Periph_Init();
  
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);
  
  HAL_Delay(10);

  for (int i=0; i<LEDS_NUM; i++)
  {
    HSV ZeroHSV;
    ZeroHSV.hue=0;
    ZeroHSV.sat=0;
    ZeroHSV.val=0;
    WS2812_Set_LED(i,ZeroHSV);
  }
  
 

    INT32U i, j, k;
  
  for (i = 0; i < LEDS_NUM; i++)
  {
    for (j = 0; j < COLRS; j++)
    {
      for (k = 0; k < 8; k++)
      {
	DMA_buf[i][j][k] = LED_LEV0;
      }
    }
  }
  // ������ ������
  for (i = LEDS_NUM; i < LEDS_NUM + 2; i++)
  {
    for (j = 0; j < COLRS; j++)
    {
      for (k = 0; k < 8; k++)
      {
	DMA_buf[i][j][k] = 0;
      }
    }
  }
}


void WS2812_Fast_Zeros(void)
{
  WS2812_Set_DMA_Zeros();
}

// Sets individual LED in RGB mode
void WS2812_Set_LED(INT16U ledn, HSV color_HSV)
{
  uint32_t color_RGB = WS2812_HSV_to_RGB(color_HSV);
  if ( ledn >= LEDS_NUM ) return;       //check overflow?
//  LEDS_buf[ledn]=color_RGB;
  WS2812_Set_DMA_Cell(ledn,LED_LEV0,LED_LEV1,color_RGB);      //set DMA byte

}
void WS2812_Set_LED_RGB(INT16U ledn, int color_RGB)
{
  if ( ledn >= LEDS_NUM ) return;       //check overflow?
//  LEDS_buf[ledn]=color_RGB;
  WS2812_Set_DMA_Cell(ledn,LED_LEV0,LED_LEV1,color_RGB);      //set DMA byte

}
//Light all LEDs with same color in range of ledn1 ledn2
void WS2812_fill_color(HSV color, int ledn1, int ledn2) 
{
//  if ( ledn2 >= LEDS_NUM ) ledn2=LEDS_NUM-1;       //check overflow?

  int color_RGB = WS2812_HSV_to_RGB(color);
  for (int i = ledn1; i < ledn2; i++)
  {
    
//    LEDS_buf[i]=color_RGB;
    WS2812_Set_DMA_Cell(i,LED_LEV0,LED_LEV1,color_RGB);      //set DMA byte
  }
}
//Instant Move LEDs by 1 position in direction dir 
//void WS2812_push(int dir)
//{
//        if ( dir == 0 )
//      {           
//        int tmpLED = LEDS_buf[0];
//        for (int i = 0; i < LEDS_NUM; i++)
//        {
//          if ( i != (LEDS_NUM - 1) )
//          {
//             WS2812_Set_LED_RGB(i,LEDS_buf[i+1]);
//          }
//          else
//          {
//            WS2812_Set_LED_RGB(i,tmpLED);
//          }
//        }
//      }
//      else
//      {           
//        int tmpLED = LEDS_buf[LEDS_NUM-1];
//        for (int i = LEDS_NUM; i > 0; i--)
//        {
//          if (i==0)
//          {
//             WS2812_Set_LED_RGB(i,tmpLED);
//          }
//          else
//          {  
//            WS2812_Set_LED_RGB(i,LEDS_buf[i]);
//          }
//        }
//      }
//}

void DMA1_Channel7_IRQHandler(void)
{
  DMA_TypeDef *dma = DMA1;
  dma->IFCR = DMA_IFCR_CTCIF7;
}



#define  __GLOBAL__



#include "main.h"



void Init_ALL(void)
{
  //HAL Setup
  HAL_Init();
  
  
  //Clock Setup
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;    //Internal High speed
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;                      //Turn ON
  RCC_OscInitStruct.HSICalibrationValue = 16;                   
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;                  //PLL ON
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;          //PLL input->HSI
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;                 //Multiplier
  HAL_RCC_OscConfig(&RCC_OscInitStruct);                        //Send
  
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1;//Configure sysclock       
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;             //Input -> PLL
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;                    //AHB divider
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;                     //APB1 divider
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;                     //APB2 divider
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);             //Send
  
  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
  HAL_Delay(10);
  
  //debug:
  printf("Clock Freq[MHz]: %d;", HAL_RCC_GetHCLKFreq()/1000000);
  
  
  HAL_Delay(10);
  
  Mirror_Init();
  
}
int main(void)
{
  Init_ALL();
  HAL_Delay(100);
  
  Mirror_Start();
  while(1)
  {
    
  }
  
  
  
}



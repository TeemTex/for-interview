//*********************************Boring shit******************************
//WS2812B LED STRIP DRIVER
//This driver uses DMA_buf array of data
//Which DMA peripherial sends to modulate TIM generated PWM
//Credits - TeemTek 2016. 
//Created on base of 
//*********************************DRIVER******************************

#ifndef __LEDS_STRIP
#define __LEDS_STRIP

//*********************************INCLUDES******************************
#include <app_types.h>	//ToDo: Remove this dependency

//This driver uses STM32 HAL Library. Configure hal_conf accordingly to get DMA and TIM included
#include <stm32f3xx_hal.h>

//*********************************STRUCTURES******************************
typedef struct {
  uint16_t hue;
  uint8_t sat;
  uint8_t val;
} HSV;

//*********************************DEFINES******************************

#define COMC_NUM                1      // Number of LED channels

#define LEDS_NUM    68			// Number of LEDS per channel ledn range 0 to LEDS_NUM-1
#define COLRS       3			// Number of colors per LED

#define LED_LEV1  (31*2)		//PWM values for getting WS2812 timings
#define LED_LEV0  (31)			//Depends on hardware implementation
					//Add 1000x prescaler to timer init
					//And use scope to find your value	

// RGB COLORS
#define COLOR_NONE       0x00000000
#define COLOR_RED        0x00FF0000
#define COLOR_RED_HALF   0x00800000
#define COLOR_GREEN      0x0000FF00
#define COLOR_GREEN_HALF 0x00008000
#define COLOR_BLUE       0x000000FF
#define COLOR_WHITE      0x00FFFFFF

// HUE COLORS (0:360)
#define LED_HUE_RED1	0
#define LED_HUE_RED2	24
#define LED_HUE_RED3	48
#define LED_HUE_ORANGE1	72
#define LED_HUE_ORANGE2	96
#define LED_HUE_GREEN1	120
#define LED_HUE_GREEN2	144
#define LED_HUE_GREEN3	168
#define LED_HUE_CYAN1	192
#define LED_HUE_CYAN2	216
#define LED_HUE_BLUE1	240
#define LED_HUE_BLUE2	264
#define LED_HUE_BLUE3	288
#define LED_HUE_PURPLE1	312
#define LED_HUE_PURPLE2	336

//*********************************PROTOTYPES******************************


//Sometimes having HSV color map is a WIN
INT32U WS2812_HSV_to_RGB(HSV color_HSV);

//************
//Peripherial used is GPIO, DMA, TIM. GPIO for now is defined in owning file
//************
//ToDo: Make peripherial tunable by owner function(give handles?)
//Initializes DMA and TIM peripherials
void WS2812_Periph_Init(void);

//Speaks for itself
void WS2812_Clear_DMA_Flags(void);

//Initializes peripherial as well as initial LED states
void WS2812_Init(void);

//************Low level driving************

// WS2812_Set_DMA_Cell: addresses DMA_buf cell where data for individual LED is stored
void WS2812_Set_DMA_Cell(INT16U cell_number, uint8_t level_low, uint8_t level_high, INT32U data);

//WS2812_Set_DMA_Zeros: addresses whole DMA_buf. Fills it with zeros
void WS2812_Set_DMA_Zeros(void);

//************High level functions************

//WS2812_Set_LED: Set individual LED 
// ledn - led number ToDo: fix it to be 1 to LED_NUM instead of 0 to LED_NUM-1
// Color_HSV sets the color in format of HSV struct
void WS2812_Set_LED(INT16U ledn, HSV color_HSV);

//WS2812_Set_LED: Set individual LED 
// ledn - led number ToDo: fix it to be 1 to LED_NUM instead of 0 to LED_NUM-1
// Color_RGB sets the color in format of uint32 RGB
// RGB format starting from LSB - 0 to 7 BLUE; 8-15 GREEN; 16-23 RED 
void WS2812_Set_LED_RGB(INT16U ledn, int color_RGB);

//Light all LEDs with same color in range of ledn1 ledn2
void WS2812_fill_color(HSV color, int ledn1, int ledn2);

//Turn LEDs off
void WS2812_Fast_Zeros(void);

void DMA1_Channel7_IRQHandler(void);


#endif
